"use strict";

const path = require("path");
const fs = require("fs");
const webpack = require("webpack");
const {Module} = require("../Module.js");
const TerserPlugin = require("terser-webpack-plugin");

class Webpack extends Module
{
  Compile(app)
  {
    return new Promise(async (resolve, reject) =>
    {
      const paths = await app.WebpackPaths();

      let compiler;
      try
      {
        compiler = webpack({
          // mode: await app.WebpackMode(),
          // mode: "production",
          mode: "none",
          context: await app.WebpackContext(),
          entry: await app.WebpackEntry(),
          output: {
            path: await app.WebpackOutputPath(),
            filename: await app.WebpackOutputFilename(),
          },

          target: "web",

          performance: {
            // hints: false,
            maxEntrypointSize: 5_000_000,
            maxAssetSize: 5_000_000,
          },

          optimization: {
            minimize: true,

            minimizer: [
              new TerserPlugin({
                parallel: false, // This is important, it breaks otherwise

                terserOptions: {
                  // https://github.com/webpack-contrib/terser-webpack-plugin#terseroptions
                  keep_classnames: true,
                  keep_fnames: true,
                  // mangle: false,
                  // module: true,
                  // sourceMap: true,
                  // enclose: true,
                },
              }),
            ],

            // mangleExports: true,
            // mangleWasmImports: false,
            // mergeDuplicateChunks: true,
            // flagIncludedChunks: true,
            concatenateModules: false,
          },

          module: {
            rules: [
              {
                test: /\.m?js$/,
                exclude: /node_modules|Server/,
                use: [
                  {
                    loader: require.resolve("babel-loader"),
                    options: {
                      presets: [
                        [
                          require.resolve("@babel/preset-env"),
                          {
                            targets: "last 6 years",
                          },
                        ],
                      ],
                    },
                  },
                ],
              },
            ],
          },

          plugins: [
            new webpack.NormalModuleReplacementPlugin(/\/js\/(.*)/, (resource) =>
            {
              for (let i = 0; i < paths.length; i++)
              {
                const p = path.resolve(paths[i], "." + resource.request);
  
                if (fs.existsSync(p))
                {
                  resource.request = p;
                  return;
                }
              }
  
              // Check if it was a valid file anyway
              if (fs.existsSync(resource.request))
              {
                return;
              }
  
              console.warn("Failed to find webpack file for", resource.request);
            }),
          ],

          resolve: {
            extensions: [".js"],
            modules: paths,
          },
        });
      }
      catch (error)
      {
        error.schema = undefined;
        return reject(error);
      }

      compiler.run((error, stats) =>
      {
        compiler.close((error) =>
        {
          if (error)
          {
            reject(error);
          }
        });

        if (error)
        {
          return reject(error);
        }
        else if (stats.hasErrors())
        {
          const info = stats.toJson();
          return reject(info.errors);
        }
        else if (stats.hasWarnings())
        {
          const info = stats.toJson();
          console.warn(info.warnings);
          return resolve(stats);
        }
        else
        {
          console.log("Webpack finished compiling");
          return resolve(stats);
        }
      });
    });
  }

  async Load(app)
  {
    this.compile = this.Compile(app);

    return super.Load(app);
  }
}

module.exports.Webpack = Webpack;
module.exports.default = Webpack;
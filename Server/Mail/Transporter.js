"use strict";

const node_mailer = require("nodemailer");

class Transporter
{
  constructor({
    host,
    port = 465,
    secure = true,
    username,
    password,
  })
  {
    this.transporter = node_mailer.createTransport({
      host,
      port,
      secure, // true for 465 port, false for other ports
      auth: {
        user: username,
        pass: password,
      },
    });
  }

  Send(options)
  {
    return new Promise((resolve, reject) =>
    {
      this.transporter.sendMail(options, (error, info) =>
      {
        if (error) reject(error);
        else resolve(info);
      });
    });
  }
}

module.exports.Transporter = Transporter;
module.exports.default = Transporter;

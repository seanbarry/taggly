"use strict";

const node_mailer = require("nodemailer");
const {Module} = require("../Module.js");

class Mailer extends Module
{
  async Load(app)
  {
    const host = await app.MailHost();
    const port = await app.MailPort();
    const username = await app.MailUsername();
    const password = await app.MailPassword();

    this.transporter = node_mailer.createTransport({
      host,
      port,
      secure: port === 465 ? true : false, // true for 465 port, false for other ports
      auth: {
        user: username,
        pass: password,
      },
    });

    return super.Load(app);
  }

  Send({
    name,
    email,
    emails,
    subject,
    text,
    html,
  })
  {
    if (!name) throw new Error(`From name is required`);
    if (!email) throw new Error(`From email is required`);
    if (!emails || emails.length === 0) throw new Error(`To emails are required`);
    if (!subject) throw new Error(`A subject is required`);
    if (!text && !html) throw new Error(`Plain text and/or html content is required`);

    const options = {};
    options.from = `"${name}" <${email}>`;
    options.to = emails.join(", ");
    options.subject = subject;
    options.text = text;
    options.html = html;

    return new Promise((resolve, reject) =>
    {
      this.transporter.sendMail(options, (error, info) =>
      {
        if (error) reject(error);
        else resolve(info);
      });
    });
  }
}

module.exports.Mailer = Mailer;
module.exports.default = Mailer;

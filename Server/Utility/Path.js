"use strict";

const fs = require("fs");
const node_path = require("path");

class Path
{
  constructor(path)
  {
    // if (path instanceof Path) return path;
    this.path = path;
  }

  Normalize(){ this.path = node_path.normalize(this.path); return this; }

  From(path)
  {
    if (path instanceof Path) path = path.GetPath();
    else if (typeof(path) !== "string") throw new Error(`Unknown path type`);

    return new Path(node_path.resolve(this.path, path));
  }

  Require(key)
  {
    try
    {
      const mod = require(this.path);

      if (key) return mod[key];
      else return mod;
    }
    catch (error)
    {
      throw new Error(`Failed to require "${this.path}" because "${error.message}"`);
      // console.error(`Failed to require "${this.path}" because "${error.message}"`);
    }
  }

  ToDirectory(){ return new Path.Directory(this.path); }
  ToFile(){ return new Path.File(this.path); }

  Split(){ return this.path.split(node_path.delimiter); }
  Name(){ return node_path.win32.basename(this.path, node_path.extname(this.path)); }
  FullName(){ return node_path.win32.basename(this.path); }
  Extension(){ return node_path.extname(this.path); }
  Parent(){ return new Path(node_path.dirname(this.path)); }

  HasPath(){ console.warn("HasPath is depreciated, use IsValid"); return this.IsValid(); }
  IsValid(){ return !!this.path; }
  IsAbsolute(){ return node_path.isAbsolute(this.path); }

  SetPath(path){ this.path = path; }
  GetPath(){ return this.path; }
}

module.exports.Path = Path;
module.exports.default = Path;

"use strict";

class Module
{
  constructor(app, resolve, reject)
  {
    this.app = app;
    this.resolve = resolve;
    this.reject = reject;
  }

  destructor()
  {
  }

  async Load(app){ return this; }
  // async Reload(){ return this.app.Replace(this.constructor); }

  // Reload()
  // {
  //   const name = this.constructor.name;
  //
  //   const promise = new Promise((resolve, reject) =>
  //   {
  //     this.resolve = resolve;
  //     this.reject = reject;
  //   });
  //
  //   this.app.modules[name] = promise;
  //   return promise;
  // }

  // Reload()
  // {
  //   const name = this.constructor.name;
  //   this.app.modules[name] = new Promise((resolve, reject) =>
  //   {
  //     this.resolve = resolve;
  //     this.reject = reject;
  //   });
  //
  //   return this;
  // }

  Run()
  {
    if (!this.resolve) throw new Error(`Invalid resolve for Module ${this.constructor.name}`);
    if (!this.reject) throw new Error(`Invalid reject for Module ${this.constructor.name}`);

    return this.Load(this.app)
    .then(r => this.resolve(this))
    .catch(e => this.reject(e));

    // return this;
  }
}

module.exports.Module = Module;
module.exports.default = Module;

"use strict";

const path = require("path");
const uid = require("uid-safe");
const {Path} = require("./Utility/Path.js");

class App
{
  CreateCommand(name, message, command, target = global)
  {
    if (target.hasOwnProperty(name))
    {
      delete target[name];
    }

    Object.defineProperty(target, name, {
      configurable: true,
      get: () =>
      {
        setTimeout(() =>
        {
          command();
        }, 1);

        return message || "";
      },
    });
  }

  constructor()
  {
    global.app = this;

    this.CreateCommand("cl", "Clearing...", () =>
    {
      console.clear();
    });

    this.CreateCommand("rb", "Rebooting...", () =>
    {
      Object.keys(require.cache).forEach((key) =>
      {
        if (key.includes("\\node_modules\\")) return;

        delete require.cache[key];
      });

      const index = require(`${process.cwd()}${this.GetIndexName()}`);
    });

    this.modules = {};
  }

  destructor()
  {
    this.connection.destructor();
    this.server.destructor();
  }

  CreateLoader(ctor)
  {
    const name = ctor.name;

    // Create a promise and store it so that modules can wait for other modules
    // to be loaded
    this.modules[name] = new Promise((resolve, reject) =>
    {
      const mod = new ctor(this);

      // Do not invoke Load on the module right away,
      // because all modules must be constructed first
      loader = () =>
      {
        mod.Load(this)
        .then(r => resolve(mod))
        .catch(e => reject(e));

        return mod;
      }
    });

    return loader;
  }

  async Load(...modules)
  {
    const promises = [];
    const loading  = [];

    for (let i = 0; i < modules.length; i++)
    {
      const ctor = modules[i];
      if (!ctor) continue;

      const name = ctor.name;
      console.log("Constructing module", name);

      // If there is an existing module for this name, destruct it
      if (this.modules[name])
      {
        const mod = await this.modules[name];
        await mod.destructor();
      }

      // Create a promise and store it so that modules can wait for other modules
      // to be loaded
      this.modules[name] = new Promise((resolve, reject) =>
      {
        const mod = new ctor(this, resolve, reject);
        loading.push(mod);
      });

      promises.push(this.modules[name]);
    }

    // Now that each module has been constructed, we can invoke Load on each
    // and resolve/reject the promise
    for (let i = 0; i < loading.length; i++)
    {
      // const {mod, name, resolve, reject} = loading[i];
      const mod = loading[i];

      // console.log("Loading module", name);

      mod.Run(this);
      // .then(r => resolve(mod))
      // .catch(e => reject(e));
    }

    return Promise.all(promises);
  }

  Reload(name)
  {
    const old_promise = this.Require(name);

    this.modules[name] = new Promise((resolve, reject) =>
    {
      old_promise.then(mod =>
      {
        mod.resolve = resolve;
        mod.reject = reject;
      });
    });

    return old_promise;
  }

  Require(name)
  {
    const mod = this.modules[name];

    if (mod) return mod;
    else throw new Error(`${name} is not registered as a Module`);
  }

  Replace(ctor)
  {
    const name = ctor.name;

    let mod;

    this.modules[name] = new Promise((resolve, reject) =>
    {
      mod = new ctor(this, resolve, reject);
      // const replacement = new mod.constructor();
    });

    console.log("Replacing module", name);

    if (mod) return mod;
    else throw new Error(`${name} is not registered as a Module`);
  }

  async Initialize()
  {
    await this.Load(
      this.ServerType(),
      this.ConnectionType(),
      this.StoreType(),
      this.SessionType(),
      this.DatabaseType(),
      this.ModelRequestType(),
      this.ModelsType(),
      this.MailType(),
      this.SeederType(),
      this.WebpackType(),
    );

    if (this.UseServer())
    {
      const server = await this.Require("Server");
      return server.Launch();
    }
  }

  Reinitialize()
  {
    Object.keys(require.cache).forEach((key) =>
    {
      delete require.cache[key];
    });

    return this.Initialize();
  }

  GetDatabase(){ return this.Require("Database"); }

  GetConfig(){ throw new Error("App.GetConfig must be overridden"); }
  GetRootPath(){ throw new Error("App.GetRootPath must be overridden"); }
  GetModulesPath(){ return "_modules.json"; }
  GetIndexName(){ return "/index.js"; }
  GetTagglyRootPath(){ return __dirname; }
  GetStaticPath(){ return "./Client"; }
  GetTagglyStaticPath(){ return "./Client"; }
  GenerateUID(byte_length = 18){ return uid(byte_length); }
  GetObjectID(){ return require("mongodb").ObjectID; }
  GetBinary(){ return require("mongodb").Binary; }
  GetPerformance(){ return require("perf_hooks").performance; }

  UseWebpack(){ return true; }
  WebpackType(){ if (this.UseWebpack()) return require("./Webpack/Webpack.js").Webpack; }
  WebpackMode(){ return "development"; }
  WebpackPaths(){ return this.ServerPaths(); }
  WebpackContext(){ return path.resolve(this.GetRootPath(), "./Client"); }
  WebpackEntry(){ return path.resolve(this.GetRootPath(), "./Client/js/Main.js"); }
  WebpackOutputPath(){ return path.resolve(this.GetRootPath(), "./Client/js"); }
  WebpackOutputFilename(){ return "Compiled.js"; }

  UseServer(){ return true; }
  ServerType(){ if (this.UseServer()) return require("./Server/Server.js").Server; }
  ServerParseJSON(){ return true; }
  ServerParseForm(){ return true; }
  ServerParseLimit(){ return "1mb"; }
  ServerPort(){ return 3000; }
  ServerKey(){ return undefined; }
  ServerCertificate(){ return undefined; }
  ServerProxy(){ return false; }
  ServerSecret(){ throw new Error(`App.ServerSecret must be overridden`); }
  async ServerPaths()
  {
    return [
      new Path(await this.GetRootPath()).From(await this.GetStaticPath()).GetPath(),
      new Path(await this.GetTagglyRootPath()).From("../Client").GetPath(),
    ];
  }

  UseStore(){ return false; }
  StoreType(){ if (this.UseStore()) return require("./Database/Store.js").Store; }
  StoreName(){ return "sessions"; }

  UseConnection(){ return true; }
  ConnectionType(){ if (this.UseConnection()) return require("./Database/Connection.js").Connection; }
  ConnectionURL(){ return "mongodb://@localhost:27017/"; }
  ConnectionUsername(){ return undefined; }
  ConnectionPassword(){ return undefined; }

  UseDatabase(){ return true; }
  DatabaseType(){ return require("./Database/Database.js").Database; }
  DatabaseName(){ throw new Error("GetDatabaseName must be overridden"); }
  DatabaseTimeout(){ return undefined; }

  UseModels(){ return true; }
  ModelsType(){ if (this.UseModels()) return require("./Database/Models.js").Models; }
  ModelRequestType(){ if (this.UseModels()) return require("./Request/ModelRequest.js").ModelRequest; }

  UseSession(){ return false; }
  SessionType(){ if (this.UseSession()) return require("./Server/Session.js").Session; }
  SessionSanatize(){ return true; }
  SessionSecret(){ throw new Error("SessionSecret must be overridden"); }
  SessionMaxAge(){ return 1000 * 60 * 60 * 24 * 7; } // 1 week

  // Default to false, because Mail setup has required parameters
  UseMail     (){ return false; }
  MailType    (){ if (this.UseMail()) return require("./Mail/Mailer.js").Mailer; }
  MailPort    (){ return 465; }
  MailIsSecure(){ return true; }
  MailHost    (){ throw new Error("A Mail host is required"); }
  MailUsername(){ throw new Error("A Mail username is requried"); }
  MailPassword(){ throw new Error("A Mail password is requried"); }

  UseSeeder(){ return true; }
  SeederType(){ if (this.UseSeeder()) return require("./Database/Seeder.js").Seeder; }
  SeederRoot(){ return "./Server/Seed"; }
  SeederPaths(){ throw new Error(`App.SeederPaths must be overridden if using a Seeder`); }

  UseCollection(){ return true; }
  CollectionType(){ if (this.UseCollection()) return require("./Database/Collection.js").Collection; }

  UseRequest(){ return true; }
  RequestType(){ if (this.UseRequest()) return require("./Request/Request.js").Request; }

  UseResponse(){ return true; }
  ResponseType(){ if (this.UseResponse()) return require("./Request/Response.js").Response; }

  ReadConfig(string)
  {
    const parts = string.split(".");

    let current = this.Config();
    for (let i = 0; i < parts.length; i++)
    {
      const part = parts[i];

      if (current.hasOwnProperty(part)) current = current[part];
      else throw new Error(`Cannot read "${string}" from config because "${part}" is missing`);
    }

    return current;
  }

  Config(...parts)
  {
    let current = this.GetConfig();
    for (let i = 0; i < parts.length; i++)
    {
      const part = parts[i];

      if (current.hasOwnProperty(part)) current = current[part];
      else throw new Error(`Cannot read "${parts.join(".")}" from config because "${part}" is missing`);
    }

    return current;
  }
}

module.exports.App = App;
module.exports.default = App;

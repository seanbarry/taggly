"use strict";

const express = require("express");
const body_parser = require("body-parser");
const cookie_parser = require("cookie-parser");
const cors = require("cors");

// const {Path} = require("filesystem");
const {Path} = require("../Utility/Path.js");
const {Module} = require("../Module.js");

const https = require("https");
const http = require("http");

class NodeServer
{
  static BuildServer(port, key, cert, listener)
  {
    let server;
    const handler = function(req, res)
    {
      server.wrapper.listener.call(this.listener, req, res);
    };

    if (!key && !cert) server = http.createServer(handler);
    else server = https.createServer({ key, cert }, handler);

    server.listen(port);
    return server;
  }

  static Create(port, key, cert, listener)
  {
    const cache = global._server_cache || (global._server_cache = {});

    if (!cache[port])
    {
      console.log("Creating new cache entry for NodeServer");
      cache[port] = {
        port,
        key,
        cert,
        server: this.BuildServer(port, key, cert, listener),
      };
    }

    return new NodeServer(cache[port], listener);
  }

  constructor({port, key, cert, server}, listener)
  {
    console.log(`${key && cert ? "HTTPS" : "HTTP"} server listening on port ${port}`);

    this.port = port;
    this.key = key;
    this.cert = cert;
    this.server = server;
    this.listener = listener;

    if (server) server.wrapper = this;
  }
}

class Server extends Module
{
  async destructor()
  {
    for (let i = 0; i < this.servers.length; i++)
    {
      await new Promise((resolve, reject) =>
      {
        this.servers[i].close(() =>
        {
          console.log("Server closed!");
          resolve(this);
        });
      });
    }

    this.servers = [];
    return super.destructor();
  }

  destructor()
  {
    console.log("Caching servers", this.servers.length);
    global._cached_servers = this.servers;
    // cached_servers.push.apply(cached_servers, this.servers);
  }

  async Load(app)
  {
    this.servers = [];
    this.port = await app.ServerPort();
    this.key = await app.ServerKey();
    this.cert = await app.ServerCertificate();
    this.proxy = await app.ServerProxy();
    this.paths = await app.ServerPaths();

    if (this.key && this.cert)
    {
      this.credentials = {
        key: this.key,
        cert: this.cert,
      };
    }

    this.express = express();
    this.express.disable("x-powered-by");

    const limit = await app.ServerParseLimit();

    // For parsing application/json
    if (await app.ServerParseJSON()) this.express.use(body_parser.json({ limit }));

    this.express.use(cookie_parser(await app.ServerSecret()));

    // For parsing application/x-www-form-urlencoded
    if (await app.ServerParseForm()) this.express.use(body_parser.urlencoded({ limit, extended: true }));
    if (await app.ServerProxy()) this.express.set("trust proxy", 1); // Trust first proxy

    this.router = express.Router();
    this.express.use(this.router);

    return super.Load(app);
  }

  Listen(method, url, fn, use_cors = false)
  {
    const Request = this.app.RequestType();
    const Response = this.app.ResponseType();

    const handler = async (req, res, next) =>
    {
      const response = new Response(this.app, res);
      const request = new Request(this.app, req, response);

      await fn(this.app, request, response);

      if (!response.IsSent()) next();
    };

    if (use_cors === true)
    {
      this.router[method](url, cors(), handler);
    }
    else
    {
      this.router[method](url, handler);
    }

    return this;
  }

  Get    (url, fn, cors){ return this.Listen("get"    , url, fn, cors); }
  Head   (url, fn, cors){ return this.Listen("head"   , url, fn, cors); }
  Post   (url, fn, cors){ return this.Listen("post"   , url, fn, cors); }
  Put    (url, fn, cors){ return this.Listen("put"    , url, fn, cors); }
  Delete (url, fn, cors){ return this.Listen("delete" , url, fn, cors); }
  Trace  (url, fn, cors){ return this.Listen("trace"  , url, fn, cors); }
  Options(url, fn, cors){ return this.Listen("options", url, fn, cors); }
  Connect(url, fn, cors){ return this.Listen("connect", url, fn, cors); }
  Patch  (url, fn, cors){ return this.Listen("patch"  , url, fn, cors); }

  CreateStatic(path)
  {
    return express.static(path, {
      dotfiles: "ignore",
      etag: true,
      extensions: ["htm", "html"],
      index: "index.html",
      lastModified: true,
      // maxAge: 86400000,
      maxAge: 3600000,
      setHeaders(response, path, stat)
      {
        if (path.endsWith("index.html"))
        {
          response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        }
      },
    });
  }

  Launch()
  {
    // console.log("Launching with:", this.paths);

    for (let i = 0; i < this.paths.length; i++)
    {
      const path = this.paths[i];

      this.express.use(express.static(path, {
        dotfiles: "ignore",
        etag: true,
        fallthrough: true,
        extensions: ["html"],
        index: "index.html",
        lastModified: true,
        immutable: false,
        // maxAge: 86400000,
        maxAge: 3600000,
        // redirect: true,
        setHeaders(response, path, stat)
        {
          if (path.endsWith("index.html"))
          {
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
          }
        },
      }));
    }

    this.express.use("*", this.CreateStatic(this.paths[0]));

    this.server = NodeServer.Create(this.port, this.key, this.cert, this.express);
  }
}

// global._cached_servers = [];

module.exports.Server = Server;
module.exports.default = Server;

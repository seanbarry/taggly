"use strict";

const {Module} = require("../Module.js");

class Database extends Module
{
  async Load(app)
  {
    this.name = await app.DatabaseName();
    this.connection = await app.Require("Connection");
    this.database = await this.connection.GetMongoDatabase(this.name);
    this.timeout = await app.DatabaseTimeout();
    this.collection_type = await app.CollectionType();

    if (!this.collection_type) throw new Error(`Invalid Collection type`);

    return super.Load(app);
  }

  Collection(name, timeout = this.timeout)
  {
    // console.log("~~~~Creating collection", name);

    return new this.collection_type({
      app: this.app,
      database: this,
      collection: this.database.collection(name),
      name,
      timeout,
    });
  }

  GetApp(){ return this.app; }
  GetConnection(){ return this.connection; }
  GetName(){ return this.name; }
  GetDatabase(){ return this.database; }
  GetMongoCollection(name){ return this.database.collection(name); }
}

module.exports.Database = Database;
module.exports.default = Database;

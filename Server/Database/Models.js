"use strict";

const fs = require("fs");
const {Module} = require("../Module.js");

class Models extends Module
{
  async Load(app)
  {
    this.models = {};

    const path = await app.GetModulesPath();
    const database = await app.Require("Database");

    if (app.IsDevelopment())
    {
      // Load models from the database and save them to the file
      this.collection = await database.Collection("Module");

      this.modules = await this.collection.Query({ group: "Models" }).AsArray();

      fs.writeFile(path, JSON.stringify(this.modules, undefined, 2), "utf8", (error) =>
      {
        if (error)
        {
          return console.error("An error occured while writing JSON Object to File.", error);
        }

        // console.log("Modules file has been saved to", path);
      });
    }
    else
    {
      // Load models from the file
      this.modules = JSON.parse(fs.readFileSync(path, "utf8"));
      console.log("Loaded", this.modules.length, "modules from file");
    }

    let main = module;
    while (main.parent !== null)
    {
      main = main.parent;
    }

    const local_require = (name) =>
    {
      if (this.models[name])
      {
        return this.models[name];
      }

      for (let i = 0; i < this.modules.length; i++)
      {
        const mod = this.modules[i];
        if (mod.name === name)
        {
          const builder = new Function("global", "window", "require", mod.data);
          const result = builder(global, global, local_require);

          this.models[name] = result;
          return result;
        }
      }

      return main.require(name);
    }

    for (let i = 0; i < this.modules.length; i++)
    {
      const mod = this.modules[i];

      try
      {
        const model = local_require(mod.name);
        await model.Module();
        await model.Connect(database);
        await model.Seed(model.GetCollection());
      }
      catch (error)
      {
        console.error("Failed to run model", mod.name, error);
      }

      // console.log(i, "Connected model", model.name);
    }

    return super.Load(app);
  }

  UpdateModel({group, name, data, date})
  {
    if (!this.app.IsDevelopment())
    {
      throw new Error(`Must be on development to update a Model!`);
    }

    for (let i = 0; i < this.modules.length; i++)
    {
      const mod = this.modules[i];

      // If the module hasn't changed, there's no need to actually update it
      if (mod.name === name && mod.data === data) return;
    }

    return this.collection.GetCollection().findOneAndReplace(
      {
        name: { $eq: name },
        // data: { $ne: data },
      },
      {
        group,
        name,
        data,
        date,
      },
      {
        upsert: true,
      },
    );
  }

  GetModels(){ return this.models; }
  GetModel(name){ return this.models[name]; }
}

module.exports.Models = Models;
module.exports.default = Models;

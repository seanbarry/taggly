"use strict";

class Query
{
  constructor(collection, cursor, filter)
  {
    this.collection = collection;
    this.cursor = cursor;

    // This is just for convenience
    if (filter) this.Filter(filter);
  }

  Project(v){ this.cursor.project(v); return this; }
  Filter(v){ this.cursor.filter(v); return this; }
  Min(v){ this.cursor.min(v); return this; }
  Max(v){ this.cursor.max(v); return this; }
  Skip(v){ if (typeof(v) === "number") this.cursor.skip(v); return this; }
  Limit(v){ if (typeof(v) === "number") this.cursor.limit(v); return this; }
  Timeout(v){ if (typeof(v) === "number") this.cursor.maxTimeMS(v); return this; }
  Sort(v){ if (v) this.cursor.sort(v); return this; }
  Hint(v){ this.cursor.hint(v); return this; }
  Map(fn){ this.cursor.map(fn); return this; }

  AsArray(use_model = true)
  {
    // If a model constructor was provided, convert each document into
    // a model before returning them
    if (use_model) return this.cursor.toArray().then(array => array.map(v => this.collection.New(v)));
    else return this.cursor.toArray();
  }

  AsForEach(fn, use_model = true)
  {
    // If a model constructor was provided, convert each document into
    // a model before handing them to the callback function
    if (use_model) return this.cursor.forEach(v => fn(this.collection.New(v)));
    else return this.cursor.forEach(fn);
  }

  AsArray(){ return this.cursor.toArray(); }
  AsModels(model){ return this.cursor.toArray().then(array => array.map(values => new model({ values }))); }
  AsForEach(fn){ return this.cursor.forEach(fn); }

  AsCount  (){ return this.cursor.count(); }
  AsExists (){ return this.cursor.count().then(c => (c > 0)); }
  AsNone   (){ return this.cursor.count().then(c => (c === 0)); }
  AsFirst  (){ return this.cursor.limit(1).toArray().then(v => this.collection.New(v[0])); }
  AsExplain(){ return this.cursor.explain(); }
  AsStream(options){ return this.cursor.transformStream(options); }

  IsClosed(){ return this.cursor.isClosed(); }
}

module.exports.Query = Query;
module.exports.default = Query;

"use strict";

const mongodb = require("mongodb");
const path = require("path");
const {Module} = require("../Module.js");

class Seeder extends Module
{
  async Load(app)
  {
    // Run it in a then, because there's no need to delay startup for this
    app.Require("Database").then(async db =>
    {
      const root = path.resolve(await app.GetRootPath(), await app.SeederRoot());
      const paths = await app.SeederPaths();

      for (let i = 0; i < paths.length; i++)
      {
        const resolved = path.resolve(root, paths[i]);

        console.log("Seeding", paths[i]);
        const mod = require(resolved);

        for (const key in mod)
        {
          if (!mod.hasOwnProperty(key)) continue;
          const value = mod[key];

          if (typeof(value) === "function")
          {
            // Wrap it in Promise.resolve, because it doesn't necessarily return a promise
            Promise.resolve(value(db, app, mongodb)).catch(error =>
            {
              console.error(`Error seeding ${paths[i]}`, error);
            });
          }
        }
      }
    });

    return super.Load(app);
  }
}

module.exports.Seeder = Seeder;
module.exports.default = Seeder;

"use strict";

const {MongoClient} = require("mongodb");
const {Module} = require("../Module.js");

class Connection extends Module
{
  destructor()
  {
    this.client.close();
  }

  async Load(app)
  {
    this.url = await app.ConnectionURL();
    this.username = await app.ConnectionUsername();
    this.password = await app.ConnectionPassword();

    this.client = new MongoClient(this.url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      // auth: {
      //   user: username,
      //   password: password,
      // },
    });

    await new Promise((resolve, reject) =>
    {
      this.client.connect(error =>
      {
        if (error) return reject(error);
        else return resolve(this);
      });
    });

    return super.Load(app);
  }

  GetApp(){ return this.app; }
  GetURL(){ return this.url; }
  GetUsername(){ return this.username; }
  GetPassword(){ return this.password; }
  GetClient(){ return this.client; }

  GetMongoDatabase(name){ return this.client.db(name); }
}

module.exports.Connection = Connection;
module.exports.default = Connection;

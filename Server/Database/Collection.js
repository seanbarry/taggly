"use strict";

const {Query} = require("./Query.js");

class Collection
{
  constructor({
    app,
    database,
    collection,
    name,
    timeout, // Optional time in milliseconds
  })
  {
    this.app = app;
    this.database = database;
    this.collection = collection;
    this.name = name;
    this.timeout = timeout;
  }

  RawInsertOne(object)
  {
    return this.collection.insertOne(object).then(result =>
    {
      object._id = result.insertedId;
      return object;
    });
  }

  RawUpsertOne(filter, update, {projection, sort, maxTimeMS} = {})
  {
    if (!filter) throw new Error("UpsertOne requires a filter");
    if (!update) throw new Error("UpsertOne requires an update");

    return this.collection.findOneAndUpdate(filter, { $set: update }, {
      projection,
      sort,
      maxTimeMS,
      upsert: true,
      returnDocument: "after",
    })
    .then(result => result ? result.value : null);
  }

  RawUpsertOneBy(key, update, options)
  {
    if (!key) throw new Error("RawUpsertOneBy requires a key");
    if (!update) throw new Error("RawUpsertOneBy requires an update");

    return this.RawUpsertOne({ [key]: update[key] }, update, options);
  }

  CreateKeyFilter(keys, source)
  {
    if (typeof(keys) === "string") keys = [keys];

    const filter = {};

    for (let i = 0; i < keys.length; i++)
    {
      const key = keys[i];
      filter[key] = source[key];
    }

    return filter;
  }

  RawUpsertOneBy(key, update, options)
  {
    if (!key) throw new Error("RawUpsertOneBy requires a key");
    if (!update) throw new Error("RawUpsertOneBy requires an update");

    return this.RawUpsertOne(this.CreateKeyFilter(key, update), update, options);
    // return this.RawUpsertOne({ [key]: update[key] }, update, options);
  }

  async RawUpsertAllBy(key, updates, options)
  {
    if (!key) throw new Error("UpsertAll requires a key");
    if (!updates) throw new Error("UpsertAll requires updates");

    const results = [];
    for (let i = 0; i < updates.length; i++)
    {
      const result = await this.RawUpsertOneBy(key, updates[i], options);
      if (result) results.push(result);
    }

    return results;
  }

  RawResertOne(filter, object, {projection, sort, maxTimeMS} = {})
  {
    if (!filter) throw new Error("ResertOne requires a filter");
    if (!object) throw new Error("ResertOne requires an object");

    return this.collection.findOneAndReplace(filter, object, {
      projection,
      sort,
      maxTimeMS,
      upsert: false,
      returnDocument: "after",
    }).then(result => result ? result.value : null);
  }

  RawResertOneBy(key, update, options)
  {
    if (!key) throw new Error("RawResertOneBy requires a key");
    if (!update) throw new Error("RawResertOneBy requires an update");

    return this.RawResertOne({ [key]: update[key] }, update, options);
  }

  RawUnsertOne(filter, object, {projection, sort, maxTimeMS} = {})
  {
    if (!filter) throw new Error("UnsertOne requires a filter");
    if (!object) throw new Error("UnsertOne requires an object");

    return this.collection.findOneAndUpdate(filter, { $setOnInsert: object }, {
      projection,
      sort,
      maxTimeMS,
      upsert: true,
      returnDocument: "after",
    })
    .then(result => result ? result.value : null);
  }

  RawUnsertOneBy(key, update, options)
  {
    if (!key) throw new Error("RawUnsertOneBy requires a key");
    if (!update) throw new Error("RawUnsertOneBy requires an update");

    return this.RawUnsertOne({ [key]: update[key] }, update, options);
  }

  RawDeleteOne(filter, {projection, sort, maxTimeMS} = {})
  {
    if (!filter) throw new Error("DeleteOne requires a filter");

    return this.collection.findOneAndDelete(filter, {
      projection,
      sort,
      maxTimeMS,
    }).then(result => result ? result.value : null);
  }

  async RawDeleteAll(filter, options)
  {
    if (!filter) throw new Error("DeleteAll requires a filter");

    const results = [];
    while (true)
    {
      const result = await this.RawDeleteOne(filter, options);

      if (!result) break;
      else results.push(result);
    }

    return results;
  }

  RawUpdateOne(filter, update, {projection, sort, maxTimeMS} = {})
  {
    if (!filter) throw new Error("UpdateOne requires a filter");
    if (!update) throw new Error("UpdateOne requires an update");

    return this.collection.findOneAndUpdate(filter, { $set: update }, {
      projection,
      sort,
      maxTimeMS,
      upsert: false,
      returnDocument: "after",
    }).then(result => result ? result.value : null);
  }

  RawUpdateOneBy(key, update, options)
  {
    if (!key) throw new Error("RawUpdateOneBy requires a key");
    if (!update) throw new Error("RawUpdateOneBy requires an update");

    return this.RawUpdateOne({ [key]: update[key] }, update, options);
  }

  RawFindOne(filter, {projection, sort, maxTimeMS} = {})
  {
    if (!filter) throw new Error("FindOne requires a filter");

    return this.collection.findOne(filter, {
      projection,
      sort,
      maxTimeMS,
    })
    .then(result => result ? result : null);
  }

  RawFindAll(filter, {skip, limit, projection, sort, maxTimeMS} = {})
  {
    if (!filter) throw new Error("FindAll requires a filter");

    const cursor = this.collection.find(filter);

    if (typeof(projection) === "object" && projection !== null) cursor.project(projection);
    if (typeof(sort      ) === "object" && sort       !== null) cursor.sort(sort);
    if (typeof(maxTimeMS ) === "number" && maxTimeMS  >      0) cursor.maxTimeMS(maxTimeMS);
    if (typeof(skip      ) === "number") cursor.skip(skip);
    if (typeof(limit     ) === "number") cursor.limit(limit);

    return new Promise((resolve, reject) =>
    {
      cursor.toArray((error, result) =>
      {
        if (error) return reject(error);
        return resolve(result);
      });
    });
  }

  RawCountOne(filter, {maxTimeMS} = {})
  {
    if (!filter) throw new Error("CountOne requires a filter");

    return this.collection.countDocuments(
      filter,
      {
        skip: 0,
        limit: 1,
        maxTimeMS,
      },
    );
  }

  RawCountAll(filter, {skip, limit, maxTimeMS} = {})
  {
    if (!filter) throw new Error("CountAll requires a filter");

    return this.collection.countDocuments(
      filter,
      {
        skip,
        limit,
        maxTimeMS,
      },
    );
  }

  InsertOne(model)
  {
    return this.collection.insertOne(model).then(result =>
    {
      model.ID(undefined, result.insertedId);
      return model;
    });
  }

  InsertAll(models)
  {
    // TODO: Test if this is how the .insertedIds array (map?) works
    return this.collection.insertMany(models).then(result =>
    {
      for (let i = 0; i < result.insertedIds.length; i++)
      {
        models[i].ID(undefined, result.insertedIds[i]);
      }

      return models;
    });
  }

  // Update Insert: Find OR create a model, then update it
  UpsertOne(model)
  {
    const filter = model.GetFilter();
    const update = model.GetUpdate();
    const projection = model.GetProject();
    const sort = model.GetSort();
    const maxTimeMS = model.GetTimeout();

    // console.log("UpsertOne", model, filter, update);

    if (!filter) throw new Error("UpsertOne requires a filter");
    if (!update) throw new Error("UpsertOne requires an update");

    return this.collection.findOneAndUpdate(filter, update, {
      projection,
      sort,
      maxTimeMS,
      upsert: true,
      returnDocument: "after",
    })
    // .then(result => result ? this.Process(model, result.value, {filter, update}) : null);
    .then(result => result ? model.New(result.value) : null);
  }

  // Replace Insert: Find OR create a model, then replace it
  ResertOne(model)
  {
    const filter = model.GetFilter();
    const projection = model.GetProject();
    const sort = model.GetSort();
    const maxTimeMS = model.GetTimeout();

    // console.log("ResertOne", model, filter);

    if (!filter) throw new Error("ResertOne requires a filter");

    return this.collection.findOneAndReplace(filter, model, {
      projection,
      sort,
      maxTimeMS,
      upsert: false,
      returnDocument: "after",
    }).then(result => result ? model.New(result.value) : null);
  }

  // Unique Insert: Find OR insert a model
  UnsertOne(model)
  {
    const filter = model.GetFilter();
    const projection = model.GetProject();
    const sort = model.GetSort();
    const maxTimeMS = model.GetTimeout();

    // console.log("UnsertOne", model, filter);

    if (!filter) throw new Error("UnsertOne requires a filter");

    return this.collection.findOneAndUpdate(filter, { $setOnInsert: model }, {
      projection,
      sort,
      maxTimeMS,
      upsert: true,
      returnDocument: "after",
    })
    .then(result => result ? model.New(result.value) : null);
  }

  DeleteOne(model)
  {
    const filter = model.GetFilter();
    const projection = model.GetProject();
    const sort = model.GetSort();
    const maxTimeMS = model.GetTimeout();

    if (!filter) throw new Error("DeleteOne requires a filter");

    return this.collection.findOneAndDelete(filter, {
      projection,
      sort,
      maxTimeMS,
    }).then(result => result ? model.New(result.value) : null);
  }

  UpdateOne(model)
  {
    const filter = model.GetFilter();
    const update = model.GetUpdate();
    const projection = model.GetProject();
    const sort = model.GetSort();
    const maxTimeMS = model.GetTimeout();

    // console.log("UpdateOne", model, filter, update);

    if (!filter) throw new Error("UpdateOne requires a filter");
    if (!update) throw new Error("UpdateOne requires an update");

    return this.collection.findOneAndUpdate(filter, update, {
      projection,
      sort,
      maxTimeMS,
      upsert: false,
      returnDocument: "after",
    }).then(result => result ? model.New(result.value) : null);
  }

  FindOne(model)
  {
    const filter = model.GetFilter();
    const projection = model.GetProject();
    const sort = model.GetSort();
    const maxTimeMS = model.GetTimeout();

    if (!filter) throw new Error("FindOne requires a filter");

    return this.collection.findOne(filter, {
      projection,
      sort,
      maxTimeMS,
    })
    .then(result => result ? model.New(result) : null);
  }

  FindOne(model)
  {
    const filter = model.GetFilter();
    const projection = model.GetProject();
    const sort = model.GetSort();
    const maxTimeMS = model.GetTimeout();
    const skip = model.GetSkip();

    if (!filter) throw new Error("FindOne requires a filter");

    return new Promise((resolve, reject) =>
    {
      const cursor = this.collection.find(filter);

      if (typeof(projection) === "object" && projection !== null) cursor.project(projection);
      if (typeof(sort      ) === "object" && sort       !== null) cursor.sort(sort);
      if (typeof(maxTimeMS ) === "number" && maxTimeMS  >      0) cursor.maxTimeMS(maxTimeMS);
      if (typeof(skip      ) === "number") cursor.skip(skip);

      cursor.limit(1); // Just find one

      cursor.toArray((error, result) =>
      {
        if (error) return reject(error);

        if (!result.length === 0) return resolve(null);
        else return resolve(model.New(result[0]));
      });
    });
  }

  FindAll(model)
  {
    const filter = model.GetFilter();
    const projection = model.GetProject();
    const sort = model.GetSort();
    const maxTimeMS = model.GetTimeout();
    const skip = model.GetSkip();
    const limit = model.GetLimit();

    // console.log("FindAll", model, filter);

    if (!filter) throw new Error("FindAll requires a filter");

    return new Promise((resolve, reject) =>
    {
      const cursor = this.collection.find(filter);

      if (typeof(projection) === "object" && projection !== null) cursor.project(projection);
      if (typeof(sort      ) === "object" && sort       !== null) cursor.sort(sort);
      if (typeof(maxTimeMS ) === "number" && maxTimeMS  >      0) cursor.maxTimeMS(maxTimeMS);
      if (typeof(skip      ) === "number") cursor.skip(skip);
      if (typeof(limit     ) === "number") cursor.limit(limit);

      cursor.toArray((error, result) =>
      {
        if (error) return reject(error);

        // For each result, create a new model of the filter type and load the result
        const models = [];
        for (let i = 0; i < result.length; i++)
        {
          try
          {
            models.push(model.New(result[i]));
          }
          catch (error)
          {
            return reject(error);
          }
        }

        return resolve(models);
      });
    });
  }

  CountOne(model)
  {
    const filter = model.GetFilter();
    const maxTimeMS = model.GetTimeout();

    if (!filter) throw new Error("CountOne requires a filter");

    return this.collection.countDocuments(
      filter,
      {
        skip: 0,
        limit: 1,
        maxTimeMS,
      },
    );
  }

  CountAll(model)
  {
    const filter = model.GetFilter();
    const skip = model.GetSkip();
    const limit = model.GetLimit();
    const maxTimeMS = model.GetTimeout();

    if (!filter) throw new Error("CountAll requires a filter");

    return this.collection.countDocuments(
      filter,
      {
        skip,
        limit,
        maxTimeMS,
      },
    );
  }

  Query(filter){ return new Query(this, this.collection.find(), filter); }

  CreateIndexes(indexes, options)
  {
    if (typeof(indexes) !== "object" || indexes === null)
    {
      throw new Error(`Expected indexes to be an object`);
    }

    return new Promise((resolve, reject) =>
    {
      this.collection.createIndex(indexes, options, (error, result) =>
      {
        if (error) return reject(error);
        else return resolve(result);
      });
    });
  }

  // CreateIndexes(indexes, options)
  // {
  //   if (typeof(indexes) !== "object" || indexes === null)
  //   {
  //     throw new Error(`Expected indexes to be an object`);
  //   }
  //
  //   return this.DeleteAllIndexes().then(r =>
  //   {
  //     return new Promise((resolve, reject) =>
  //     {
  //       this.collection.createIndex(indexes, options, (error, result) =>
  //       {
  //         if (error) return reject(error);
  //         else return resolve(result);
  //       });
  //     });
  //   });
  // }

  CreateUniqueIndexes(indexes){ return this.CreateIndexes(indexes, { unique: true }); }
  CreateExpirationIndexes(indexes, seconds = 0){ return this.CreateIndexes(indexes, { expireAfterSeconds: seconds }); }

  DeleteIndex(index, options)
  {
    return new Promise((resolve, reject) =>
    {
      this.collection.dropIndex(index, options, (error, result) =>
      {
        if (error) return reject(error);
        else return resolve(this);
      });
    });
  }

  DeleteAllIndexes(options)
  {
    return new Promise((resolve, reject) =>
    {
      this.collection.dropIndexes(options, (error, result) =>
      {
        if (error) return reject(error);
        else return resolve(this);
      });
    });
  }

  GetApp       (){ return this.app; }
  GetDatabase  (){ return this.database; }
  GetCollection(){ return this.collection; }
  GetName      (){ return this.name; }
  GetTimeout   (){ return this.timeout; }
}

module.exports.Collection = Collection;
module.exports.default = Collection;

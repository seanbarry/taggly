"use strict";

class Tag
{
  static New(type, cls, close = true){ return new Tag(type, close).Class(cls); }

  static Abbr          (c){ return Tag.New("abbr", c); }
  static Abbreviation  (c){ return Tag.New("abbr", c); }
  static Anchor        (c){ return Tag.New("a", c); }
  static Area          (c){ return Tag.New("area", c); }
  static Article       (c){ return Tag.New("article", c); }
  static Aside         (c){ return Tag.New("aside", c); }
  static Bold          (c){ return Tag.New("b", c); }
  static Body          (c){ return Tag.New("body", c); }
  static BreakLine     (c){ return Tag.New("br", c); }
  static Button        (c){ return Tag.New("button", c, false); }
  static Code          (c){ return Tag.New("code", c); }
  static Canvas        (c){ return Tag.New("canvas", c); }
  static Div           (c){ return Tag.New("div", c); }
  static Emphasis      (c){ return Tag.New("em", c); }
  static Embed         (c){ return Tag.New("embed", c); }
  static Figure        (c){ return Tag.New("figure", c); }
  static FigureCaption (c){ return Tag.New("figcaption", c); }
  static Footer        (c){ return Tag.New("footer", c); }
  static Form          (c){ return Tag.New("form", c); }
  static H1            (c){ return Tag.New("h1", c); }
  static H2            (c){ return Tag.New("h2", c); }
  static H3            (c){ return Tag.New("h3", c); }
  static H4            (c){ return Tag.New("h4", c); }
  static H5            (c){ return Tag.New("h5", c); }
  static H6            (c){ return Tag.New("h6", c); }
  static Head          (c){ return Tag.New("head", c); }
  static Header1       (c){ return Tag.New("h1", c); }
  static Header2       (c){ return Tag.New("h2", c); }
  static Header3       (c){ return Tag.New("h3", c); }
  static Header4       (c){ return Tag.New("h4", c); }
  static Header5       (c){ return Tag.New("h5", c); }
  static Header6       (c){ return Tag.New("h6", c); }
  static Head          (c){ return Tag.New("head", c); }
  static Header        (c){ return Tag.New("header", c); }
  static HorizontalRule(c){ return Tag.New("hr", c); }
  static HTML          (c){ return Tag.New("html", c); }
  static Italic        (c){ return Tag.New("i", c); }
  static InlineFrame   (c){ return Tag.New("iframe", c); }
  static IFrame        (c){ return Tag.New("iframe", c); }
  static Img           (c){ return Tag.New("img", c, false); }
  static Image         (c){ return Tag.New("img", c, false); }
  static Input         (c){ return Tag.New("input", c); }
  static Label         (c){ return Tag.New("label", c); }
  static ListItem      (c){ return Tag.New("li", c); }
  static LI            (c){ return Tag.New("li", c); }
  static Link          (c){ return Tag.New("link", c, false); }
  static Main          (c){ return Tag.New("main", c); }
  static Meta          (c){ return Tag.New("meta", c, false); }
  static Nav           (c){ return Tag.New("nav", c); }
  static NoScript      (c){ return Tag.New("noscript", c); }
  static OrderedList   (c){ return Tag.New("ol", c); }
  static OL            (c){ return Tag.New("ol", c); }
  static Option        (c){ return Tag.New("option", c); }
  static Output        (c){ return Tag.New("output", c); }
  static Paragraph     (c){ return Tag.New("p", c); }
  static Pre           (c){ return Tag.New("pre", c); }
  static Progress      (c){ return Tag.New("progress", c); }
  static Script        (c){ return Tag.New("script", c); }
  static Section       (c){ return Tag.New("section", c); }
  static Select        (c){ return Tag.New("select", c); }
  static Small         (c){ return Tag.New("small", c); }
  static Span          (c){ return Tag.New("span", c); }
  static Strong        (c){ return Tag.New("strong", c); }
  static Style         (c){ return Tag.New("style", c); }
  static Source        (c){ return Tag.New("source", c); }
  static Summary       (c){ return Tag.New("summary", c); }
  static Table         (c){ return Tag.New("table", c); }
  static TextArea      (c){ return Tag.New("textarea", c); }
  static TableBody     (c){ return Tag.New("tbody", c); }
  static TableData     (c){ return Tag.New("td", c); }
  static TableFoot     (c){ return Tag.New("tfoot", c); }
  static TableHeader   (c){ return Tag.New("th", c); }
  static TableHead     (c){ return Tag.New("thead", c); }
  static Time          (c){ return Tag.New("time", c); }
  static Title         (c){ return Tag.New("title", c); }
  static TableRow      (c){ return Tag.New("tr", c); }
  static Track         (c){ return Tag.New("track", c); }
  static Template      (c){ return Tag.New("template", c); }
  static UnorderedList (c){ return Tag.New("ul", c); }
  static UL            (c){ return Tag.New("ul", c); }
  static Video         (c){ return Tag.New("video", c); }

  constructor(type, close)
  {
    this.type = type;
    this.close = close;
    this.attributes = {};
    this.children = [];
    this.content = "";
  }

  Inner(value){ this.content = value; return this; }
  Text (value){ this.content = value; return this; }
  Code(v)
  {
    // let code = v.toString().split("\n").map(l => l.trim()).join("\n");
    let code = v.toString();
    return this.Text(code);
  }

  Attribute(key, value){ this.attributes[key] = value; return this; }

  Class            (v){ return this.Attribute("class",             v); }
  Style            (v){ return this.Attribute("style",             v); }
  ID               (v){ return this.Attribute("id",                v); }
  Name             (v){ return this.Attribute("name",              v); }
  Placeholder      (v){ return this.Attribute("placeholder",       v); }
  Value            (v){ return this.Attribute("value",             v); }
  HRefDefault      (v){ return this.Attribute("href",              v); }
  Src              (v){ return this.Attribute("src",               v); }
  Rel              (v){ return this.Attribute("rel",               v); }
  Role             (v){ return this.Attribute("role",              v); }
  Type             (v){ return this.Attribute("type",              v); }
  Alt              (v){ return this.Attribute("alt",               v); }
  Min              (v){ return this.Attribute("min",               v); }
  Max              (v){ return this.Attribute("max",               v); }
  For              (v){ return this.Attribute("for",               v); }
  Checked          (v){ return this.Attribute("checked",           v); }
  Disabled         (v){ return this.Attribute("disabled",          v); }
  Draggable        (v){ return this.Attribute("draggable",         v); }
  Controls         (v){ return this.Attribute("controls",          v); }
  ReadOnly         (v){ return this.Attribute("readonly",          v); }
  AutoComplete     (v){ return this.Attribute("autocomplete",      v); }
  Width            (v){ return this.Attribute("width",             v); }
  Height           (v){ return this.Attribute("height",            v); }
  Selected         (v){ return this.Attribute("selected",          v); }
  Title            (v){ return this.Attribute("title",             v); }
  CrossOrigin      (v){ return this.Attribute("crossorigin",       v); }
  AriaHasPopUp     (v){ return this.Attribute("aria-haspopup",     v); }
  AriaControls     (v){ return this.Attribute("aria-controls",     v); }
  AriaHidden       (v){ return this.Attribute("aria-hidden",       v); }
  AriaLabel        (v){ return this.Attribute("aria-label",        v); }
  AriaCurrent      (v){ return this.Attribute("aria-current",      v); }
  Required         (v){ return this.Attribute("required",          v); }
  Preload          (v){ return this.Attribute("preload",           v); }
  PlaysInline      (v){ return this.Attribute("playsinline",       v); }
  AutoPlay         (v){ return this.Attribute("autoplay",          v); }
  Muted            (v){ return this.Attribute("muted",             v); }
  Loop             (v){ return this.Attribute("loop",              v); }
  Poster           (v){ return this.Attribute("poster",            v); }
  FrameBorder      (v){ return this.Attribute("frameborder",       v); }
  Allow            (v){ return this.Attribute("allow",             v); }
  AllowFullScreen  (v){ return this.Attribute("allowfullscreen",   v); }
  AllowTransparency(v){ return this.Attribute("allowtransparency", v); }
  HRef             (v){ return this.Attribute("href",              v); }
  CharSet          (v){ return this.Attribute("charset",           v); }
  Content          (v){ return this.Attribute("content",           v); }
  Sizes            (v){ return this.Attribute("sizes",             v); }
  Data       (name, v){ return this.Attribute(`data-${name}`,      v); }

  InsertAt(value, index)
  {
    if ((index === undefined) || (index === null) || (index >= this.children.length))
    {
      this.children.push(value);
    }
    else
    {
      this.children.splice(index, 0, value);
    }

    return true;
  }

  Insert(item, index)
  {
    switch (typeof(item))
    {
      case "undefined": return false;
      case "boolean": return false;
      case "symbol": return false;
      case "string": return this.InsertAt(item, index);
      case "number": return this.InsertAt(item.toString(), index);
      case "function": return this.Insert(item(this), index);
      case "object":
      {
        if      (item === null) return false; // Because null is an object
        else if (item instanceof Tag) return this.InsertAt(item, index);
        else if (item instanceof Array)
        {
          for (let i = 0; i < item.length; i++)
          {
            this.Insert(item[i], index);
            if (index !== undefined) index += 1;
          }
        }
        else if (item instanceof Promise)
        {
          index = index || this.children.length;
          item.then(result => this.Insert(result, index));
        }
        else if (typeof(item[Symbol.iterator]) === "function") // If it's iterable
        {
          for (const i of item)
          {
            this.Insert(i, index);
            if (index !== undefined) index += 1;
          }
        }
        else
        {
          return false;
        }

        return true;
      }
      default: return false;
    }

    return true;
  }

  Add(...tags)
  {
    for (let i = 0, l = tags.length; i < l; i++)
    {
      this.Insert(tags[i]);
    }

    return this;
  }

  Render(indent)
  {
    const lines = [];
    const keys = Object.keys(this.attributes).filter(a => this.attributes[a] !== undefined);
    const attributes = keys.map(a => ` ${a}="${this.attributes[a]}"`);

    if (this.close)
    {
      lines.push(`${indent}<${this.type}${attributes.join("")}>`);
    }
    else
    {
      lines.push(`${indent}<${this.type}${attributes.join("")}/>`);
    }

    const raised = indent + "  ";

    if (this.content)
    {
      lines.push(this.content.split("\n").map(c => raised + c).join("\n"));
    }
    else if (this.children.length > 0)
    {
      for (let i = 0; i < this.children.length; i++)
      {
        const child = this.children[i];
        lines.push.apply(lines, child.Render(raised));
      }
    }

    if (this.close)
    {
      lines.push(`${indent}</${this.type}>`);
    }

    return lines;
  }

  toString()
  {
    return this.Render("").join("\n");
  }
}

module.exports.Tag = Tag;
module.exports.default = Tag;

"use strict";

const {File, Directory, Path} = require("filesystem");
const {Utility} = require("debug");
const {Compiler} = require("./Compiler.js");

class ImportsCompiler extends Compiler
{
  constructor(options)
  {
    super(options);

    this.links = [];
    this.calls = [];

    this.AddPlugin({
      ClassDeclaration: (path) =>
      {
        const types = this.GetBabelTypes();
        const body = [];

        console.log("Running");

        for (let i = 0; i < this.links.length; i++)
        {
          const {name, alias, url, identifier} = this.links[i];

          path.insertBefore(
            types.importDeclaration(
              [
                types.importSpecifier(
                  types.identifier(alias),
                  types.identifier(name),
                ),
              ],
              types.stringLiteral(url),
            ),
          );

          body.push(
            types.expressionStatement(
              types.callExpression(
                types.memberExpression(
                  types.thisExpression(),
                  types.identifier("AddLink"),
                ),
                [
                  types.identifier(alias),
                  types.stringLiteral(identifier),
                ],
              ),
            ),
          );
        }

        for (let i = 0; i < this.calls.length; i++)
        {
          const {url, identifier} = this.calls[i];

          body.push(
            types.expressionStatement(
              types.callExpression(
                types.memberExpression(
                  types.thisExpression(),
                  types.identifier("AddCall"),
                ),
                [
                  types.stringLiteral(url),
                  types.stringLiteral(identifier),
                ],
              ),
            ),
          );
        }

        path.get("body").pushContainer(
          "body",
          types.classMethod(
            "method",
            types.identifier("Build"),
            [],
            types.blockStatement(body),
          ),
        );
      },
    });
  }

  Transform(ast)
  {
    this.Call(ast);
  }

  async FindFiles(directory, current, parts = [], files = [])
  {
    console.log(directory);
    const keys = Object.keys(current);
    for (let i = 0; i < keys.length; i++)
    {
      const key = keys[i];
      const val = current[key];

      parts.push(key);

      if (val instanceof Path)
      {
        let rel = val.GetPath();
        if (!rel)
        {
          rel = val.SetPath(parts.join("/") + ".js");
        }

        // console.log("Finding", directory.GetPath() + rel);
        const file = await directory.FindChild(rel);

        files.push({
          key,
          path: val,
          identifier: parts.join("."),
          file,
        });
      }
      else
      {
        await this.FindFiles(directory, val, parts, files);
      }

      parts.pop();
    }

    return files;
  }

  FindPages(root, pages, parts = [])
  {
    for (let i = 0; i < keys.length; i++)
    {
      const key = keys[i];
      const val = pages[key];

      parts.push(key);

      if (val instanceof Path)
      {
        let rel = val.GetPath();
        if (!rel) rel = parts.join("/") + ".js";

        const path = root + val.GetPath();
        console.log("Found a page!", i, parts.join("."));

        this.AddLink({
          name: key,
          url: val.GetPath(),
          identifier: parts.join("."),
        });
      }
      else
      {
        this.FindPages(root, val, parts);
      }

      parts.pop();
    }
  }

  AddLink({name, url, identifier, file})
  {
    this.links.push({
      name,
      // url: path.GetPath(),
      url,
      identifier,
      alias: `${name}Link`,
    });
  }

  AddCall({name, url, identifier, file})
  {
    this.calls.push({
      name,
      // url: Utility.String.KebabCase(path.GetPath().replace(/\.js/, "")),
      url,
      identifier,
    });
  }

  async FindCalls(root)
  {
    const directory = new Directory(root);
    const files = await directory.GetAllChildFiles();

    console.log(files);
  }
}

async function Test()
{
  // const compiler = new ImportsCompiler();
  const compiler = new ImportsCompiler({
    root: "C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/BeFitBeyondFifty",
    input: "./Client/js/Main.js",
    output: "../taggly/Client/js/Bundle.js",
    paths: [
      "./Client",
      "../taggly/Client",
    ],
  });

  compiler.AddLink({
    name: "Home",
    url: "/js/Pages/Home.js",
    identifier: "Home",
  });

  compiler.AddCall({
    url: "/api/user/create",
    identifier: "API.User.Create",
  });

  // const code = await compiler.Compile("C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/taggly/Client/js/Imports.js");
  const code = await compiler.Compile("./Server/Compiler/ImportsTemplate.js");

  console.log("\n~~~RESULTS~~~");
  console.log(code);

  // const ast = await compiler.Parse("C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/taggly/Client/js/Imports.js");
  //
  // await compiler.Transform(ast);
  // await compiler.Compile(ast);
}

async function Test()
{
  const root = "C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/BeFitBeyondFifty";
  const client_root = root + "/Client";
  const controller_root = root + "/Server/Controller";

  // const client_directory = new Directory(client_root);
  // const controller_directory = new Directory(controller_root);

  const compiler = new ImportsCompiler({
    root: "C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/BeFitBeyondFifty",
    // root: __dirname,
    input: "../taggly/Server/Compiler/ImportsTemplate.js",
    output: "../taggly/Client/js/Imports.js",
    paths: [
      // "./Client",
      // "../taggly/Client",
    ],
  });

  compiler.AddLink({
    name: "Home",
    url: "/js/Pages/Home.js",
    // path: new Path(),
    identifier: "Home",
  });

  compiler.AddCall({
    name: "Create",
    url: "/api/user/create",
    // path: new Path(),
    identifier: "API.User.Create",
  });

  // if (client_directory)
  // {
  //   const files = await compiler.FindFiles(client_directory, pages);
  //   files.forEach(f => compiler.AddLink(f));
  // }
  //
  // if (controller_directory)
  // {
  //   const files = await compiler.FindFiles(controller_directory, controllers);
  //   files.forEach(f => compiler.AddCall(f));
  // }

  const code = await compiler.Compile();

  console.log("\n~~~RESULTS~~~");
  console.log(code);
}

// Test();

module.exports.ImportsCompiler = ImportsCompiler;
module.exports.default = ImportsCompiler;

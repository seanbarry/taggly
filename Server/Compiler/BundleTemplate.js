const modules = {};
const cache = {};

function require(path)
{
  if (cache[path]) return cache[path];

  if (modules[path]) return cache[path] = modules[path]();
  else throw new Error(`Failed to find module from "${path}"`);
}

require.modules = modules;
require.cache = cache;

const modules = {};
const cache = {};

function require(path)
{
  if (cache[path]) return cache[path];

  if (modules[path]) return cache[path] = modules[path]();
  else throw new Error(`Failed to find module from "${path}"`);
}

require.modules = modules;
require.cache = cache;

modules["/js/Main.js"] = function()
{
  const {App} = require("App");
  const {Tag} = require("Tag");

  class BeFitBeyondFifty extends App
  {
    constructor()
    {
      super();
    }

    RenderPages(tag)
    {
      return tag.Clear().Add(
      );
    }

    RenderNotifications(tag)
    {
      return tag.Clear().Add(
      );
    }

    Render()
    {
      return super.Render(
        Tag.Div("pages").Bind(this, "RenderPages"),
        Tag.Div("notifications").Bind(this, "RenderNotifications"),
      );
    }
  }

  return {
    BeFitBeyondFifty,
  };
}

modules["/js/Utility/Connection.js"] = function()
{
  class Connection
  {
    constructor(path)
    {
      this.path = path;
      this.parts = path.split(".");
    }
  }

  class Link extends Connection
  {
    constructor(path)
    {
      super(path);
    }
  }

  class Call extends Connection
  {
    constructor(path)
    {
      super(path);
    }
  }

  return {
    Connection,
    Link,
    Call,
  };
}

const {Link, Call} = require("/js/Utility/Connection.js");
console.log(Link, Call);

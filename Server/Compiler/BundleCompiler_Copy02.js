"use strict";

// const babel = require("@babel/core");
const traverse = require("@babel/traverse").default;
// const types = require("@babel/types");
// const {parse} = require("@babel/parser");
// const generate = require("@babel/generator").default;

const fs = require("fs");

const {File, Directory, Path} = require("filesystem");
const {Utility} = require("debug");
const {Compiler} = require("./Compiler.js");

class AST
{
  constructor(compiler, ast, url)
  {
    this.compiler = compiler;
    this.ast = ast;
    this.url = url;
    // this.current = ast;
    this.Call(ast);
  }

  Call(node)
  {
    this.parent = this.current;
    this.current = node;
    if (this[node.type]) this[node.type](node);

    return this;
  }

  Parse(url)
  {
    const file = this.compiler.Resolve(url);
    return this.compiler.Parse(file);
  }

  File({program}){ return this.Call(program); }
  Program({body}){ body.forEach(b => this.Call(b)); }
}

class Module extends AST
{
  // constructor(ast)
  // {
  //   super(ast);
  // }

  ImportDeclaration({specifiers, source})
  {
    console.log("Import declaration!", source);
  }

  ExportDefaultDeclaration({declaration})
  {
    return this.Call(declaration);
  }
}

class BundleCompiler extends Compiler
{
  constructor(options)
  {
    super(options);

    const imports = {};
    const modules = {};

    const body = [];

    // this.AddPlugin({
    //   ImportDeclaration: (path) =>
    //   {
    //     const types = this.GetBabelTypes();
    //     const url = path.node.source.value;
    //
    //     let ast;
    //     if (imports[url] === undefined)
    //     {
    //       imports[url] = true;
    //       console.log("Loading", url);
    //
    //       const file = this.Resolve(url);
    //
    //       if (file)
    //       {
    //         ast = this.Parse({file});
    //         imports[url] = {
    //           ast,
    //           url,
    //         };
    //       }
    //       else
    //       {
    //         // throw new Error(`Failed to import from "${url}"`);
    //         console.log("Skipping", url);
    //         return;
    //       }
    //     }
    //     else
    //     {
    //       ast = imports[url].ast;
    //     }
    //
    //     // const parent = path.findParent(p => p.isProgram());
    //
    //     // console.log("Parent:", path.getFunctionParent());
    //
    //     // path.getFunctionParent().insertBefore(
    //     //   types.expressionStatement(
    //     //     types.assignmentExpression(
    //     //       "=",
    //     //       types.memberExpression(
    //     //         types.identifier("modules"),
    //     //         types.stringLiteral(url),
    //     //         true,
    //     //       ),
    //     //       types.functionExpression(
    //     //         null, // id
    //     //         // types.identifier(),
    //     //         [],
    //     //         types.blockStatement(ast.program.body),
    //     //         false, // Generator
    //     //         false, // Async
    //     //       ),
    //     //     ),
    //     //   ),
    //     // );
    //
    //     path.replaceWithMultiple([
    //       // types.expressionStatement(
    //       //   types.assignmentExpression(
    //       //     "=",
    //       //     types.memberExpression(
    //       //       types.identifier("modules"),
    //       //       types.stringLiteral(url),
    //       //       true,
    //       //     ),
    //       //     types.functionExpression(
    //       //       null, // id
    //       //       // types.identifier(),
    //       //       [],
    //       //       types.blockStatement(ast.program.body),
    //       //       false, // Generator
    //       //       false, // Async
    //       //     ),
    //       //   ),
    //       // ),
    //
    //       types.variableDeclaration(
    //         "const",
    //         [
    //           types.variableDeclarator(
    //             types.objectPattern(
    //               path.node.specifiers.map(s =>
    //               {
    //                 return types.objectProperty(s.imported, s.imported);
    //               }),
    //             ),
    //             types.callExpression(
    //               types.identifier("require"), // Callee
    //               [
    //                 types.stringLiteral(url),
    //               ],
    //             ),
    //           ),
    //         ],
    //       ),
    //     ]);
    //
    //     // path.replaceWith(
    //     //   types.variableDeclaration(
    //     //     "const",
    //     //     [
    //     //       types.variableDeclarator(
    //     //         types.objectPattern(
    //     //           path.node.specifiers.map(s =>
    //     //           {
    //     //             return types.objectProperty(s.imported, s.imported);
    //     //           }),
    //     //         ),
    //     //         types.callExpression(
    //     //           types.identifier("require"), // Callee
    //     //           [
    //     //             types.stringLiteral(url),
    //     //           ],
    //     //         ),
    //     //       ),
    //     //     ],
    //     //   ),
    //     // );
    //   },
    // });

    const ImportDeclaration = (path) =>
    {
      const types = this.GetBabelTypes();
      const url = path.node.source.value;

      // path.replaceWith(
      //   types.variableDeclaration(
      //     "const",
      //     [
      //       types.variableDeclarator(
      //         types.objectPattern(
      //           path.node.specifiers.map(s =>
      //           {
      //             return types.objectProperty(s.imported, s.imported);
      //           }),
      //         ),
      //         types.callExpression(
      //           types.identifier("require"), // Callee
      //           [
      //             types.stringLiteral(url),
      //           ],
      //         ),
      //       ),
      //     ],
      //   ),
      // );

      let ast;
      if (imports[url] === undefined)
      {
        // imports[url] = true;
        console.log("Loading", url);

        const file = this.Resolve(url);

        if (file)
        {
          ast = this.Parse({file});
          modules[url] = new Module(this, ast, url);

          imports[url] = {
            ast,
            url,
          };
        }
        else
        {
          // throw new Error(`Failed to import from "${url}"`);
          console.log("Skipping", url);
          return;
        }
      }
      else
      {
        ast = imports[url].ast;
      }

      this.Traverse(ast, {
        ImportDeclaration,
      });
    };

    const types = this.GetBabelTypes();

    this.AddPlugin({
      ImportDeclaration,
      // ExportDefaultDeclaration: (path) =>
      // {
      //   let declaration = path.node.declaration;
      //   let id;
      //
      //   if (declaration.id === null)
      //   {
      //     id = types.identifier("_default");
      //
      //     declaration = types.assignmentExpression(
      //       "=",
      //       id,
      //       declaration,
      //     );
      //   }
      //   else
      //   {
      //     id = declaration.id;
      //   }
      //
      //   const id = path.node.declaration.id;
      //   // path.remove();
      //   path.replaceWithMultiple([
      //     declaration,
      //     types.returnStatement(
      //       types.objectExpression([
      //         types.objectProperty(
      //           types.identifier("default"),
      //           id,
      //         ),
      //       ]),
      //     ),
      //   ]);
      // },
      // ExportNamedDeclaration: (path) =>
      // {
      //   path.replaceWithMultiple([
      //     path.node.declaration,
      //     types.returnStatement(
      //       types.objectExpression([
      //         types.objectProperty(
      //           path.node.declaration.id,
      //           path.node.declaration.id,
      //         ),
      //       ]),
      //     ),
      //   ]);
      // },
      Program: {
        enter: (path) =>
        {
          console.log("~~PROGRAM ENTER~~");

          const types = this.GetBabelTypes();

          // const names = Object.getOwnPropertyNames(path.constructor.prototype);
          // for (let i = 0; i < names.length; i++)
          // {
          //   // const item = names[i];
          //   console.log(i, names[i]);
          // }

          const ast = this.Parse({
            path: "../taggly/Server/Compiler/BundleTemplate.js",
          });

          body.push.apply(body, ast.program.body);

          // ast.program.body.reverse().forEach(node =>
          // {
          //   path.unshiftContainer(
          //     "body",
          //     node,
          //   );
          // });

          // const imports = {};
          // function Traverse(ast)
          // {
          //   traverse(ast, {
          //     ImportDeclaration: (path) =>
          //     {
          //
          //     },
          //   });
          // }
          //
          // Traverse(ast);
        },
        exit: (path) =>
        {
          console.log("~~PROGRAM EXIT~~");

          const types = this.GetBabelTypes();

          for (const key in imports)
          {
            if (!imports.hasOwnProperty(key)) continue;
            const {ast, url} = imports[key];

            console.log("Loading", url);

            body.push(
              types.expressionStatement(
                types.assignmentExpression(
                  "=",
                  types.memberExpression(
                    types.identifier("modules"),
                    types.stringLiteral(url),
                    true,
                  ),
                  types.functionExpression(
                    null, // id
                    [],
                    types.blockStatement(ast.program.body),
                    false, // Generator
                    false, // Async
                  ),
                ),
              ),
            );
          }

          body.reverse().forEach(node =>
          {
            path.unshiftContainer(
              "body",
              node,
            );
          });
        },
      },
    });

    this.AddPreset("@babel/preset-env", {
      targets: "> 0.25%, not dead",
    });
  }
}

async function Test()
{
  const compiler = new BundleCompiler();

  compiler.AddLink({
    name: "Home",
    url: "/js/Pages/Home.js",
    identifier: "Home",
  });

  compiler.AddCall({
    url: "/api/user/create",
    identifier: "API.User.Create",
  });

  // const code = await compiler.Compile("C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/taggly/Client/js/Imports.js");
  const code = await compiler.Compile("./Server/Compiler/ImportsTemplate.js");

  console.log("\n~~~RESULTS~~~");
  console.log(code);

  // const ast = await compiler.Parse("C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/taggly/Client/js/Imports.js");
  //
  // await compiler.Transform(ast);
  // await compiler.Compile(ast);
}

async function Test()
{
  const root = "C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/BeFitBeyondFifty";

  const paths = [
    "C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/BeFitBeyondFifty/Client",
    "C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/taggly/Client",
  ];

  const compiler = new BundleCompiler(paths);
  // const code = await compiler.Compile(root + "/Client/js/Main.js");

  // const file = File.New("./Server/Compiler/BundleTemplate.js");
  const file = File.New("C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/BeFitBeyondFifty/Client/js/Main.js");
  const code = await compiler.Compile(file);

  // console.log("\n~~~RESULTS~~~");
  // console.log(code);

  fs.writeFile("C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/taggly/Client/js/Bundle.js", code, (error) =>
  {
    if (error) throw error;

    console.log("Done writing");
  });
}

async function Test()
{
  const root = "C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/BeFitBeyondFifty";

  const compiler = new BundleCompiler({
    root: "C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/BeFitBeyondFifty",
    input: "./Client/js/Main.js",
    output: "",
    paths: [
      "./Client",
      "../taggly/Client",
    ],
  });
  // const code = await compiler.Compile(root + "/Client/js/Main.js");

  // const file = File.New("./Server/Compiler/BundleTemplate.js");
  const file = File.New("C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/BeFitBeyondFifty/Client/js/Main.js");
  // const ast = await compiler.Parse(file);
  // console.log(ast);

  const files = {};
  function Traverse(ast)
  {
    traverse(ast, {
      ImportDeclaration(path)
      {
        console.log("Import");
        const url = path.node.source.value;

        let file;
        for (let i = 0; i < paths.length; i++)
        {
          const temp_file = new File(paths[i] + url);
          if (temp_file.IsValid())
          {
            file = temp_file;
            break;
          }
        }

        if (!file)
        {
          console.log("No file for", url);
          return;
        }

        const filepath = file.GetPath();
        if (files[filepath]) return;

        files[filepath] = compiler.ParseSync(file);
        Traverse(files[filepath]);
      },
    });
  }

  // Traverse(ast);

  // traverse(ast, {
  //   Program(path)
  //   {
  //     for (const key in files)
  //     {
  //       if (!files.hasOwnProperty(key)) continue;
  //       const value = files[key];
  //
  //       console.log(value.program.body);
  //
  //       // path.unshiftContainer(
  //       //   "body",
  //       //   // types.program(value.program.body),
  //       //   types.declareModule(
  //       //     types.identifier("_GENERATED_"),
  //       //     value.program.body,
  //       //     "CommonJS",
  //       //   ),
  //       //   // value.program,
  //       //   // types.expressionStatement(
  //       //   //   types.blockStatement(value.program.body),
  //       //   // ),
  //       //   // value.program.body,
  //       // );
  //
  //       // path.unshiftContainer(
  //       //   "body",
  //       //   types.expressionStatement(
  //       //     types.callExpression(
  //       //       types.functionExpression(
  //       //         // null, // id
  //       //         types.identifier("_GENERATED_"),
  //       //         [],
  //       //         types.blockStatement(value.program.body),
  //       //         // types.blockStatement([]),
  //       //         false, // Generator
  //       //         false, // Async
  //       //       ),
  //       //       [],
  //       //     ),
  //       //   ),
  //       // );
  //     }
  //   },
  // });

  // traverse(ast, {
  //   ImportDeclaration(path)
  //   {
  //     console.log("Import");
  //     const url = path.node.source.value;
  //
  //     let file;
  //     for (let i = 0; i < paths.length; i++)
  //     {
  //       const temp_file = new File(paths[i] + url);
  //       if (temp_file.IsValid())
  //       {
  //         file = temp_file;
  //         break;
  //       }
  //     }
  //
  //     if (!file)
  //     {
  //       console.log("No file for", url);
  //       return;
  //     }
  //
  //     const ast = compiler.ParseSync(file);
  //
  //     path.insertBefore(
  //       types.expressionStatement(
  //         types.callExpression(
  //           types.functionExpression(
  //             // null, // id
  //             types.identifier("_GENERATED_"),
  //             [],
  //             types.blockStatement(ast.program.body),
  //             false, // Generator
  //             false, // Async
  //           ),
  //           [],
  //         ),
  //       ),
  //     );
  //   },
  //   exit(path)
  //   {
  //     if (path.node.type === "Program")
  //     {
  //       console.log("~~PROGRAM~~");
  //     }
  //   },
  // });

  const main_code = await file.Read();
  const ast = await babel.parse(main_code, {
    presets: [
      ["@babel/preset-env", {
        // targets: {
        //   "ie": "9",
        // },
        targets: "> 0.25%, not dead",
      }],
    ],
  });

  // console.log(ast.program.body);

  const {code} = await babel.transformFromAstAsync(ast, {
    presets: [
      ["@babel/preset-env", {
        // targets: {
        //   "ie": "9",
        // },
        targets: "> 0.25%, not dead",
      }],
    ],
  });

  console.log(code);

  fs.writeFile("C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/taggly/Client/js/Bundle.js", code, (error) =>
  {
    if (error) throw error;

    console.log("Done writing");
  });
}

async function Test()
{
  const compiler = new BundleCompiler({
    root: "C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/BeFitBeyondFifty",
    input: "./Client/js/Main.js",
    output: "../taggly/Client/js/Bundle.js",
    paths: [
      "./Client",
      "../taggly/Client",
    ],
  });

  const code = await compiler.Compile();

  // console.log(code);

  // await compiler.Save(code);
}

Test();

module.exports.BundleCompiler = BundleCompiler;
module.exports.default = BundleCompiler;

"use strict";

const babel = require("@babel/core");
const traverse = require("@babel/traverse").default;
const types = require("@babel/types");
const parser = require("@babel/parser");
const generate = require("@babel/generator").default;

const {File, Directory} = require("filesystem");
// const browserify = require("browserify");
const fs = require("fs");
const path = require("path");

class Compiler
{
  constructor({
    root,
    input,
    output,
    paths,
  })
  {
    this.root = new Directory(root);
    this.input = input;
    this.output = output;
    this.paths = paths;

    this.types = null;
    this.plugins = [
      // "@babel/plugin-transform-runtime",
    ];
    this.presets = [];
    this.cached = {};
  }

  AddPlugin(plugin)
  {
    this.plugins.push(babel =>
    {
      this.types = babel.types;
      return {
        visitor: plugin,
      };
    });
  }

  AddPreset(preset, options)
  {
    this.presets.push([preset, options]);
  }

  async Parse(path)
  {
    const file = new File(path);
    const code = await file.Read();

    const ast = await babel.parseAsync(code);
    return ast;
  }

  async Parse(file)
  {
    const code = await file.Read();

    const result = await babel.parse(code, {
      // plugins: [this.GetPlugin()],
      plugins: this.plugins,
      presets: this.presets,
    });

    return result;
  }

  Parse({code, file, path})
  {
    if (path && !file)
    {
      path = this.root.Resolve(path);
      file = new File(path);
    }

    if (file && !code) code = file.ReadSync();

    const result = parser.parse(code, {
      sourceType: "module",
    });

    return result;
  }

  Parse(file)
  {
    let cached = this.cached[file.GetPath()];
    if (cached) return cached;

    const code = file.ReadSync();

    const ast = parser.parse(code, {
      sourceType: "module",
    });

    this.cached[file.GetPath()] = ast;

    return ast;
  }

  Traverse(ast, plugin)
  {
    return traverse(ast, plugin);
  }

  Generate({
    ast,
    sources,
    pre_comment,
    post_comment,
    compact = false,
    minified = false,
    filename,
    root,
  })
  {
    const {code, map} = generate(ast, {
      auxiliaryCommentBefore: pre_comment,
      auxiliaryCommentAfter: post_comment,
      retainLines: false,
      retainFunctionParens: false,
      comments: true,
      compact, // Add whitespace for formatting
      minified,
      filename,
      jsonCompatibleStrings: false,
      sourceMaps: false,
      sourceRoot: root,
      sourceFileName: filename,
    }, sources);

    return code;
  }

  // ParseSync(file)
  // {
  //   const code = file.ReadSync();
  //
  //   const result = babel.parseSync(code, {
  //     // plugins: [this.GetPlugin()],
  //     plugins: this.plugins,
  //     presets: this.presets,
  //   });
  //
  //   return result;
  // }

  Resolve(path)
  {
    if (!path.startsWith(".")) path = "." + path;

    let file;
    for (let i = 0; i < this.paths.length; i++)
    {
      const resolved = this.root.Resolve(this.paths[i], path);

      const temp = new File(resolved);
      if (temp.IsValid())
      {
        file = temp;
        break;
      }
    }

    return file;
  }

  async Compile()
  {
    const file = await this.root.GetChild(this.input);
    const code = await file.Read();

    const result = await babel.transform(code, {
      // plugins: [this.GetPlugin()],
      plugins: this.plugins,
      presets: this.presets,
      // optional: ["runtime"],
    });

    return result.code;
  }

  async Save(code)
  {
    const file = await this.root.GetChild(this.output);
    return file.Write(code);
  }

  CompileSync(file)
  {
    // const code = await file.Read();

    const result = babel.transformFileSync(file.GetPath(), {
      // plugins: [this.GetPlugin()],
      plugins: this.plugins,
      presets: this.presets,
    });

    return result.code;
  }

  GetBabelTypes(){ return babel.types; }

  GetPlugin()
  {
    return ({types}) =>
    {
      this.types = types;
      return {
        visitor: this.plugin,
      };
    };
  }
}

module.exports.Compiler = Compiler;
module.exports.default = Compiler;

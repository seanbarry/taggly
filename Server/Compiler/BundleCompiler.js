"use strict";

// const babel = require("@babel/core");
const traverse = require("@babel/traverse").default;
const types = require("@babel/types");
// const {parse} = require("@babel/parser");
// const generate = require("@babel/generator").default;

const fs = require("fs");

const {File, Directory, Path} = require("filesystem");
const {Utility} = require("debug");
const {Compiler} = require("./Compiler.js");

class AST
{
  constructor(compiler, ast, url)
  {
    this.compiler = compiler;
    this.ast = ast;
    this.url = url;
    this.imports = [];
    this.exports = {};
    // this.current = ast;
    this.Call(ast);
  }

  Call(node, parent = this.current)
  {
    this.parent = parent;
    this.current = node;
    console.log("Calling", node.type);
    if (this[node.type]) this[node.type](node, this.parent);

    return this;
  }

  AddImport(url)
  {
    const file = this.compiler.Resolve(url);
    if (file)
    {
      const child = new this.constructor(this.compiler, this.compiler.Parse(file), url);

      this.imports.push(child);
      return child;
    }
    else
    {
      console.log("Skipping", url);
    }
  }

  AddExport(name, declaration)
  {
    console.log("Exporting", name);
    this.exports[name] = declaration;
  }

  File(file){ return this.Call(file.program, file); }

  Program(program)
  {
    program.body.forEach(b => this.Call(b, program));
  }

  CollectImports(imports = {})
  {
    for (let i = 0; i < this.imports.length; i++)
    {
      const url = this.imports[i].url;
      // imports.push.apply(imports, this.imports);

      // imports[url] = this.imports[i].ast.program.body;
      if (!imports[url])
      {
        console.log(i, "Collecting", url);
        imports[url] = this.imports[i];
      }

      // imports.push.apply(imports, this.imports[i].ast.program.body);
      this.imports[i].CollectImports(imports);
    }

    return imports;
  }
}

class Module extends AST
{
  // Program(program)
  // {
  //   super.Program(program);
  //
  //   program.body = [
  //     types.expressionStatement(
  //       types.assignmentExpression(
  //         "=",
  //         types.memberExpression(
  //           types.identifier("modules"),
  //           types.stringLiteral(this.url),
  //           true,
  //         ),
  //         types.functionExpression(
  //           null, // id
  //           [],
  //           types.blockStatement(program.body),
  //           false, // Generator
  //           false, // Async
  //         ),
  //       ),
  //     ),
  //   ];
  // }

  ImportDeclaration(import_declaration, parent)
  {
    const {specifiers, source} = import_declaration;

    console.log("Import declaration", source.value);

    const i = parent.body.indexOf(import_declaration);
    if (i === -1) throw new Error("Failed to remove import!");

    const mod = this.AddImport(source.value);
    if (!mod) return;

    parent.body[i] =
    types.variableDeclaration(
      "const",
      [
        types.variableDeclarator(
          types.objectPattern(
            specifiers.map(s =>
            {
              return types.objectProperty(s.imported, s.imported);
            }),
          ),
          types.callExpression(
            types.identifier("require"), // Callee
            [
              types.stringLiteral(mod.url), // Arg
            ],
          ),
        ),
      ],
    );
  }

  ExportNamedDeclaration(export_declaration, parent)
  {
    const {declaration} = export_declaration;

    this.AddExport(declaration.id.name, declaration);

    const i = parent.body.indexOf(export_declaration);
    if (i === -1) throw new Error("Failed to remove named export!");
    parent.body[i] = declaration;

    return this.Call(declaration);
  }

  ExportDefaultDeclaration(export_declaration, parent)
  {
    const {declaration} = export_declaration;

    let name = "default";
    if (declaration.id) name = declaration.id.name;

    this.AddExport(name, declaration);

    const i = parent.body.indexOf(export_declaration);
    if (i === -1) throw new Error("Failed to remove named export!");
    parent.body[i] = declaration;

    return this.Call(declaration);
  }
}

class BundleCompiler extends Compiler
{
  constructor(options)
  {
    super(options);

    const imports = {};
    const modules = {};

    const body = [];

    // this.AddPlugin({
    //   ImportDeclaration: (path) =>
    //   {
    //     const types = this.GetBabelTypes();
    //     const url = path.node.source.value;
    //
    //     let ast;
    //     if (imports[url] === undefined)
    //     {
    //       imports[url] = true;
    //       console.log("Loading", url);
    //
    //       const file = this.Resolve(url);
    //
    //       if (file)
    //       {
    //         ast = this.Parse({file});
    //         imports[url] = {
    //           ast,
    //           url,
    //         };
    //       }
    //       else
    //       {
    //         // throw new Error(`Failed to import from "${url}"`);
    //         console.log("Skipping", url);
    //         return;
    //       }
    //     }
    //     else
    //     {
    //       ast = imports[url].ast;
    //     }
    //
    //     // const parent = path.findParent(p => p.isProgram());
    //
    //     // console.log("Parent:", path.getFunctionParent());
    //
    //     // path.getFunctionParent().insertBefore(
    //     //   types.expressionStatement(
    //     //     types.assignmentExpression(
    //     //       "=",
    //     //       types.memberExpression(
    //     //         types.identifier("modules"),
    //     //         types.stringLiteral(url),
    //     //         true,
    //     //       ),
    //     //       types.functionExpression(
    //     //         null, // id
    //     //         // types.identifier(),
    //     //         [],
    //     //         types.blockStatement(ast.program.body),
    //     //         false, // Generator
    //     //         false, // Async
    //     //       ),
    //     //     ),
    //     //   ),
    //     // );
    //
    //     path.replaceWithMultiple([
    //       // types.expressionStatement(
    //       //   types.assignmentExpression(
    //       //     "=",
    //       //     types.memberExpression(
    //       //       types.identifier("modules"),
    //       //       types.stringLiteral(url),
    //       //       true,
    //       //     ),
    //       //     types.functionExpression(
    //       //       null, // id
    //       //       // types.identifier(),
    //       //       [],
    //       //       types.blockStatement(ast.program.body),
    //       //       false, // Generator
    //       //       false, // Async
    //       //     ),
    //       //   ),
    //       // ),
    //
    //       types.variableDeclaration(
    //         "const",
    //         [
    //           types.variableDeclarator(
    //             types.objectPattern(
    //               path.node.specifiers.map(s =>
    //               {
    //                 return types.objectProperty(s.imported, s.imported);
    //               }),
    //             ),
    //             types.callExpression(
    //               types.identifier("require"), // Callee
    //               [
    //                 types.stringLiteral(url),
    //               ],
    //             ),
    //           ),
    //         ],
    //       ),
    //     ]);
    //
    //     // path.replaceWith(
    //     //   types.variableDeclaration(
    //     //     "const",
    //     //     [
    //     //       types.variableDeclarator(
    //     //         types.objectPattern(
    //     //           path.node.specifiers.map(s =>
    //     //           {
    //     //             return types.objectProperty(s.imported, s.imported);
    //     //           }),
    //     //         ),
    //     //         types.callExpression(
    //     //           types.identifier("require"), // Callee
    //     //           [
    //     //             types.stringLiteral(url),
    //     //           ],
    //     //         ),
    //     //       ),
    //     //     ],
    //     //   ),
    //     // );
    //   },
    // });

    const ImportDeclaration = (path) =>
    {
      const types = this.GetBabelTypes();
      const url = path.node.source.value;

      // path.replaceWith(
      //   types.variableDeclaration(
      //     "const",
      //     [
      //       types.variableDeclarator(
      //         types.objectPattern(
      //           path.node.specifiers.map(s =>
      //           {
      //             return types.objectProperty(s.imported, s.imported);
      //           }),
      //         ),
      //         types.callExpression(
      //           types.identifier("require"), // Callee
      //           [
      //             types.stringLiteral(url),
      //           ],
      //         ),
      //       ),
      //     ],
      //   ),
      // );

      let ast;
      if (imports[url] === undefined)
      {
        // imports[url] = true;
        console.log("Loading", url);

        const file = this.Resolve(url);

        if (file)
        {
          ast = this.Parse({file});
          modules[url] = new Module(this, ast, url);

          imports[url] = {
            ast,
            url,
          };
        }
        else
        {
          // throw new Error(`Failed to import from "${url}"`);
          console.log("Skipping", url);
          return;
        }
      }
      else
      {
        ast = imports[url].ast;
      }

      this.Traverse(ast, {
        ImportDeclaration,
      });
    };

    const types = this.GetBabelTypes();

    this.AddPlugin({
      // ImportDeclaration,
      // ExportDefaultDeclaration: (path) =>
      // {
      //   let declaration = path.node.declaration;
      //   let id;
      //
      //   if (declaration.id === null)
      //   {
      //     id = types.identifier("_default");
      //
      //     declaration = types.assignmentExpression(
      //       "=",
      //       id,
      //       declaration,
      //     );
      //   }
      //   else
      //   {
      //     id = declaration.id;
      //   }
      //
      //   const id = path.node.declaration.id;
      //   // path.remove();
      //   path.replaceWithMultiple([
      //     declaration,
      //     types.returnStatement(
      //       types.objectExpression([
      //         types.objectProperty(
      //           types.identifier("default"),
      //           id,
      //         ),
      //       ]),
      //     ),
      //   ]);
      // },
      // ExportNamedDeclaration: (path) =>
      // {
      //   path.replaceWithMultiple([
      //     path.node.declaration,
      //     types.returnStatement(
      //       types.objectExpression([
      //         types.objectProperty(
      //           path.node.declaration.id,
      //           path.node.declaration.id,
      //         ),
      //       ]),
      //     ),
      //   ]);
      // },
      Program: {
        enter: (path) =>
        {
          console.log("~~PROGRAM ENTER~~");

          const file = new File("../taggly/Server/Compiler/BundleTemplate.js");
          const ast = this.Parse(file);

          const main = new Module(this, path.node, "/js/Main.js");

          const imports = main.CollectImports();

          const body = ast.program.body;
          for (const key in imports)
          {
            if (!imports.hasOwnProperty(key)) continue;
            // const imp = imports[key];
            const {ast, url, exports} = imports[key];

            const export_array = [];
            for (const key in exports)
            {
              if (!exports.hasOwnProperty(key)) continue;
              const value = exports[key];

              console.log("Export", key, value.id.name);

              export_array.push(
                types.objectProperty(
                  types.identifier(key),
                  value.id,
                ),
              );
            }

            body.push.apply(body, [
              types.expressionStatement(
                types.assignmentExpression(
                  "=",
                  types.memberExpression(
                    types.identifier("modules"),
                    types.stringLiteral(url),
                    true,
                  ),
                  types.functionExpression(
                    null, // id
                    [],
                    types.blockStatement([
                      ...ast.program.body,
                      types.returnStatement(
                        types.objectExpression(export_array),
                      ),
                    ]),
                    false, // Generator
                    false, // Async
                  ),
                ),
              ),
            ]);
          }

          body.reverse().forEach(node =>
          {
            path.unshiftContainer(
              "body",
              node,
            );
          });

          // for (let i = 0; i < main.imports.length; i++)
          // {
          //   // console.log(i, "import", main.imports[i]);
          //   main.ast.body = [
          //     ...body,
          //     // ...imports,
          //     // ...imports.map(m => m.ast.program.body),
          //     // ...main.imports[i].ast.program.body,
          //     // ...main.ast.body,
          //   ];
          //   // const item = main.imports[i];
          //   // main.ast.file.program.body.shift(main.imports[i].ast.);
          // }

          // console.log(main.ast.file.program);


          // const names = Object.getOwnPropertyNames(path.constructor.prototype);
          // for (let i = 0; i < names.length; i++)
          // {
          //   // const item = names[i];
          //   console.log(i, names[i]);
          // }

          // body.push.apply(body, ast.program.body);
        },
        exit: (path) =>
        {
          console.log("~~PROGRAM EXIT~~");

          // const types = this.GetBabelTypes();
          //
          // for (const key in imports)
          // {
          //   if (!imports.hasOwnProperty(key)) continue;
          //   const {ast, url} = imports[key];
          //
          //   console.log("Loading", url);
          //
          //   body.push(
          //     types.expressionStatement(
          //       types.assignmentExpression(
          //         "=",
          //         types.memberExpression(
          //           types.identifier("modules"),
          //           types.stringLiteral(url),
          //           true,
          //         ),
          //         types.functionExpression(
          //           null, // id
          //           [],
          //           types.blockStatement(ast.program.body),
          //           false, // Generator
          //           false, // Async
          //         ),
          //       ),
          //     ),
          //   );
          // }
          //
          // body.reverse().forEach(node =>
          // {
          //   path.unshiftContainer(
          //     "body",
          //     node,
          //   );
          // });
        },
      },
    });

    this.AddPreset("@babel/preset-env", {
      targets: "> 0.25%, not dead",
    });
  }
}

async function Test()
{
  const compiler = new BundleCompiler({
    root: "C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/BeFitBeyondFifty",
    input: "./Client/js/Main.js",
    output: "../taggly/Client/js/Bundle.js",
    paths: [
      "./Client",
      "../taggly/Client",
    ],
  });

  const code = await compiler.Compile();

  // console.log(code);

  await compiler.Save(code);
}

Test();

module.exports.BundleCompiler = BundleCompiler;
module.exports.default = BundleCompiler;

export class Imports
{
  constructor()
  {
    this.links = [];
    this.calls = [];
  }

  AddLink(mod, path)
  {
    this.links.push({
      module: mod,
      path,
    });
  }

  AddCall(url, path)
  {
    this.calls.push({
      url,
      path,
    });
  }
}

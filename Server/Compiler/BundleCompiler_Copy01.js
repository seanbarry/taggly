"use strict";

const babel = require("@babel/core");
const traverse = require("@babel/traverse").default;
const types = require("@babel/types");

const fs = require("fs");

const {File, Directory, Path} = require("filesystem");
const {Utility} = require("debug");
const {Compiler} = require("./Compiler.js");

class BundleCompiler extends Compiler
{
  constructor(paths)
  {
    super();

    this.paths = paths;

    const imports = {};

    this.AddPlugin({
      _Program: (path) =>
      {
        console.log("~~PROGRAM~~");
        // this.program = path;
      },
      _ImportDeclaration: (path) =>
      {
        const types = this.GetBabelTypes();
        const body = [];
        const url = path.node.source.value;

        // console.log("Import!", url, imports);

        if (!imports[url])
        {
          imports[url] = true;
          console.log("Loading", url);

          let file;
          for (let i = 0; i < paths.length; i++)
          {
            const temp_file = new File(paths[i] + url);
            if (temp_file.IsValid())
            {
              file = temp_file;
              break;
            }
          }

          if (file)
          {
            // console.log("Has program?", this.program);
            imports[url] = this.ParseSync(file);
            // console.log(imports[url]);

            path.insertBefore(
              types.expressionStatement(
                types.callExpression(
                  types.functionExpression(
                    // null, // id
                    types.identifier("_GENERATED_"),
                    [],
                    types.blockStatement(imports[url].program.body),
                    false, // Generator
                    false, // Async
                  ),
                  [],
                ),
              ),
            );
          }
        }
      },
    });

    this.AddPreset("@babel/preset-env", {
      targets: "> 0.25%, not dead",
    });
  }
}

async function Test()
{
  const compiler = new BundleCompiler();

  compiler.AddLink({
    name: "Home",
    url: "/js/Pages/Home.js",
    identifier: "Home",
  });

  compiler.AddCall({
    url: "/api/user/create",
    identifier: "API.User.Create",
  });

  // const code = await compiler.Compile("C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/taggly/Client/js/Imports.js");
  const code = await compiler.Compile("./Server/Compiler/ImportsTemplate.js");

  console.log("\n~~~RESULTS~~~");
  console.log(code);

  // const ast = await compiler.Parse("C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/taggly/Client/js/Imports.js");
  //
  // await compiler.Transform(ast);
  // await compiler.Compile(ast);
}

async function Test()
{
  const root = "C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/BeFitBeyondFifty";

  const paths = [
    "C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/BeFitBeyondFifty/Client",
    "C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/taggly/Client",
  ];

  const compiler = new BundleCompiler(paths);
  // const code = await compiler.Compile(root + "/Client/js/Main.js");

  // const file = File.New("./Server/Compiler/BundleTemplate.js");
  const file = File.New("C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/BeFitBeyondFifty/Client/js/Main.js");
  const code = await compiler.Compile(file);

  // console.log("\n~~~RESULTS~~~");
  // console.log(code);

  fs.writeFile("C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/taggly/Client/js/Bundle.js", code, (error) =>
  {
    if (error) throw error;

    console.log("Done writing");
  });
}

async function Test()
{
  const root = "C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/BeFitBeyondFifty";

  const paths = [
    "C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/BeFitBeyondFifty/Client",
    "C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/taggly/Client",
  ];

  const compiler = new BundleCompiler(paths);
  // const code = await compiler.Compile(root + "/Client/js/Main.js");

  // const file = File.New("./Server/Compiler/BundleTemplate.js");
  const file = File.New("C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/BeFitBeyondFifty/Client/js/Main.js");
  // const ast = await compiler.Parse(file);
  // console.log(ast);

  const files = {};
  function Traverse(ast)
  {
    traverse(ast, {
      ImportDeclaration(path)
      {
        console.log("Import");
        const url = path.node.source.value;

        let file;
        for (let i = 0; i < paths.length; i++)
        {
          const temp_file = new File(paths[i] + url);
          if (temp_file.IsValid())
          {
            file = temp_file;
            break;
          }
        }

        if (!file)
        {
          console.log("No file for", url);
          return;
        }

        const filepath = file.GetPath();
        if (files[filepath]) return;

        files[filepath] = compiler.ParseSync(file);
        Traverse(files[filepath]);
      },
    });
  }

  // Traverse(ast);

  // traverse(ast, {
  //   Program(path)
  //   {
  //     for (const key in files)
  //     {
  //       if (!files.hasOwnProperty(key)) continue;
  //       const value = files[key];
  //
  //       console.log(value.program.body);
  //
  //       // path.unshiftContainer(
  //       //   "body",
  //       //   // types.program(value.program.body),
  //       //   types.declareModule(
  //       //     types.identifier("_GENERATED_"),
  //       //     value.program.body,
  //       //     "CommonJS",
  //       //   ),
  //       //   // value.program,
  //       //   // types.expressionStatement(
  //       //   //   types.blockStatement(value.program.body),
  //       //   // ),
  //       //   // value.program.body,
  //       // );
  //
  //       // path.unshiftContainer(
  //       //   "body",
  //       //   types.expressionStatement(
  //       //     types.callExpression(
  //       //       types.functionExpression(
  //       //         // null, // id
  //       //         types.identifier("_GENERATED_"),
  //       //         [],
  //       //         types.blockStatement(value.program.body),
  //       //         // types.blockStatement([]),
  //       //         false, // Generator
  //       //         false, // Async
  //       //       ),
  //       //       [],
  //       //     ),
  //       //   ),
  //       // );
  //     }
  //   },
  // });

  // traverse(ast, {
  //   ImportDeclaration(path)
  //   {
  //     console.log("Import");
  //     const url = path.node.source.value;
  //
  //     let file;
  //     for (let i = 0; i < paths.length; i++)
  //     {
  //       const temp_file = new File(paths[i] + url);
  //       if (temp_file.IsValid())
  //       {
  //         file = temp_file;
  //         break;
  //       }
  //     }
  //
  //     if (!file)
  //     {
  //       console.log("No file for", url);
  //       return;
  //     }
  //
  //     const ast = compiler.ParseSync(file);
  //
  //     path.insertBefore(
  //       types.expressionStatement(
  //         types.callExpression(
  //           types.functionExpression(
  //             // null, // id
  //             types.identifier("_GENERATED_"),
  //             [],
  //             types.blockStatement(ast.program.body),
  //             false, // Generator
  //             false, // Async
  //           ),
  //           [],
  //         ),
  //       ),
  //     );
  //   },
  //   exit(path)
  //   {
  //     if (path.node.type === "Program")
  //     {
  //       console.log("~~PROGRAM~~");
  //     }
  //   },
  // });

  const main_code = await file.Read();
  const ast = await babel.parse(main_code, {
    presets: [
      ["@babel/preset-env", {
        // targets: {
        //   "ie": "9",
        // },
        targets: "> 0.25%, not dead",
      }],
    ],
  });

  // console.log(ast.program.body);

  const {code} = await babel.transformFromAstAsync(ast, {
    presets: [
      ["@babel/preset-env", {
        // targets: {
        //   "ie": "9",
        // },
        targets: "> 0.25%, not dead",
      }],
    ],
  });

  console.log(code);

  fs.writeFile("C:/Users/Sean/Documents/Visual Studio 2017/Projects/JavaScript/taggly/Client/js/Bundle.js", code, (error) =>
  {
    if (error) throw error;

    console.log("Done writing");
  });
}

Test();

module.exports.BundleCompiler = BundleCompiler;
module.exports.default = BundleCompiler;

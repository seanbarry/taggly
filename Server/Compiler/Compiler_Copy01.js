"use strict";

const babel = require("@babel/core");
const traverse = require("@babel/traverse").default;
const types = require("@babel/types");
const {parse} = require("@babel/parser");
const generate = require("@babel/generator").default;

const {File, Directory} = require("filesystem");
// const browserify = require("browserify");
const fs = require("fs");
const path = require("path");

class Compiler
{
  constructor()
  {
    this.types = null;
    this.plugins = [];
    this.presets = [];
  }

  AddPlugin(plugin)
  {
    this.plugins.push(babel =>
    {
      this.types = babel.types;
      return {
        visitor: plugin,
      };
    });
  }

  AddPreset(preset, options)
  {
    this.presets.push([preset, options]);
  }

  Call(node)
  {
    // console.log("Calling", node.type);
    const call = this[node.type];

    if (typeof(call) === "function")
    {
      return this[node.type](node, node);
    }
    else
    {
      console.log("Skipping", node.type);
    }
  }

  File({program}, file)
  {
    return this.Call(program);
  }

  Program({body})
  {
    for (let i = 0; i < body.length; i++)
    {
      this.Call(body[i]);
    }
  }

  ImportDeclaration({specifiers, source})
  {
    for (let i = 0; i < specifiers.length; i++)
    {
      this.Call(specifiers[i]);
    }

    return this.Call(source);
  }

  ImportSpecifier({imported, local, ...rest})
  {
    this.Call(imported);
    this.Call(local);
  }

  async Parse(path)
  {
    const file = new File(path);
    const code = await file.Read();

    const ast = await babel.parseAsync(code);
    return ast;
  }

  async Parse(file)
  {
    const code = await file.Read();

    const result = await babel.parse(code, {
      // plugins: [this.GetPlugin()],
      plugins: this.plugins,
      presets: this.presets,
    });

    return result;
  }

  ParseSync(file)
  {
    const code = file.ReadSync();

    const result = babel.parseSync(code, {
      // plugins: [this.GetPlugin()],
      plugins: this.plugins,
      presets: this.presets,
    });

    return result;
  }

  async Compile(file)
  {
    const code = await file.Read();

    const result = await babel.transform(code, {
      // plugins: [this.GetPlugin()],
      plugins: this.plugins,
      presets: this.presets,
    });

    return result.code;
  }

  CompileSync(file)
  {
    // const code = await file.Read();

    const result = babel.transformFileSync(file.GetPath(), {
      // plugins: [this.GetPlugin()],
      plugins: this.plugins,
      presets: this.presets,
    });

    return result.code;
  }

  GetBabelTypes(){ return babel.types; }

  GetPlugin()
  {
    return ({types}) =>
    {
      this.types = types;
      return {
        visitor: this.plugin,
      };
    };
  }
}

module.exports.Compiler = Compiler;
module.exports.default = Compiler;

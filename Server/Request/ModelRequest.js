"use strict";

const {Module} = require("../Module.js");

class ModelRequest extends Module
{
  async Load(app)
  {
    const server = await app.Require("Server");

    // server.Get("/model", this.Get.bind(this));
    server.Post("/model-connect", this.Get.bind(this));
    server.Post("/model", this.Post.bind(this));

    if (app.IsDevelopment())
    {
      server.Put("/model", this.Put.bind(this));
    }

    return super.Load(app)
  }

  // The GET request is used to do the initial connect, and get the sessions
  async Get(app, request, response)
  {
    try
    {
      const session_id = request.GetSessionID();
      if (!session_id) throw new Error(`Invalid session_id`);

      const models = await this.app.Require("Models");
      const {Client, Session} = models.GetModels();
      if (!Client) throw new Error(`Invalid Client Model`);
      if (!Session) throw new Error(`Invalid Session Model`);

      // Find OR create a Client for this session_id
      const client = await new Client()
      .SessionID("==", session_id)
      .$Mode("CRUDL")
      .Unsert();

      // console.log("Got Client", client);

      // Find OR create a Session for this client
      const session = await new Session()
      .SessionID("==", session_id)
      .Type("==", "Client")
      .Model("==", client)
      .Created("?=", new Date())
      .Updated("=", new Date())
      .$Mode("CRUDL")
      .Upsert();

      // console.log("Got Session", session);

      const sessions = (await client.Session().Filter()) || [];

      // Reveal the session_id to the client as an extra hidden pseudo session
      sessions.push(session_id);

      return response.Accept(sessions);
    }
    catch (error)
    {
      return response.Reject(error);
    }
  }

  // POST is used for all general Model use
  async Post(app, request, response)
  {
    try
    {
      const session_id = request.GetSessionID();
      if (!session_id) throw new Error(`Invalid session_id`);

      const body = request.GetBody();

      if (!body._owner_id) throw new Error(`Invalid _owner_id`);
      if (!body._owner_type) throw new Error(`Invalid _owner_type`);
      if (!body._access) throw new Error(`Invalid _access`);
      if (!body._action) throw new Error(`Invalid _action`);

      const models = await app.Require("Models");

      const Session = models.GetModel("Session");
      if (!Session) throw new Error(`Invalid Session Model`);

      const Owner = models.GetModel(body._owner_type);
      if (!Owner) throw new Error(`Invalid Owner Model`);

      const session = await new Owner().ID("==", body._owner_id).ProcessSession(body, session_id, Session);
      if (!session) throw new Error(`Invalid session for ${body._owner_type}.${body._access}().${body._action}()`);

      const model = await session.model.ProcessAccess(body, session);
      const result = await model.ProcessAction(body, session);

      return response.Accept(result);
    }
    catch (error)
    {
      return response.Reject(error.message);
    }
  }

  // PUT is used to update the Model modules from the client during development
  async Put(app, request, response)
  {
    try
    {
      if (!app.IsDevelopment())
      {
        throw new Error(`Cannot use ModelRequest.Put on a live server!`);
      }

      const models = await this.app.Reload("Models");

      const modules = request.GetBody();
      for (let i = 0; i < modules.length; i++)
      {
        await models.UpdateModel(modules[i]);
      }

      await models.Run();
      // console.log("~~Done rebuilding Models~~\n");

      return response.Accept("Modules updated");
    }
    catch (error)
    {
      return response.Reject(error);
    }
  }
}

module.exports.ModelRequest = ModelRequest;
module.exports.default = ModelRequest;

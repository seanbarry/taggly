"use strict";

class Response
{
  constructor(app, response)
  {
    this.app = app;

    // Hide the property so it doesn't flood logging
    Object.defineProperty(this, "response", {
      enumerable: false,
      writable: true,
      value: response,
    });

    this.sent = false;
  }

  Code (code ){ this.code  = code ; return this; }
  Type (type ){ this.type  = type ; return this; }
  Value(value){ this.value = value; return this; }

  // For ease of setting the type
  JSON    (){ return this.Type("json"    ); }
  HTML    (){ return this.Type("render"  ); }
  Text    (){ return this.Type("send"    ); }
  Blob    (){ return this.Type("download"); }
  File    (){ return this.Type("sendFile"); }
  Redirect(){ return this.Type("redirect"); }

  // For conveniently setting common responses
  Accept(value = null){ return this.Code(200).JSON().Value({ value }).Send(); }

  Reject(error)
  {
    console.error("Rejecting", error);
    return this.Code(200).JSON().Value({ error }).Send();
  }

  // Reject     (value){ return this.Code(200).JSON().Value({ reject: value }); }
  // Error      (value){ return this.Code(200).JSON().Value({ error: value }); }
  // ClientError(client_error){ return this.Code(200).JSON().Value({ client_error }); }
  // ServerError(server_error){ return this.Code(200).JSON().Value({ server_error }); }
  // InternalError(internal_error){ return this.Code(200).JSON().Value({ internal_error }); }

  // // These all return 200, because browsers have built in handling for codes I want to avoid
  // ClientError  (message, name){ return this.Code(200).JSON().Value({ error: "ClientError", message, name, }); }
  // ServerError  (message, name){ return this.Code(200).JSON().Value({ error: "ServerError", message, name, }); }
  // InternalError(message, name){ return this.Code(200).JSON().Value({ error: "InternalError", message, name, }); }

  // Error(error)
  // {
  //   if (error instanceof ClientError) this.ClientError(error.message, error.name);
  //   else if (error instanceof ServerError) this.ServerError(error.message, error.name);
  //   else this.InternalError("Internal Error", "InternalError");
  // }

  HasCode(){ return this.code !== undefined; }
  HasType(){ return this.type !== undefined; }
  HasValue(){ return this.value !== undefined; }

  GetCode(){ return this.code || Response.default_code; }
  GetType(){ return this.type || Response.default_type; }
  GetValue(){ return this.value || Response.default_value; }

  SetStatus(code){ this.response.status(code); return this; }
  SetHeader(name, value){ this.response.setHeader(name, value); return this; }
  WriteHead(code, headers){ console.log("~~WRITING HEAD~~", code, headers); this.response.writeHead(code, headers); return this; }
  Write(data){ this.response.write(data); return this; }

  End(value, type){ this.response.end(value, type); return this; }

  Send()
  {
    if (this.sent === true)
    {
      throw new Error(`Response already sent`);
    }

    this.sent = true;

    const code = this.GetCode();
    const type = this.GetType();
    const value = this.GetValue();

    this.response.status(code)[type](value);
    return this;
  }

  IsSent(){ return this.sent === true; }

  GetApp(){ return this.app; }
  GetResponse(){ return this.response; }

  SetCookie({
    name,
    value,
    domain,
    encode,
    expires,
    httpOnly = true,
    maxAge,
    path,
    secure,
    signed,
    sameSite,
  })
  {
    this.response.cookie(name, value, {
      domain,
      encode,
      expires,
      httpOnly,
      maxAge,
      path,
      secure,
      signed,
      sameSite,
    });

    return value;
  }
}

Response.default_code = 200;
Response.default_type = "json";
Response.default_value = {};

module.exports.Response = Response;
module.exports.default = Response;

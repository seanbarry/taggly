"use strict";

const uid = require("uid-safe");

class Request
{
  constructor(app, request, response)
  {
    this.app = app;
    this.response = response;
    this.url = request.url.replace(/^\/|\/$/g, "");

    // Hide the request property so it doesn't flood logging
    Object.defineProperty(this, "request", {
      enumerable: false,
      writable: true,
      value: request,
    });
  }

  Respond()
  {
    return this.Process()
    .then(result =>
    {
      // console.log("Responding with", this.response.value);
      this.response.Send();
    })
    .catch(error =>
    {
      this.app.HandleRequestError(error);

      this.response.Error(error);
      this.response.Send();
    });
  }

  GetApp(){ return this.app; }
  GetRequest(){ return this.request; }
  GetResponse(){ return this.response; }
  GetURL(){ return this.url; }
  GetBaseURL(){ return this.request.url; }
  GetBody(){ return this.request.body; }
  GetCookies(){ return this.request.cookies; }
  GetSignedCookies(){ return this.request.signedCookies; }
  GetCookie(name){ return this.request.cookies[name]; }
  GetSignedCookie(name){ return this.request.signedCookies[name]; }
  GetParameters(){ return this.request.params; }
  GetParameter(parameter){ return this.request.params[parameter]; }
  GetHeader(header){ return this.request.headers[header]; }
  GetHost(){ return this.GetHeader("host"); }
  GetFullURL(){ return this.request.protocol + "://" + this.request.get("host") + this.request.originalUrl; }

  GetUUID(){ return uid.sync(24); }

  GetBodySessionID()
  {
    const body = this.GetBody();
    if (body && body._special_session_id)
    {
      // console.log("Using body session id!");
      return body._special_session_id;
    }
  }

  GetSessionID(name = "session_id")
  {
    return this.response.SetCookie({
      name,
      value: this.GetBodySessionID() || this.GetSignedCookie(name) || this.GetUUID(), // Either refresh it or generate a new one
      signed: true,
      secure: true,
      maxAge: this.app.SessionMaxAge(),
      httpOnly: true,
      sameSite: "lax",
    });
  }
}

module.exports.Request = Request;
module.exports.default = Request;

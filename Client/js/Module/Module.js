export class Module
{
  static GetModules(){ return this.modules; }

  static Register(ctor)
  {
    const mod = new Module(ctor);
    this.modules.push(mod);

    return mod;
  }

  constructor(ctor)
  {
    this.name = ctor.name;
    this.data = ctor.toString();
    this.minify = true;
  }
}

Module.modules = [];

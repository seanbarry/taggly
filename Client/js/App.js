import {History} from "/js/App/History.js";
import {DynamicImporter} from "/js/App/DynamicImporter.js";
import {TimeManager} from "/js/App/TimeManager.js";
import {Observer} from "/js/App/Observer.js";
import {BrowserDetection} from "/js/App/BrowserDetection.js";
import {SessionCache} from "/js/App/SessionCache.js";
import {Tag} from "/js/Tag.js";
import {Model} from "/js/Model.js";
import {Url} from "/js/Utility/Url.js";

import * as Tags from "/js/Tag/Tags.js";

for (const key in Tags)
{
  Tags[key].Register();
}

export class App extends Tag
{
  InitializeGlobal()
  {
    // Set the global access for the app
    window.app = this;
  }

  InitializeImporter()
  {
    this.importer = new DynamicImporter(this);
  }

  InitializeBrowserDetection()
  {
    this.browser_detection = new BrowserDetection(this);
  }

  InitializeHistory()
  {
    this.history = new History(this);
  }

  InitializeObserver()
  {
    this.observer = new Observer();
  }

  InitializeTimeManager()
  {
    this.time_manager = new TimeManager(this);
  }

  InitializeClient()
  {
    const Client = Model.GetModel("Client");
    if (!Client) throw new Error(`A Client Model must be defined and registered`);

    this.client = window.app.Request({
      path: "/model",
      method: "GET",
    }).then(data => new Client(data.value));
  }

  InitializeTitle()
  {
    // this.SetTitle("Loading");
  }

  async Initialize()
  {
    await this.InitializeGlobal();
    await this.InitializeImporter();
    await this.InitializeBrowserDetection();
    await this.InitializeHistory();
    await this.InitializeObserver();
    // await this.InitializeTimeManager();
    await this.InitializeTitle();
    // await this.InitializeClient();
    // await this.InitializeSessions();

    return this;
  }

  constructor()
  {
    super("div", "app").AddClass(this.GetKebabName());
    // this.rendered = true; // Block the normal rendering until after initialization
  }

  Import(path){ return this.importer.Import(path); }
  Require(path, name){ return this.importer.Require(path, name); }

  Request({
    method = "POST",
    mode = "cors",
    path,
    body,
  })
  {
    if (body && typeof(body) !== "string")
    {
      body = JSON.stringify(body);
    }

    return fetch(path, {
      method,
      mode,
      cache: "no-cache",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json",
      },
      redirect: "follow",
      body,
    })
    .then(response => response.json());
    // .then(json => this.Process(json));
  }

  Parse(url_string)
  {
    if (!url_string) throw new Error(`Cannot find page for an invalid url`);

    const root = this.GetPathRoot();
    const url = new Url(url_string);

    return root.Test(url);
  }

  Go(url = "/", replace = false)
  {
    if (typeof(url) !== "string") throw new Error(`Invalid url string for app.Go`);

    console.log("Navigating from", this.GetUrlBase(), "to", url);

    const event = new Event("appgo");
    event.from = this.GetUrlBase();
    event.to = url;
    event.replace = replace;
    this.Fire(event);

    if (replace === true) this.history.Replace(url);
    else this.history.Push(url); // Update the url (which also pushes the history)

    return this.Render();
  }

  ReplaceURL(url)
  {
    this.history.Replace(url);
    return this;
  }

  SetParameter(name, value)
  {
    const url = new URL(window.location.href);
    url.searchParams.set(name, value);
    this.history.Replace(url);

    return this;
  }

  AppendParameter(name, value)
  {
    const url = new URL(window.location.href);
    url.searchParams.append(name, value);
    this.history.Replace(url);

    return this;
  }

  DeleteParameter(name)
  {
    const url = new URL(window.location.href);
    url.searchParams.delete(name);
    this.history.Replace(url);

    return this;
  }

  Redirect(url)
  {
    console.log("Redirecting from", this.GetUrlBase(), "to", url);

    const event = new Event("appredirect");
    event.from = this.GetUrlBase();
    event.to = url;
    this.Fire(event);

    this.history.Redirect(url);
  }

  ExternalRedirect(url)
  {
    window.location = url;
    // return this;
  }

  OnRenderError(error)
  {
    const event = new Event("tagerror");
    event.error = error;
    this.Fire(event);
  }

  async Render(page, ...tags)
  {
    try
    {
      const event = new Event("apprender");

      if (!page)
      {
        const root = this.GetPathRoot();
        this.url = new Url(window.location.pathname);
        this.page_constructor = await root.Test(this.url);
        if (!this.page_constructor) throw new Error(`No page found for "${window.location.pathname}"`);

        const path = this.url.GetLastTag();
        const meta = path.QueryAll("meta");
        for (let i = 0; i < meta.length; i++)
        {
          const tag = meta[i];

          if ((tag.GetAttribute("property") === "og:title") && tag.HasAttribute("content"))
          {
            event.page_title = tag.GetAttribute("content");
            this.SetTitle(tag.GetAttribute("content"));
            break;
          }
        }

        page = new this.page_constructor();
        this.page = page;
      }

      event.page = page;
      event.page_url = this.GetUrlValue();
      this.Fire(event);

      this.Clear();
      const result = await super.Render(page, tags);

      const hash = window.location.hash;
      if (hash)
      {
        const elem = document.getElementById(hash.substring(1));
        if (elem)
        {
          elem.scrollIntoView();
        }
      }

      // return result;
    }
    catch (error)
    {
      this.OnRenderError(error);
    }
  }

  HRef(url){ window.location.href = url; return this; }

  Back   (){ this.history.Back   (); }
  Forward(){ this.history.Forward(); }
  Reload (){ window.location.reload(); }

  GUID(base = "guid")
  {
    if (this._guid_index === undefined)
    {
      this._guid_index = 0;
    }

    return `${base}-${this._guid_index++}`;
  }

  GUID(base = "guid")
  {
    if (this._guid_index === undefined)
    {
      this._guid_index = 0;
    }

    return `${base}-${this._guid_index++}`;
  }

  SetTitle(title, app_title = this.GetName())
  {
    this.current_title = title;
    document.title = `${title} - ${app_title}`;
  }

  GetClient()
  {
    const Client = Model.GetModel("Client");
    if (!Client) throw new Error(`A Client Model must be defined and registered`);

    return this.GetSessionCache().then(cache => cache.Get(Client));
  }

  GetStorage()
  {
    if (this._storage) return this._storage;

    try
    {
      localStorage.setItem("test", "test");
      localStorage.removeItem("test");
      this._storage = localStorage;
    }
    catch (error)
    {
      try
      {
        sessionStorage.setItem("test", "test");
        sessionStorage.removeItem("test");
        this._storage = sessionStorage;
      }
      catch (error)
      {
        console.log("Neither localStorage nor sessionStorage is available.");
      }
    }

    return this._storage;
  }

  GetSessionCache()
  {
    if (!this.session_cache)
    {
      const storage = this.GetStorage();

      const Session = Model.GetModel("Session");
      if (!Session) throw new Error(`A Session Model must be defined and registered`);

      if (!this._session_id && storage)
      {
        this._session_id = storage.getItem("_session_id");
      }

      this.session_cache = this.Request({
        path: "/model-connect",
        method: "POST",
        body: {
          _special_session_id: this._session_id,
        },
      })
      .then(data =>
      {
        if (data && data.value instanceof Array)
        {
          this._session_id = data.value.pop();
          
          if (storage)
          {
            storage.setItem("_session_id", this._session_id);
          }
        }

        return new SessionCache(this, Session.Load(data.value));
      });
    }

    return this.session_cache;
  }

  GetSessionID(){ return this._session_id; }
  GetDebug(){ return false; }
  GetPage(){ return this.page; }
  GetTitle(){ return this.current_title || document.title; }
  GetHostName(){ return window.document.location.hostname; }
  GetLanguage(){ return window.navigator.language; }
  GetVendor(){ return window.navigator.vendor; }
  GetName(){ return this.GetClassName(); }
  GetClassName(){ return this.constructor.name; }
  GetKebabName(){ return this.GetClassName().replace(/([a-z0-9])([A-Z])/g, "$1-$2").toLowerCase(); }
  GetSessionExpirationOffset(){ return 100; } // Add this when checking if a session is expired
  GetPathRoot(){ return Tag.Query("path.root"); }

  GetUrl(){ return this.url; }
  GetUrlTag(index){ return this.url.GetTag(index); }
  GetUrlValue(){ return this.url.GetUrl(); }
  GetUrlBase(){ return this.url.GetBaseUrl(); }
  GetUrlEncoded(){ return this.url.GetEncodedUrl(); }
  GetUrlParameter(name){ return this.url.GetParameter(name); }
  GetUrlSearch(name){ return this.url.GetSearch(name); }

  GetOS()
  {
    const agent = navigator.userAgent;

    if      (agent.indexOf("Win"     ) != -1) return "Windows";
    else if (agent.indexOf("Mac"     ) != -1) return "Macintosh";
    else if (agent.indexOf("Linux"   ) != -1) return "Linux";
    else if (agent.indexOf("Android" ) != -1) return "Android";
    else if (agent.indexOf("like Mac") != -1) return "iOS";
    else return "Unknown";
  }

  GetBrowser()
  {
    return this.browser_detection.GetBrowser();
  }

  GetRAM(){ return window.navigator.deviceMemory; }
  GetCPU(){ return window.navigator.hardwareConcurrency; }

  GetSearchParameters()
  {
    const variables = window.location.search.substring(1).split("&");
    const map = {};

    if (variables.length === 1 && variables[0] === "") return map;

    for (let i = 0; i < variables.length; i++)
    {
      const [key, value] = variables[i].split("=");
      map[key] = decodeURIComponent(value);
    }

    return map;
  }

  IsDebug(){ return this.GetDebug() === true; }
  IsLocalHost(){ return this.GetHostName() === "localhost"; }
}

/*
  This file contains extra HTML tags which are standard,
  but are niche or depreciated or deemed unimportant by me.
  This allows them to be loaded if required,
  but otherwise, let's not waste time with them.
*/

import {Tag} from "/js/Tag.js";

export class Applet                  extends Tag { constructor(c){ super("applet", c); } }
export class BidirectionalIsolate    extends Tag { constructor(c){ super("bdi", c); } }
export class BidirectionalOverride   extends Tag { constructor(c){ super("bdo", c); } }
export class Content                 extends Tag { constructor(c){ super("content", c); } }
export class Dir                     extends Tag { constructor(c){ super("dir", c); } }
export class Element                 extends Tag { constructor(c){ super("element", c); } }
export class NoEmbed                 extends Tag { constructor(c){ super("noembed", c); } }
export class Ruby                    extends Tag { constructor(c){ super("ruby", c); } }
export class RubyBase                extends Tag { constructor(c){ super("rb", c); } }
export class RubyParenthesis         extends Tag { constructor(c){ super("rp", c); } }
export class RubyText                extends Tag { constructor(c){ super("rt", c); } }
export class RubyTextContainer       extends Tag { constructor(c){ super("rtc", c); } }
export class Shadow                  extends Tag { constructor(c){ super("shadow", c); } }
export class Slot                    extends Tag { constructor(c){ super("slot", c); } }
export class Strike                  extends Tag { constructor(c){ super("strike", c); } }
export class TeletypeText            extends Tag { constructor(c){ super("tt", c); } }
export class UnarticulatedAnnotation extends Tag { constructor(c){ super("u", c); } }

Applet.Register();
BidirectionalIsolate.Register();
BidirectionalOverride.Register();
Content.Register();
Dir.Register();
Element.Register();
NoEmbed.Register();
Ruby.Register();
RubyBase.Register();
RubyParenthesis.Register();
RubyText.Register();
RubyTextContainer.Register();
Shadow.Register();
Slot.Register();
Strike.Register();
TeletypeText.Register();
UnarticulatedAnnotation.Register();

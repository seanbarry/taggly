export class TagData
{
  Save(key, value)
  {
    if (value !== undefined && value !== null)
    {
      this[key] = value;
    }

    return this;
  }

  // toJSON()
  // {
  //   return {
  //     type: this.type,
  //     data: this.data,
  //     attributes: this.attributes,
  //   };
  // }
}

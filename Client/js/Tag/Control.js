import {Animation} from "/js/Tag/Animation.js";

export const EVENTS = {
  // Resource events
  OnError: "error",
  OnAbort: "abort",
  OnLoad: "load",
  OnBeforeUnload: "beforeunload",
  OnUnload: "unload",

  // Network events
  OnOnline: "online",
  OnOffline: "offline",

  // Focus events
  OnInput: "input",
  OnFocus: "focus",
  OnBlur: "blur",
  OnChange: "change",

  // WebSocket events
  OnSocketOpen: "open",
  OnSocketMessage: "message",
  OnSocketError: "error",
  OnSocketClose: "close",

  // Session History events
  OnPageHide: "pagehide",
  OnPageShow: "pageshow",
  OnPopState: "popstate",

  // CSS Animation events
  OnAnimationStart: "animationstart",
  OnAnimationCancel: "animationcancel",
  OnAnimationEnd: "animationend",
  OnAnimationIteration: "animationiteration",

  // CSS Transition events
  OnTransitionStart: "transitionstart",
  OnTransitionCancel: "transitioncancel",
  OnTransitionEnd: "transitionend",
  OnTransitionRun: "transitionrun",

  // Form events
  OnReset: "reset",
  OnSubmit: "submit",

  // Printing events
  OnBeforePrint: "beforeprint",
  OnAfterPrint: "afterprint",

  // Text Composition events
  OnCompositionStart: "compositionstart",
  OnCompositionUpdate: "compositionupdate",
  OnCompositionEnd: "compositionend",

  // View events
  OnFullscreenChange: "fullscreenchange",
  OnFullscreenError: "fullscreenerror",
  OnResize: "resize",
  OnScroll: "scroll",

  // Clipboard events
  OnCut: "cut",
  OnCopy: "copy",
  OnPaste: "paste",

  // Keyboard events
  OnKeyDown: "keydown",
  OnKeyPress: "keypress",
  OnKeyUp: "keyup",

  // Mouse events
  OnAuxClick: "auxclick",
  OnClick: "click",
  OnContextMenu: "contextmenu",
  OnDblClick: "dblclick",
  OnMouseDown: "mousedown",
  OnMouseEnter: "mouseenter",
  OnMouseLeave: "mouseleave",
  OnMouseMove: "mousemove",
  OnMouseOver: "mouseover",
  OnMouseOut: "mouseout",
  OnMouseUp: "mouseup",
  OnPointerLockChange: "pointerlockchange",
  OnPointerLockError: "pointerlockerror",
  OnSelect: "select",
  OnWheel: "wheel",

  // Drag & Drop events
  OnDrag: "drag",
  OnDragEnd: "dragend",
  OnDragEnter: "dragenter",
  OnDragStart: "dragstart",
  OnDragLeave: "dragleave",
  OnDragOver: "dragover",
  OnDrop: "drop",

  // Media events
  OnAudioProcess: "audioprocess",
  OnCanPlay: "canplay",
  OnCanPlayThrough: "canplaythrough",
  Complete: "complete",
  OnDurationChange: "durationchange",
  OnEmptied: "emptied",
  OnEnded: "ended",
  OnLoadedData: "loadeddata",
  OnLoadedMetaData: "loadedmetadata",
  OnPause: "pause",
  OnPlay: "play",
  OnPlaying: "playing",
  OnRateChange: "ratechange",
  OnSeeked: "seeked",
  OnSeeking: "seeking",
  OnStalled: "stalled",
  OnSuspend: "suspend",
  OnTimeUpdate: "timeupdate",
  OnVolumeChange: "volumechange",
  OnWaiting: "waiting",

  // Progress events
  OnProgressAbort: "abort",
  OnProgressError: "error",
  OnProgressLoad: "load",
  OnProgressLoadEnd: "loadend",
  OnProgressLoadStart: "loadstart",
  OnProgressProgress: "progress",
  OnProgressTimeout: "timeout",

  // Custom observer events
  OnAdd: "add",
  OnRemove: "remove",
  OnAddChild: "addchild",
  OnRemoveChild: "removechild",
  OnRemoving: "removing",
  OnAttribute: "attribute",
  OnFullViewEnter: "fullviewenter",
  OnFullViewLeave: "fullviewleave",
  OnViewEnter: "viewenter",
  OnViewLeave: "viewleave",

  // Custom Taggly events
  OnAppRender: "apprender",
  OnAppRedirect: "appredirect",
  OnAppGo: "appgo",
  OnAppError: "apperror",
  OnTagError: "tagerror",
};

export const KEYBOARD_EVENTS = {
  OnKeyBackspace: 8,
  OnKeyTab: 9,
  OnKeyEnter: 13,
  OnKeyShift: 16,
  OnKeyCtrl: 17,
  OnKeyAlt: 18,
  OnKeyPause: 19,
  OnKeyCapsLock: 20,
  OnKeyEscape: 27,
  OnKeyPageUp: 33,
  OnKeyPageDown: 34,
  OnKeyEnd: 35,
  OnKeyHome: 36,
  OnKeyLeftArrow: 37,
  OnKeyUpArrow: 38,
  OnKeyRightArrow: 39,
  OnKeyDownArrow: 40,
  OnKeyInsert: 45,
  OnKeyDelete: 46,
  OnKey0: 48,
  OnKey1: 49,
  OnKey2: 50,
  OnKey3: 51,
  OnKey4: 52,
  OnKey5: 53,
  OnKey6: 54,
  OnKey7: 55,
  OnKey8: 56,
  OnKey9: 57,
  OnKeyA: 65,
  OnKeyB: 66,
  OnKeyC: 67,
  OnKeyD: 68,
  OnKeyE: 69,
  OnKeyF: 70,
  OnKeyG: 71,
  OnKeyH: 72,
  OnKeyI: 73,
  OnKeyJ: 74,
  OnKeyK: 75,
  OnKeyL: 76,
  OnKeyM: 77,
  OnKeyN: 78,
  OnKeyO: 79,
  OnKeyP: 80,
  OnKeyQ: 81,
  OnKeyR: 82,
  OnKeyS: 83,
  OnKeyT: 84,
  OnKeyU: 85,
  OnKeyV: 86,
  OnKeyW: 87,
  OnKeyX: 88,
  OnKeyY: 89,
  OnKeyZ: 90,
  OnKeyLeftWindowKey: 91,
  OnKeyRightWindowKey: 92,
  OnKeySelectKey: 93,
  OnKeyNumpad0: 96,
  OnKeyNumpad1: 97,
  OnKeyNumpad2: 98,
  OnKeyNumpad3: 99,
  OnKeyNumpad4: 100,
  OnKeyNumpad5: 101,
  OnKeyNumpad6: 102,
  OnKeyNumpad7: 103,
  OnKeyNumpad8: 104,
  OnKeyNumpad9: 105,
  OnKeyMultiply: 106,
  OnKeyAdd: 107,
  OnKeySubtract: 109,
  OnKeyDecimalPoint: 110,
  OnKeyDivide: 111,
  OnKeyF1: 112,
  OnKeyF2: 113,
  OnKeyF3: 114,
  OnKeyF4: 115,
  OnKeyF5: 116,
  OnKeyF6: 117,
  OnKeyF7: 118,
  OnKeyF8: 119,
  OnKeyF9: 120,
  OnKeyF10: 121,
  OnKeyF11: 122,
  OnKeyF12: 123,
  OnKeyNumLock: 144,
  OnKeyScrollLock: 145,
  OnKeySemiColon: 186,
  OnKeyEqualSign: 187,
  OnKeyComma: 188,
  OnKeyDash: 189,
  OnKeyPeriod: 190,
  OnKeyForwardSlash: 191,
  OnKeyGraveAccent: 192,
  OnKeyOpenBracket: 219,
  OnKeyBackSlash: 220,
  OnKeyCloseBraket: 221,
  OnKeySingleQuote: 222,
}

export class Control
{
  IsEventHandler(fn)
  {
    if (typeof(fn) !== "function") return false;
    else return EVENTS.hasOwnProperty(fn.name);
  }

  IsKeyboardEventHandler(fn)
  {
    if (typeof(fn) !== "function") return false;
    else return KEYBOARD_EVENTS.hasOwnProperty(fn.name);
  }

  Initialize()
  {
    const proto = this.constructor.prototype;
    const props = Object.getOwnPropertyNames(proto);

    for (let i = 0; i < props.length; i++)
    {
      const name = props[i];
      const fn = proto[name];

      if (name === "constructor") continue;
      if (typeof(fn) !== "function") continue;

      if (EVENTS[name]) this.Subscribe(fn);
    }
  }

  Subscribe(fn)
  {
    console.log("Subscribing", fn.name);
    if (typeof(fn) !== "function") throw new Error(`Control.Subscribe must be given a function`);

    const name = fn.name;
    const node = this.GetTag().GetNode();

    if (!EVENTS[name]) throw new Error(`Name ${name} is not a valid event`);

    if (this.handlers[name]) throw new Error(`Control already has a handler for event ${name}`);

    // Create the handler function and store it so it can be unsubscribed later
    this.handlers[name] = (event) =>
    {
      const args = this.GetArgs();
      args[args.length - 1] = event;

      fn.apply(this, args);
    };

    // Start listening to the event
    node.addEventListener(EVENTS[name], this.handlers[name]);
  }

  Subscribe(fn)
  {
    if (typeof(fn) !== "function") throw new Error(`Control.Subscribe must be given a function`);

    const name = fn.name;
    if (!name) throw new Error(`Control cannot subscribe an anonymous function`);

    const node = this.GetTag().GetNode();
    if (!node) throw new Error(`Control cannot subscribe without a valid tag node`);

    if (this.handlers[name]) throw new Error(`Control already has a handler for event ${name}`);

    if (this.IsEventHandler(fn))
    {
      // Create the handler function and store it so it can be unsubscribed later
      this.handlers[name] = (event) =>
      {
        const args = this.GetArgs();
        args[args.length - 1] = event;

        fn.apply(this, args);
      };

      // Start listening to the event
      node.addEventListener(EVENTS[name], this.handlers[name]);
    }
    else if (this.IsKeyboardEventHandler(fn))
    {
      const key = KEYBOARD_EVENTS[name];

      let type;
      if      (typeof(key) === "number") type = "keyCode";
      else if (typeof(key) === "string") type = "key";
      else throw new TypeError("Unknown key type");

      // Create the handler function and store it so it can be unsubscribed later
      this.handlers[name] = (event) =>
      {
        const args = this.GetArgs();
        args[args.length - 1] = event;

        // If the event is for the requested key
        if (event[type] === key)
        {
          fn.apply(this, args);
        }
      };

      // Start listening to the event
      node.addEventListener("keydown", this.handlers[name]);
    }
  }

  Unsubscribe(fn)
  {
    if (typeof(fn) !== "function") throw new Error(`Control.Unsubscribe must be given a function`);

    const name = fn.name;
    if (!name) throw new Error(`Control cannot unsubscribe an anonymous function`);

    const node = this.GetTag().GetNode();
    if (!node) throw new Error(`Control cannot unsubscribe without a valid tag node`);

    if (!this.handlers[name]) throw new Error(`Control does not have a handler for event ${name} to Unsubscribe`);

    if (this.IsEventHandler(fn))
    {
      node.removeEventListener(EVENTS[name], this.handlers[name]);
    }
    else if (this.IsKeyboardEventHandler(fn))
    {
      node.removeEventListener("keydown", this.handlers[name]);
    }

    this.handlers[name] = undefined;
  }

  constructor(tag, args = [])
  {
    if (!tag) throw new Error(`Control.constructor expected a tag`);
    if (!tag.node) throw new Error(`Control.constructor expected tag to have a node`);

    // Add a slot for the event in the args
    args.push(undefined);

    this.tag = tag;
    this.args = args;
    this.handlers = {};

    this.Initialize.apply(this, args);
  }

  Animation(tag = this.tag)
  {
    const animation = new Animation(tag);
    return animation;
  }

  GetTag(){ return this.tag; }
  GetArgs(){ return this.args; }
  GetHandlers(){ return this.handlers; }
}

import {Tag} from "/js/Tag.js";
import {Path} from "/js/Tag/Path.js";

// export * from "/js/Tag/Path.js";

export class System extends Tag
{
  constructor()
  {
    // super("body", );
    super("body", document.body).AddClass("system", "is-relative");
    // document.body = this.GetNode();
  }

  // Render(...values)
  // {
  //   console.log("~~~Rendering Tag.System~~~");
  //   // document.body = this.GetNode();
  //   return super.Render(...values);
  // }
}

export class Anchor extends Tag
{
  constructor(c){ super("a", c); }

  SaveData(d){ return super.SaveData(d).Save("href", this.GetAttribute("href")); }
  // LoadData(d){ return super.LoadData(d).HRef(`/redirect/${encodeURIComponent(d.href)}/${window.app.GetEncodedUrl()}`).Style("color: #82a5dc;"); }
  // LoadData(d){ return super.LoadData(d).HRef(encodeURIComponent(d.href)).Style("color: #82a5dc;"); }

  LoadData(d)
  {
    // .Style("color: #82a5dc;")
    const url = `/redirect/${encodeURIComponent(d.href)}/${encodeURIComponent(window.app.GetUrl())}`;
    return super.LoadData(d).HRef(url).OnClick(e =>
    {
      // console.log("Click untrusted link!");
      window.app.Go(url);
    });
  }
}

export class Image extends Tag
{
  constructor(c){ super("img", c); }

  SaveData(d)
  {
    return super
    .SaveData(d)
    .Save("src", this.GetAttribute("src"))
    .Save("alt", this.GetAttribute("alt"));
  }

  LoadData(d){ return super.LoadData(d).Src(d.src).Alt(d.alt); }
}

export class Span extends Tag
{
  constructor(c){ super("span", c); }

  SaveData(d){ return super.SaveData(d).Save("text", this.GetText()); }
  LoadData(d){ return super.LoadData(d).Text(d.text); }
}

export class Abbreviation    extends Tag { constructor(c){ super("abbr",       c); } }
export class Address         extends Tag { constructor(c){ super("address",    c); } }
export class Article         extends Tag { constructor(c){ super("article",    c); } }
export class Audio           extends Tag { constructor(c){ super("audio",      c); } }
export class Area            extends Tag { constructor(c){ super("area",       c); } }
export class AreaMap         extends Tag { constructor(c){ super("map",        c); } }
export class Aside           extends Tag { constructor(c){ super("aside",      c); } }
export class Base            extends Tag { constructor(c){ super("base",       c); } }
export class Bold            extends Tag { constructor(c){ super("b",          c); } }
export class Body            extends Tag { constructor(c){ super("body",       c); } }
export class BreakLine       extends Tag { constructor(c){ super("br",         c); } }
export class Button          extends Tag { constructor(c){ super("button",     c); } }
export class BlockQuote      extends Tag { constructor(c){ super("blockquote", c); } }
export class Cite            extends Tag { constructor(c){ super("cite",       c); } }
export class Code            extends Tag { constructor(c){ super("code",       c); } }
export class Canvas          extends Tag { constructor(c){ super("canvas",     c); } }
export class Column          extends Tag { constructor(c){ super("col",        c); } }
export class ColumnGroup     extends Tag { constructor(c){ super("colgroup",   c); } }
export class Data            extends Tag { constructor(c){ super("data",       c); } }
export class DataList        extends Tag { constructor(c){ super("datalist",   c); } }
export class Deleted         extends Tag { constructor(c){ super("del",        c); } }
export class Details         extends Tag { constructor(c){ super("details",    c); } }
export class Dialog          extends Tag { constructor(c){ super("dialog",     c); } }
export class Div             extends Tag { constructor(c){ super("div",        c); } }
export class Definition      extends Tag { constructor(c){ super("dfn",        c); } }
export class DescriptionData extends Tag { constructor(c){ super("dd",         c); } }
export class DescriptionList extends Tag { constructor(c){ super("dl",         c); } }
export class DescriptionTerm extends Tag { constructor(c){ super("dt",         c); } }
export class Emphasis        extends Tag { constructor(c){ super("em",         c); } }
export class Embed           extends Tag { constructor(c){ super("embed",      c); } }
export class ExternalObject  extends Tag { constructor(c){ super("object",     c); } }
export class ExternalParam   extends Tag { constructor(c){ super("param",      c); } }
export class Figure          extends Tag { constructor(c){ super("figure",     c); } }
export class FigureCaption   extends Tag { constructor(c){ super("figcaption", c); } }
export class Footer          extends Tag { constructor(c){ super("footer",     c); } }
export class Form            extends Tag { constructor(c){ super("form",       c); } }
export class Head            extends Tag { constructor(c){ super("head",       c); } }
export class Header          extends Tag { constructor(c){ super("header",     c); } }
export class Header1         extends Tag { constructor(c){ super("h1",         c); } }
export class Header2         extends Tag { constructor(c){ super("h2",         c); } }
export class Header3         extends Tag { constructor(c){ super("h3",         c); } }
export class Header4         extends Tag { constructor(c){ super("h4",         c); } }
export class Header5         extends Tag { constructor(c){ super("h5",         c); } }
export class Header6         extends Tag { constructor(c){ super("h6",         c); } }
export class HeaderGroup     extends Tag { constructor(c){ super("hgroup",     c); } }
export class HorizontalRule  extends Tag { constructor(c){ super("hr",         c); } }
// export class HTML            extends Tag { constructor(c){ super("html",       c); } }
export class Italic          extends Tag { constructor(c){ super("i",          c); } }
export class InlineFrame     extends Tag { constructor(c){ super("iframe",     c); } }
// export class Image           extends Tag { constructor(c){ super("img",        c); } }
export class Input           extends Tag { constructor(c){ super("input",      c); } }
export class Inserted        extends Tag { constructor(c){ super("ins",        c); } }
export class KeyboardInput   extends Tag { constructor(c){ super("kbd",        c); } }
export class Label           extends Tag { constructor(c){ super("label",      c); } }
export class Legend          extends Tag { constructor(c){ super("legend",     c); } }
export class ListItem        extends Tag { constructor(c){ super("li",         c); } }
export class Link            extends Tag { constructor(c){ super("link",       c); } }
export class Main            extends Tag { constructor(c){ super("main",       c); } }
export class Mark            extends Tag { constructor(c){ super("mark",       c); } }
export class Menu            extends Tag { constructor(c){ super("menu",       c); } }
export class MenuItem        extends Tag { constructor(c){ super("menuitem",   c); } }
export class Meter           extends Tag { constructor(c){ super("meter",      c); } }
export class Nav             extends Tag { constructor(c){ super("nav",        c); } }
export class NoScript        extends Tag { constructor(c){ super("noscript",   c); } }
export class OrderedList     extends Tag { constructor(c){ super("ol",         c); } }
export class Option          extends Tag { constructor(c){ super("option",     c); } }
export class OptionGroup     extends Tag { constructor(c){ super("optgroup",   c); } }
export class Output          extends Tag { constructor(c){ super("output",     c); } }
export class Paragraph       extends Tag { constructor(c){ super("p",          c); } }
export class Picture         extends Tag { constructor(c){ super("picture",    c); } }
export class Pre             extends Tag { constructor(c){ super("pre",        c); } }
export class Progress        extends Tag { constructor(c){ super("progress",   c); } }
export class Quote           extends Tag { constructor(c){ super("q",          c); } }
export class Strike          extends Tag { constructor(c){ super("s",          c); } }
export class Sample          extends Tag { constructor(c){ super("samp",       c); } }
export class SubScript       extends Tag { constructor(c){ super("sub",        c); } }
export class SuperScript     extends Tag { constructor(c){ super("sup",        c); } }
export class Section         extends Tag { constructor(c){ super("section",    c); } }
export class Select          extends Tag { constructor(c){ super("select",     c); } }
export class Small           extends Tag { constructor(c){ super("small",      c); } }
// export class Span            extends Tag { constructor(c){ super("span",       c); } }
export class Strong          extends Tag { constructor(c){ super("strong",     c); } }
export class Style           extends Tag { constructor(c){ super("style",      c); } }
export class Source          extends Tag { constructor(c){ super("source",     c); } }
export class Summary         extends Tag { constructor(c){ super("summary",    c); } }
export class Table           extends Tag { constructor(c){ super("table",      c); } }
export class TextArea        extends Tag { constructor(c){ super("textarea",   c); } }
export class TableFoot       extends Tag { constructor(c){ super("tfoot",      c); } }
export class TableHeader     extends Tag { constructor(c){ super("th",         c); } }
export class TableHead       extends Tag { constructor(c){ super("thead",      c); } }
export class TableBody       extends Tag { constructor(c){ super("tbody",      c); } }
export class TableRow        extends Tag { constructor(c){ super("tr",         c); } }
export class TableData       extends Tag { constructor(c){ super("td",         c); } }
export class FieldSet        extends Tag { constructor(c){ super("fieldset",   c); } }
export class Time            extends Tag { constructor(c){ super("time",       c); } }
export class Title           extends Tag { constructor(c){ super("title",      c); } }
export class Track           extends Tag { constructor(c){ super("track",      c); } }
export class Template        extends Tag { constructor(c){ super("template",   c); } }
export class UnorderedList   extends Tag { constructor(c){ super("ul",         c); } }
export class Var             extends Tag { constructor(c){ super("var",        c); } }
export class Video           extends Tag { constructor(c){ super("video",      c); } }
export class WBR             extends Tag { constructor(c){ super("wbr",        c); } }

const SVG_NS = "http://www.w3.org/2000/svg";

class BaseSVG extends Tag
{
  static GetRegisterParent(){ return Tag; }

  constructor(type, cls)
  {
    super(document.createElementNS(SVG_NS, type), cls);
  }

  Text(text)
  {
    this.GetNode().textContent = text;
    return this;
  }

  ViewBox(value){ return this.SetAttribute("viewBox", value); }
  Fill(value){ return this.SetAttribute("fill", value); }
  R(value){ return this.SetAttribute("r", value); }
  X(value){ return this.SetAttribute("x", value); }
  Y(value){ return this.SetAttribute("y", value); }
  CX(value){ return this.SetAttribute("cx", value); }
  CY(value){ return this.SetAttribute("cy", value); }
  FontSize(value){ return this.SetAttribute("font-size", value); }
  TextAnchor(value){ return this.SetAttribute("text-anchor", value); }
}

export class SVG extends BaseSVG { constructor(c){ super("svg", c).SetAttribute("version", "1.1"); } }
export class GSVG extends BaseSVG { constructor(c){ super("g", c); } }
export class PathSVG extends BaseSVG { constructor(c){ super("path", c); } }
export class TextSVG extends BaseSVG { constructor(c){ super("text", c); } }
export class RectSVG extends BaseSVG { constructor(c){ super("rect", c); } }
export class CircleSVG extends BaseSVG { constructor(c){ super("circle", c); } }

export class HTML extends Tag
{
  constructor(c)
  {
    super("html", c);
    // document.documentElement = this.node;
    document.replaceChild(this.node, document.documentElement);
  }
}

class XML extends Tag
{
  constructor(type)
  {
    super(type);
  }

  // Since an XML tag is created differently (it uses its Render parameter as its node type),
  // it needs to be loaded from JSON differently
  static fromJSON(json)
  {
    const tag = new this();
    tag.Render(json.name);

    const keys = Object.keys(json.attributes);
    for (let i = 0; i < keys.length; i++)
    {
      const key = keys[i];
      const val = json.attributes[key];

      tag.SetAttribute(key, val);
    }

    if (json.text)
    {
      tag.Text(json.text);
    }
    else
    {
      for (let i = 0; i < json.children.length; i++)
      {
        tag.Add(Tag.fromJSON(json.children[i]));
      }
    }

    return tag;
  }
}

export class Meta extends Tag
{
  constructor(c)
  {
    super("meta", c);
  }

  Charset(v){ return this.SetAttribute("charset", v); }
  Content(v){ return this.SetAttribute("content", v); }
  Property(v){ return this.SetAttribute("property", v); }

  // Open Graph Properties
  Title      (v){ return this.Property("og:title"           ).Content(v); }
  Description(v){ return this.Property("og:description"     ).Content(v); }
  Determiner (v){ return this.Property("og:determiner"      ).Content(v); }
  Locale     (v){ return this.Property("og:locale"          ).Content(v); }
  LocaleAlt  (v){ return this.Property("og:locale:alternate").Content(v); }
  SiteName   (v){ return this.Property("og:site_name"       ).Content(v); }
  Type       (v){ return this.Property("og:type"            ).Content(v); }
  Url        (v){ return this.Property("og:url"             ).Content(v); }

  EnUS(v){ return this.Locale("en_US"); }
  EnGB(v){ return this.Locale("en_GB"); }
  DeterminerA(v){ return this.Determiner("a"); }
  DeterminerAn(v){ return this.Determiner("an"); }
  DeterminerThe(v){ return this.Determiner("the"); }
  DeterminerBlank(v){ return this.Determiner(""); }
  DeterminerAuto(v){ return this.Determiner("auto"); }

  ImageURL      (v){ return this.Property("og:image"           ).Content(v); }
  ImageSecureURL(v){ return this.Property("og:image:secure_url").Content(v); }
  ImageType     (v){ return this.Property("og:image:type"      ).Content(v); }
  ImageWidth    (v){ return this.Property("og:image:width"     ).Content(v); }
  ImageHeight   (v){ return this.Property("og:image:height"    ).Content(v); }
  ImageAlt      (v){ return this.Property("og:image:alt"       ).Content(v); }

  VideoURL      (v){ return this.Property("og:video"           ).Content(v); }
  VideoSecureURL(v){ return this.Property("og:video:secure_url").Content(v); }
  VideoType     (v){ return this.Property("og:video:type"      ).Content(v); }
  VideoWidth    (v){ return this.Property("og:video:width"     ).Content(v); }
  VideoHeight   (v){ return this.Property("og:video:height"    ).Content(v); }

  AudioURL      (v){ return this.Property("og:audio"           ).Content(v); }
  AudioSecureURL(v){ return this.Property("og:audio:secure_url").Content(v); }
  AudioType     (v){ return this.Property("og:audio:type"      ).Content(v); }

  ImageTypeJPG(){ return this.ImageType("image/jpg"); }
  ImageTypePNG(){ return this.ImageType("image/png"); }
  VideoTypeMP4(){ return this.VideoType("video/mp4"); }
  AudioTypeMP3(){ return this.AudioType("audio/mp3"); }
  TypeWebsite(){ return this.Type("website"); }
  TypeArticle(){ return this.Type("article"); }
}

export class Script extends Tag
{
  constructor(c)
  {
    super("script", c);
  }

  Body(fn)
  {
    const text = fn.toString();
    // const body = text.replace(/.+\{(.*)\}/s, (m, p1) =>
    const body = text.replace(/.+\{(.*)\}/, (m, p1) =>
    {
      return p1.split("\r\n").map(l => l.trim()).join("\r\n");
    });

    this.Text(body);
    return this;
  }
}

export class Error extends Tag
{
  static GetName(){ return "Error"; }

  constructor(error)
  {
    super("div", "error");
    this.error = error;

    console.error(error);
  }

  Render()
  {
    return super.Render(
      Tag.Div("content is-medium").Add(
        Tag.Div("notification is-danger").Add(
          Tag.Paragraph().Add(
            Tag.Span().Text(this.error.constructor.name),
            Tag.Span().Text(": "),
            Tag.Span().Text(this.error.message),
          ),
          this.error.stack && Tag.Div().Add(
            Tag.Div("content is-small").Add(
              Tag.Pre().Style("white-space: pre-wrap;").Add(
                Tag.Code().Text(this.error.stack),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

export class Promise extends Tag
{
  static GetName(){ return "Promise"; }

  constructor(value)
  {
    super("div", "promise");
    this.value = value;
  }
}

export class String extends Tag
{
  static GetName(){ return "String"; }

  constructor(value)
  {
    super(document.createTextNode(value));
    this.value = value;
  }
}

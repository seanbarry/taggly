import {Tag} from "/js/Tag.js";

const IMAGE_DIMENSIONS = new Set([
  "is-square",
  "is-16x16",
  "is-24x24",
  "is-32x32",
  "is-48x48",
  "is-64x64",
  "is-96x96",
  "is-96x96",
  "is-128x128",
  "is-1by1",
  "is-5by4",
  "is-4by3",
  "is-5by3",
  "is-16by9",
  "is-2by1",
  "is-3by1",
  "is-4by5",
  "is-3by4",
  "is-2by3",
  "is-3by5",
  "is-9by16",
  "is-1by2",
  "is-1by3",
]);

function GenerateRichTextHelper(element)
{
  const rich = {};

  rich.type = element.tagName;

  switch (element.tagName)
  {
    case "P":
    {
      break;
    }
    case "A":
    {
      rich.href = element.getAttribute("href");
      rich.text = element.innerText;
      break;
    }
    case "IMG":
    {
      rich.src = element.getAttribute("src");
      rich.alt = element.getAttribute("alt");
      break;
    }
    case "SPAN":
    {
      rich.text = element.innerText;
      break;
    }
    case "BR":
    {
      return;
    }
    case "DIV":
    {
      rich.text = element.innerText;
      break;
    }
    default:
    {
      throw new Error(`Unknown RichText type: "${element.tagName}"`);
    }
  }

  if (element.children && element.children.length > 0)
  {
    rich.data = GenerateRichText(element.children);
  }

  if (element.classList && element.classList.length > 0)
  {
    rich.mods = [];
    for (let i = 0; i < element.classList.length; i++)
    {
      rich.mods.push(element.classList[i]);
    }
  }

  return rich;
}

function GenerateRichText(children)
{
  const data = [];

  for (let i = 0; i < children.length; i++)
  {
    const rich = GenerateRichTextHelper(children[i]);
    if (rich) data.push(rich);
  }

  return data;
}

class RichTextBuilder
{
  Data(element)
  {
    const name = element.tagName || "";

    const fn = this.constructor.prototype[name];
    if (typeof(fn) === "function")
    {
      return fn.call(this, element);
    }
  }

  Next(element)
  {
    const data = [];

    if (element.children && element.children.length > 0)
    {
      for (let i = 0; i < element.children.length; i++)
      {
        const child = element.children[i];
        const result = this.Data(child);
        if (result) data.push(result);
      }
    }
    else
    {
      data.push(element.innerText);
    }

    return data;
  }

  BR(e){ return { type: "BREAK" }; }
  HR(e){ return { type: "RULE" }; }

  P(e)
  {
    return {
      type: "PARAGRAPH",
      data: this.Next(e),
    };
  }

  A(e)
  {
    return {
      type: "LINK",
      href: e.getAttribute("href"),
      data: this.Next(e),
    };
  }

  TIME(e)
  {
    return {
      type: "TIME",
      data: this.Next(e),
    };
  }

  QUOTE(e)
  {
    return {
      type: "QUOTE",
      user_id: e.getAttribute("user_id"),
      data: this.Next(e),
    };
  }

  ABBR(e)
  {
    return {
      type: "ABBREVIATION",
      data: this.Next(e),
    };
  }

  IMG(e)
  {
    return {
      type: "IMAGE",
      src: e.getAttribute("src"),
      alt: e.getAttribute("alt"),
      dimensions: e.getAttribute("dimensions"),
    };
  }

  VIDEO(e)
  {
    return {
      type: "VIDEO",
      src: e.getAttribute("src"),
      alt: e.getAttribute("alt"),
    };
  }

  AUDIO(e)
  {
    return {
      type: "AUDIO",
      src: e.getAttribute("src"),
      alt: e.getAttribute("alt"),
    };
  }

  EM(e)
  {
    return {
      type: "BOLD",
      data: this.Next(e),
    };
  }

  I(e)
  {
    return {
      type: "ITALIC",
      data: this.Next(e),
    };
  }

  CODE(e)
  {
    return {
      type: "CODE",
      data: this.Next(e),
    };
  }

  H1(e)
  {
    return {
      type: "HEADER",
      size: 1,
      data: this.Next(e),
    };
  }

  H2(e)
  {
    return {
      type: "HEADER",
      size: 2,
      data: this.Next(e),
    };
  }

  H3(e)
  {
    return {
      type: "HEADER",
      size: 3,
      data: this.Next(e),
    };
  }

  H4(e)
  {
    return {
      type: "HEADER",
      size: 4,
      data: this.Next(e),
    };
  }

  H5(e)
  {
    return {
      type: "HEADER",
      size: 5,
      data: this.Next(e),
    };
  }

  H6(e)
  {
    return {
      type: "HEADER",
      size: 6,
      data: this.Next(e),
    };
  }

  OL(e)
  {
    return {
      type: "ORDERED_LIST",
      data: this.Next(e),
    };
  }

  UL(e)
  {
    return {
      type: "UNORDERED_LIST",
      data: this.Next(e),
    };
  }
}

class RichTextNode
{
  constructor({type})
  {
    this.type = type;
  }
}

export class RichText extends Tag
{
  Data(item)
  {
    switch (typeof(item))
    {
      case "string": return item;
      case "object":
      {
        if (typeof(item.type) !== "string") throw new Error(`Invalid type for data item ${i}.`);

        const type = item.type.toUpperCase();

        const fn = this.constructor.prototype[type];
        if (typeof(fn) === "function")
        {
          return fn.call(this, item);
        }
      }
      default:
      {
        throw new Error(`Unknown data type: "${typeof(item)}"`);
      }
    }
  }

  Next(data)
  {
    if (!data) return;

    const results = [];
    for (let i = 0; i < data.length; i++)
    {
      const item = data[i];

      const result = this.Data(item);
      if (result !== undefined) results.push(result);
    }

    return results;
  }

  PARAGRAPH({data}){ return Tag.Paragraph().Add(this.Next(data)); }
  LINE({data}){ return Tag.Span().Add(this.Next(data)); }
  LINK({href, data}){ return Tag.Anchor().HRef(href).Add(this.Next(data)); }
  TIME({data}){ return Tag.Time().Add(this.Next(data)); }
  RULE(){ return Tag.HorizontalRule(); }
  BREAK(){ return Tag.BreakLine(); }
  QUOTE({user_id, data}){ return Tag.Embed().Add(this.Next(data)); }
  ABBREVIATION({user_id, data}){ return Tag.Abbreviation().Add(this.Next(data)); }

  IMAGE({src, alt, dimensions})
  {
    return Tag.Figure("image").AddClass(IMAGE_DIMENSIONS.has(dimensions) ? dimensions : "").Add(
      Tag.Image(dimensions === "is-rounded" ? "is-rounded" : undefined).Src(src).Alt(alt),
    );
  }

  VIDEO({src}){ return Tag.Video().Controls(true).Src(src); }
  AUDIO({src}){ return Tag.Audio().Controls(true).Src(src); }

  BOLD({data}){ return Tag.Strong().Add(this.Next(data)); }
  ITALIC({data}){ return Tag.Emphasis().Add(this.Next(data)); }
  CODE({data, language}){ return Tag.Code().Add(this.Next(data)); }

  HEADER({size, data})
  {
    switch (size)
    {
      case 1: return Tag.H1().Add(this.Next(data));
      case 2: return Tag.H2().Add(this.Next(data));
      case 3: return Tag.H3().Add(this.Next(data));
      case 4: return Tag.H4().Add(this.Next(data));
      case 5: return Tag.H5().Add(this.Next(data));
      case 6: return Tag.H6().Add(this.Next(data));
      default: throw new Error(`Size ${size} is not a valid for a Header Tag. Must be 1 - 6.`);
    }
  }

  ORDERED_LIST({data})
  {
    return Tag.OrderedList().Add(
      data.map(d =>
        Tag.ListItem().Add(
          this.Next(d)
        ),
      ),
    );
  }

  UNORDERED_LIST({data})
  {
    return Tag.UnorderedList().Add(
      data.map(d =>
        Tag.ListItem().Add(
          this.Next(d)
        ),
      ),
    );
  }

  POLL({data}){ throw new Error("Poll is not yet implemented"); }
  SUPERSCRIPT({data}){ throw new Error("Superscript is not yet implemented"); }
  SUBSCRIPT({data}){ throw new Error("Subscript is not yet implemented"); }
  TABLE({data}){ throw new Error("Table is not yet implemented"); }
  TITLE({data}){ throw new Error("Title is not yet implemented"); }
  SUBTITLE({data}){ throw new Error("Subtitle is not yet implemented"); }

  // Data(data){ this.data = data; return this; }

  Render()
  {
    return super.Render(
      Tag.Div("content").Save(this, "content").Add(
        this.Next(this.data),
      ),
    );
  }

  Build(ctor = RichTextBuilder)
  {
    const builder = new ctor();
    return builder.Data(this.current.Get());
  }
}

RichText.Register();

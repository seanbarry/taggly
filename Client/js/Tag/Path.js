import {Tag} from "/js/Tag.js";
import {Url} from "/js/Utility/Url.js";

export class PathParts
{
  static Register(){}

  constructor(url)
  {
    if (typeof(url) !== "string")
    {
      throw new Error(`Invalid url string`);
    }

    this.base_url = url;
    this.url = url.replace(/^\/|\/$/g, "").toLowerCase();
    this.parts = this.url.split("/");
    this.index = 0;
    this.tags = [];
    this.parameters = {};
  }

  Push(tag)
  {
    this.tags.push(tag);
    return this;
  }

  Save(key, value)
  {
    this.parameters[key] = value;
    return this;
  }

  Next()
  {
    this.index += 1;
    return this;
  }

  Prev()
  {
    this.index -= 1;
    return this;
  }

  Create()
  {
    if (this.page)
    {
      return new this.page(this.parameters);
    }
  }

  Page(page){ this.page = page; return this; }

  GetSlice(index = this.index){ return this.parts.slice(index); }
  GetPart(index = this.index){ return this.parts[index]; }
  GetTag(index = this.index){ return this.tags[index]; }
  GetLastTag(){ return this.tags[this.tags.length - 1]; }
  GetParameter(key){ return this.parameters[key]; }
  GetBaseUrl(){ return this.base_url; }
  GetUrl(){ return this.url; }
  GetParts(){ return this.parts; }
  GetIndex(){ return this.index; }
  GetTags(){ return this.tags; }
  GetPage(){ return this.page; }
  GetParameters(){ return this.parameters; }
}

export class Path extends Tag
{
  constructor(type)
  {
    super("path", type);
  }

  Test(url)
  {
    const children = this.node.children;
    for (let i = 0; i < children.length; i++)
    {
      const child = children[i].tag;

      if (child && child.Test)
      {
        const result = child.Test(url);

        if (result) return result;
      }
    }
  }
}

export class Root extends Path
{
  constructor(...roots)
  {
    super("root");

    this.roots = roots;
    this.SetAttribute("roots", roots.join(", "));
  }

  Test(url)
  {
    // for (let i = 0; i < this.matches.length; i++)
    // {
    //   const match = this.matches[i];
    //
    //   if (match === url.Get())
    //   {
    //     return super.Test(url.Push(this).Save(url.GetIndex(), match).Next());
    //   }
    // }

    return super.Test(url.Push(this));
  }

  // Parse(url)
  // {
  //   const parts = new PathParts(url);
  //   this.Test(parts);
  //
  //   return parts;
  //   // return this.Test(parts);
  // }
}

export class Page extends Path
{
  constructor(page, path)
  {
    super("page");
    this.page = page;
    this.path = path;

    // this.meta_tags = [];
    if (typeof(page) === "string")
    {
      this.SetAttribute("page", page);
    }
    else
    {
      this.SetAttribute("page", page.name);
    }
  }

  Test(url)
  {
    if (typeof(this.page) === "string")
    {
      const name = this.page;
      this.page = window.app.Import(this.path).then(mod =>
      {
        console.log("Imported", this.path);
        if (!mod[name])
        {
          throw new Error(`Invalid export "${name}" from "${this.path}"`);
        }

        return mod[name];
      });
    }

    return super.Test(url.Push(this)) || this.page;
  }
}

export class Redirect extends Path
{
  constructor(redirect)
  {
    super("redirect");

    // this.meta_tags = [];
    this.redirect = redirect;
    this.SetAttribute("redirect", redirect);
  }

  Test(url)
  {
    // Append any unused url parts to the end of the redirect url
    const redirect = this.redirect + "/" + url.GetSlice().join("/");

    // console.log({ redirect });

    window.app.Redirect(redirect);

    return url.GetTag(0).Test(url.Push(this).Redirect(redirect));
    // return url.GetTag(0).Test(new Url(redirect));
    // return url.GetTag(0).Test(new Url(redirect));
  }
}

export class Match extends Path
{
  constructor(...matches)
  {
    super("match");

    this.matches = matches;
    this.SetAttribute("matches", matches.join(", "));
  }

  Test(url)
  {
    for (let i = 0; i < this.matches.length; i++)
    {
      const match = this.matches[i];

      if (match === url.GetPart())
      {
        return super.Test(url.Push(this).Save(url.GetIndex(), match).Next());
      }
    }
  }
}

export class String extends Path
{
  static GetName(){ return "String"; }

  constructor(name)
  {
    super("string");

    this.name = name;
    this.SetAttribute("name", name);
  }

  Test(url)
  {
    const part = url.GetPart();

    if ((typeof(part) === "string") && (part.length > 0))
    {
      return super.Test(url.Push(this).Save(this.name, part).Next());
    }
  }
}

export class ObjectID extends Path
{
  constructor(name)
  {
    super("object-id");

    this.name = name;
    this.SetAttribute("name", name);
  }

  Test(url)
  {
    const part = url.GetPart();

    const reg = /[0-9A-Fa-f]{6}/g;

    if (typeof(part) !== "string") return;
    if (part.length !== 24) return;

    // Make sure the part is a hex string
    if (!reg.test(part)) return;

    return super.Test(url.Push(this).Save(this.name, part).Next());
  }
}

export class Number extends Path
{
  static GetName(){ return "Number"; }

  constructor(name)
  {
    super("path-number");

    this.name = name;
    this.SetAttribute("name", name);
  }

  Positive(v = true){ return this.SetAttribute("positive", v); }
  Negative(v = true){ return this.SetAttribute("negative", v); }
  Int(v = true){ return this.SetAttribute("int", v); }
  Float(v = true){ return this.SetAttribute("float", v); }

  Test(url)
  {
    const part = window.Number(url.GetPart());

    if (typeof(part) !== "number") return;
    if (window.Number.isNaN(part)) return;

    if      (this.HasAttribute("positive") && 0 > part) return;
    else if (this.HasAttribute("negative") && 0 < part) return;

    if      (this.HasAttribute("int"  ) && !window.Number.isInteger(part)) return;
    else if (this.HasAttribute("float") &&  window.Number.isInteger(part)) return;

    return super.Test(url.Push(this).Save(this.name, part).Next());
  }
}

Path.Register();
Root.Register();
Page.Register();
Redirect.Register();
Match.Register();
String.Register();
ObjectID.Register();
Number.Register();

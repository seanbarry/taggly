export class Line
{
  constructor(a, b)
  {
    this.a = a;
    this.b = b;
  }
}

export class Timeline
{
  constructor(points)
  {
    this.frames = [];

    for (let i = 0; i < (points.length - 1); i++)
    {
      const a = points[i];
      const b = points[i + 1];

      this.frames.push(new Line(a, b));
    }
  }
}

export class Opacity extends Timeline
{
  constructor(offset)
  {
    this.offset = offset;
  }
}

export class Animation
{
  constructor(duration)
  {
    this.frames = {};
  }

  Add(ctor, frames)
  {
    for (let i = 0; i < (frames.length - 1); i++)
    {
      const a = frames[i];
      const b = frames[i + 1];

      this.frames.push(new ctor(a, b));
    }
  }

  Opacity(...a){ return this.Add(Opacity, a); }
}

function Example()
{
  const anim = new Animation().Opacity(1.0, 0.0, 1.0, 0.5);
}

// class Timeline
// {
//   constructor(animation, points)
//   {
//     this.animation = animation;
//     this.points = points;
//     // this.value = points[0] || 0;
//     this.index = 0;
//   }
//
//   Tick(tag, elapsed, duration)
//   {
//     const progress = Math.min(1, elapsed / duration);
//
//     const offset = duration / (this.points.length - 2);
//     const next = offset * this.index;
//
//     const current = elapsed / (this.index + 1);
//     const end = next - duration;
//
//     const remaining = next - elapsed;
//     // const progress = 1 / (next / remaining);
//
//     // const progress = elapsed / duration;
//     // const index = Math.floor((this.points.length) * progress);
//
//     const a = this.points[this.index];
//     const b = this.points[this.index + 1];
//     // const next = this.points[index];
//
//     // this.value = next * progress;
//
//     console.log("Tick", progress, a, b);
//     if (elapsed >= next)
//     {
//       console.log("~~NEXT~~");
//       // this.current = 0;
//       this.index += 1;
//     }
//
//     // const each = duration / (this.points.length - 1);
//     // const next = index * each;
//
//     // this.Update(tag, elapsed, next, this.points[index]);
//   }
// }
//
// class Opacity extends Timeline
// {
//   Update(tag, percent)
//   {
//     // console.log("Updating Opacity", percent);
//     const target = this.GetTarget(percent);
//     const scaled = this.GetScaled(percent);
//
//     // this.value = ((target - this.value) * scaled);
//     this.value = ((this.value / target) * scaled);
//
//     console.log({ opacity: this.value });
//
//     // console.log({ percent, target, scaled, });
//   }
//
//   Update(tag, elapsed, duration, point)
//   {
//     this.value = ((elapsed / duration) * point);
//     console.log("Updating Opacity", this.value);
//     // console.log("Updating Opacity", elapsed, duration, point);
//     // console.log("Updating Opacity", (elapsed / duration) * point);
//   }
// }

export class Line
{
  constructor(a, b)
  {
    this.a = a;
    this.b = b;
  }
}

export class Timeline
{
  constructor(animation, points)
  {
    this.animation = animation;
    this.lines = [];
    this.index = 0;
    this.value = 0;
    this.time = 0;

    for (let i = 0; i < (points.length - 1); i++)
    {
      const a = points[i];
      const b = points[i + 1];

      this.lines.push(new Line(a, b));
    }

    console.log(this.lines);
  }

  Build(tag, duration)
  {
    const each = duration / this.lines.length;
    console.log("Building", duration, each);
    for (let i = 0; i < this.lines.length; i++)
    {
      const line = this.lines[i];
      line.duration = each;
    }
  }

  Tick(tag, time)
  {
    // const progress = Math.min(1, elapsed / duration);
    // console.log("Tick", time);

    this.time += time;

    const {a, b, duration} = this.lines[this.index];
    const average = (a + b) / 2;
    const step = time / duration;

    const temp = (a + b) * Math.sin(step);
    // const temp = (b - a) / (a * step);
    // console.log(temp);
    this.value += temp;

    // const temp = Math.sqrt(Math.pow(a * step, 2) + Math.pow(b * step, 2));
    // console.log((a * step) + (b * step));

    // console.log();

    // this.value += ((a * step) + (b * step));
    // this.value += temp;
    console.log(this.value);

    if (this.time > duration)
    {
      console.log("~Next~");
      this.time -= duration;
      // this.time = 0;
      this.index += 1;
    }
  }
}

export class Opacity extends Timeline
{
  // constructor(offset)
  // {
  //   this.offset = offset;
  // }
}

class Animation
{
  constructor(tag)
  {
    this.tag = tag;
    this.updator = this.Update.bind(this);
    this.frame = 0;
    this.frames = {};
  }

  Frame(key, value)
  {
    this.frames[key] = value;
    return this;
  }

  // SetPercent(k, v){ return this.Set(k, `${v}%`); }

  Opactity(...a){ return this.Frame("opacity", new Opacity(this, a)); }

  Update(time)
  {
    if (this.frame === 0)
    {
      this.start = time;
      this.time = time;
    }

    this.change = time - this.time;
    this.time = time;
    this.elapsed = time - this.start;
    this.remaining = this.duration - this.elapsed;
    this.frame += 1;
    this.percent = Math.min(1.0, this.elapsed / this.duration);

    // this.elapsed = time - this.start;
    // this.time = time;
    // this.remaining = (this)

    // element.style.transform = 'translateX(' + Math.min(progress / 10, 200) + 'px)';

    // console.log("Updating", this.remaining);

    for (let i = 0; i < this.keys.length; i++)
    {
      const key = this.keys[i];
      const frame = this.frames[key];
      frame.Tick(this.tag, this.change);
    }

    if (this.elapsed < this.duration)
    {
      return window.requestAnimationFrame(this.updator);
    }
  }

  Start()
  {
    this.keys = Object.keys(this.frames);

    for (let i = 0; i < this.keys.length; i++)
    {
      const key = this.keys[i];
      const frame = this.frames[key];
      frame.Build(this.tag, this.duration);
    }

    // this.start = performance.now();
    // this.time = this.start;
    // this.end = this.start + this.duration;

    return window.requestAnimationFrame(this.updator);
  }

  Play(duration)
  {
    this.duration = duration;

    return new Promise((resolve, reject) =>
    {
      this.resolve = resolve;
      this.reject = reject;
      this.Start();
    });
  }
}

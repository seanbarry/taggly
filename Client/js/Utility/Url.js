export class Url
{
  constructor(url)
  {
    if (typeof(url) !== "string")
    {
      throw new Error(`Invalid url string "${url}"`);
    }

    const temp = new URL(url, window.location.origin);

    this.base_url = url;
    this.url = temp.pathname.replace(/^\/|\/$/g, "").toLowerCase();
    this.encoded_url = encodeURIComponent(this.url);
    this.parts = this.url.split("/");
    this.index = 0;
    this.tags = [];
    this.parameters = {};
    this.search = temp.searchParams;
  }

  Redirect(url)
  {
    if (typeof(url) !== "string")
    {
      throw new Error(`Invalid url string "${url}"`);
    }

    const temp = new URL(url, window.location.origin);

    this.base_url = url;
    this.url = temp.pathname.replace(/^\/|\/$/g, "").toLowerCase();
    this.encoded_url = encodeURIComponent(this.url);
    this.parts = this.url.split("/");
    this.index = 0;
    this.tags = [];
    this.parameters = {};
    this.search = temp.searchParams;

    return this;
  }

  Push(tag)
  {
    this.tags.push(tag);
    return this;
  }

  Save(key, value)
  {
    this.parameters[key] = value;
    return this;
  }

  Next()
  {
    this.index += 1;
    return this;
  }

  Prev()
  {
    this.index -= 1;
    return this;
  }

  Create()
  {
    if (this.page)
    {
      return new this.page(this.parameters);
    }
  }

  // Page(page){ this.page = page; return this; }

  GetSlice(index = this.index){ return this.parts.slice(index); }
  GetPart(index = this.index){ return this.parts[index]; }
  GetTag(index = this.index){ return this.tags[index]; }
  GetLastTag(){ return this.tags[this.tags.length - 1]; }
  GetParameter(key){ return this.parameters[key]; }
  GetSearch(key){ return this.search.get(key); }
  GetBaseUrl(){ return this.base_url; }
  GetUrl(){ return this.url; }
  GetEncodedUrl(){ return this.encoded_url; }
  GetParts(){ return this.parts; }
  GetIndex(){ return this.index; }
  GetTags(){ return this.tags; }
  // GetPage(){ return this.page; }
  GetParameters(){ return this.parameters; }
}

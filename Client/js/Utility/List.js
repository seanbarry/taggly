export class Entry
{
  constructor(key, value)
  {
    this.key = key;
    this.value = value;
    this.next = undefined;
  }

  SetNext(entry){ this.next = entry; }

  GetNext(){ return this.next; }
  GetKey(){ return this.key; }
  GetValue(){ return this.value; }
}

export class List
{
  constructor()
  {
    this.head = undefined;
    this.size = 0;
  }

  // Override the compare function to use a different comparison
  Compare(a, b){ return a > b; }

  Insert(key, value)
  {
    const entry = new Entry(key, value);
    let current = this.head;


    if (current === undefined)
    {
      this.head = entry;
      this.size += 1;
      return entry;
    }
    else if (this.Compare(entry.key, current.key) === false)
    {
      this.head = entry;
      entry.next = current;
      this.size += 1;
      return entry;
    }
    else
    {
      while (current)
      {
        if (this.Compare(current.key, entry.key) === false)
        {
          const next = current.next;
          if (next === undefined)
          {
            current.next = entry;
            this.size += 1;

            return entry;
          }
          else if (this.Compare(next.key, entry.key) === true)
          {
            current.next = entry;
            entry.next = next;
            this.size += 1;

            return entry;
          }
        }

        current = current.next;
      }

      throw new Error(`Failed to insert ${key}`);
    }
  }

  Clear(){ this.head = undefined; }

  Remove(key)
  {
    let current = this.head;
    if (current.key === key)
    {
      this.head = current.next;
      this.size -= 1;
      return current;
    }
    else
    {
      let prev;
      do
      {
        prev = current;
        current = current.next;

        if (current.key === key)
        {
          prev.next = current.next;
          this.size -= 1;
          return current;
        }
      }
      while (current);
    }

    throw new Error(`Failed to remove "${key}"`);
  }

  Pop()
  {
    const head = this.head;
    if (head)
    {
      this.size -= 1;
      this.head = head.next;
      return head;
    }
  }

  Find(key)
  {
    let current = this.head;
    while (current)
    {
      if (current.key === key) return current;
      else current = current.next;
    }
  }

  Get(key)
  {
    const entry = this.Find(key);
    if (entry) return entry;
    else throw new Error(`Failed to get "${key}"`);
  }

  ForEach(fn)
  {
    let current = this.head;
    while (current)
    {
      fn(current);
      current = current.next;
    }
  }

  Size(){ return this.size; }
  Head(){ return this.head; }
}

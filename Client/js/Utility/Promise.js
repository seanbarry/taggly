// import {window} from "/js/Window.js";
// import {Function} from "/js/Function.js";

const DOM_LOADED = new Promise((resolve, reject) =>
{
  window.addEventListener("DOMContentLoaded", event =>
  {
    resolve(event);
  }, { once: true });
});

const BEFORE_UNLOAD = new Promise((resolve, reject) =>
{
  window.addEventListener("beforeunload", event =>
  {
    resolve(event);
  }, { once: true });
});

const UNLOAD = new Promise((resolve, reject) =>
{
  window.addEventListener("unload", event =>
  {
    resolve(event);
  }, { once: true });
});

// Create a pseudo requestIdleCallback if none exists
// It's important to note that this is NOT the same behavior
window.requestIdleCallback ??= function(handler)
{
  const start = window.performance.now();

  function timeRemaining()
  {
    return Math.max(0, 50.0 - (window.performance.now() - start));
  }

  return window.setTimeout(function()
  {
    handler({
      didTimeout: false,
      timeRemaining,
    });
  }, 1);
}

window.cancelIdleCallback ??= function(id)
{
  window.clearTimeout(id);
}

// These constructors are not available as globals,
// but we can access them from an instance
const AsyncFunction = Object.getPrototypeOf(async function(){}).constructor;
const GeneratorFunction = Object.getPrototypeOf(function*(){}).constructor;
const AsyncGeneratorFunction = Object.getPrototypeOf(async function*(){}).constructor;

let profile_count = 1;
let queue;
let queued_tasks = [];
let frame;
let idle;
let tasks = 0;
export class PromiseUtilities
{
  static IsFunctionAsync(fn){ return fn instanceof AsyncFunction; }
  static IsFunctionGenerator(fn){ return fn instanceof GeneratorFunction; }
  static IsFunctionAsyncGenerator(fn){ return fn instanceof AsyncGeneratorFunction; }

  static Loaded(){ return DOM_LOADED; }
  static BeforeUnload(){ return BEFORE_UNLOAD; }
  static Unload(){ return UNLOAD; }

  static Loaded(fn)
  {
    if (!fn) return DOM_LOADED;
    else return DOM_LOADED.then(fn);
  }

  static BeforeUnload(fn)
  {
    if (!fn) return BEFORE_UNLOAD;
    else return BEFORE_UNLOAD.then(fn);
  }

  static Unload(fn)
  {
    if (!fn) return UNLOAD;
    else return UNLOAD.then(fn);
  }

  static Sleep(ms)
  {
    return new Promise(resolve => globalThis.setTimeout(resolve, ms));
  }

  static async Queue(promise)
  {
    const prev = queue;
    queue = promise;

    await prev;
    // await this.Sleep(1000);

    // return this.Sleep(1000).then(prev);
    return promise;

    // return queue = new Promise((resolve, reject) =>
    // {
    //   if (prev)
    //   {
    //     prev.then(resolve); // Wait for the previous promise to resolve
    //   }
    //   else
    //   {
    //     resolve();
    //   }
    // });
  }

  static async Queue(fn)
  {
    queued_tasks.push(fn);

    while (queued_tasks.length > 0)
    {
      const task = queued_tasks.shift();
      await task();
    }

    // const prev = queue;
    // queue = fn();
    //
    // await prev;
    // await this.Sleep(1000);

    // return this.Sleep(1000).then(prev);
    // return queue;

    // return queue = new Promise((resolve, reject) =>
    // {
    //   if (prev)
    //   {
    //     prev.then(resolve); // Wait for the previous promise to resolve
    //   }
    //   else
    //   {
    //     resolve();
    //   }
    // });
  }

  static Frame()
  {
    return frame ??= new Promise((resolve, reject) =>
    {
      window.requestAnimationFrame(dt =>
      {
        // Clear the frame, so that when AwaitAnimationFrame is called it generates a new promise
        frame = undefined;
        resolve(dt);
      });
    });
  }

  static Idle(options)
  {
    return idle ??= new Promise((resolve, reject) =>
    {
      window.requestIdleCallback(deadline =>
      {
        idle = undefined;
        resolve(deadline);
      }, options);
    });
  }

  static async Task(callback, options)
  {
    tasks += 1;

    const is_generator = this.IsFunctionGenerator(callback);
    const is_async_generator = this.IsFunctionAsyncGenerator(callback);

    if (!is_generator && !is_async_generator)
    {
      // Wait for the thread to be idle (on supporting browsers)
      const deadline = await this.Idle(options);

      // Then invoke the function
      return callback();
    }

    const generator = callback();
    let slowest = 0.1;
    let next;

    while (true)
    {
      // Wait for the thread to be idle (on supporting browsers)
      const deadline = await this.Idle(options);
      let remaining = deadline.timeRemaining();

      // Only run the next step if we estimate that it will
      // take less time than the remaining time
      while (remaining >= (slowest * 1))
      {
        if (is_generator)
        {
          next = generator.next();
        }
        else if (is_async_generator)
        {
          next = await generator.next();
        }

        if (next.done === true)
        {
          tasks -= 1;

          if (tasks === 0)
          {
            console.log(tasks, "tasks remaining");
          }

          return next.value;
        }
        else if (deadline.didTimeout === true)
        {
          console.log("Timed out!");
          break;
        }
        else
        {
          const prev = remaining;
          remaining = deadline.timeRemaining();

          // Track the slowest iteration, and use that to estimate if
          // we have enough time to perform another iteration
          const elapsed = prev - remaining;
          if (elapsed > slowest) slowest = elapsed;

          if (slowest >= remaining) // Ran out of time
          {
            // console.log("Ran out of time, looping again", remaining);
            break; // Break the inner loop to get a new deadline
          }
        }
      }
    }
  }

  static async QueueTask(...args)
  {
    // const promise = Promise.all(queued_tasks);
    // queued_tasks = [promise];
    await Promise.all(queued_tasks);

    const result = this.Task(...args);
    queued_tasks.unshift(result);

    result.then(() =>
    {
      for (let i = queued_tasks.length - 1; i >= 0; i--)
      {
        if (queued_tasks[i] === result)
        {
          queued_tasks.splice(i, 1);
          return;
        }
      }

      console.warn("Failed to remove the queued task");
    });

    return result;
  }

  static async Profile(callback, options = {})
  {
    const has_duration = options.hasOwnProperty("duration");
    const has_iterations = options.hasOwnProperty("iterations");
    const is_async = this.IsFunctionAsync(callback);
    const is_generator = this.IsFunctionGenerator(callback);
    const is_async_generator = this.IsFunctionAsyncGenerator(callback);
    const name = callback.name ?? `profile-${profile_count++}`;

    let generator;
    let next;
    let done = false;
    let idles = 0;
    let steps = 0;
    let total = 0; // Total run time in MS
    let average = 0;
    let fastest = Number.MAX_SAFE_INTEGER;
    let slowest = 0;

    if (is_generator || is_async_generator)
    {
      generator = callback();
    }

    while (true)
    {
      // Wait for the thread to be idle
      const deadline = await this.Idle(options);
      idles++;

      let remaining = deadline.timeRemaining();

      // Only run the next step if we estimate that it will take less
      // time than the remaining time
      while (remaining >= slowest)
      {
        if      (is_generator) next = generator.next();
        else if (is_async_generator) next = await generator.next();
        else if (is_async) await callback();
        else callback();

        if (has_duration)
        {
          done = total >= options.duration;
        }
        else if (has_iterations)
        {
          done = steps >= options.iterations;
        }
        else if (generator)
        {
          done = next.done;
        }
        else
        {
          throw new Error(`Promise.Task was not provided with a stopping condition, such as an iterations or duration number`);
        }

        if (done === true || deadline.didTimeout === true)
        {
          return {
            name,
            idles,
            steps,
            total,
            value: next?.value,
            average,
            fastest,
            slowest,
            timeout: deadline.didTimeout,
            nsper: average * (1000.0 * 1000.0),
          };
        }
        else
        {
          const prev = remaining;
          remaining = deadline.timeRemaining();
          const time = prev - remaining;

          steps++;

          if (time > slowest) slowest = time;
          if (time < fastest) fastest = time;

          total += time;
          average = total / steps;
        }
      }
    }
  }

  static DurationProfile(callback, duration){ return this.Profile(callback, { duration }); }
  static IterationProfile(callback, iterations){ return this.Profile(callback, { iterations }); }
  static TimeoutProfile(callback, timeout){ return this.Profile(callback, { timeout }); }

  static async Profiles(options)
  {
    const profiles = [];
    for (const key of Object.keys(options))
    {
      const value = options[key];

      if (typeof(value) === "function")
      {
        const profile = await this.Profile(value, options);
        profiles.push(profile);
      }
    }

    return profiles.sort((a, b) =>
    {
      return a.average - b.average;
    });
  }

  static Encode(buffer, value)
  {
    throw new Error(`A Promise cannot be encoded to a buffer, resolve it first`);
  }

  static Decode(buffer)
  {
    throw new Error(`A Promise cannot be decoded from a buffer, and shouldn't have ever been encoded`);
  }
}

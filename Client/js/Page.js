import {Tag} from "/js/Tag.js";
// import {Url} from "/js/Utility/Url.js";

const pages = [];
export class Page extends Tag
{
  // static Url(minimum)
  // {
  //   this.url = new Url(this, minimum);
  //   return this.url;
  // }

  static Register()
  {
    // pages.push(this);
    // return super.Register(this);
  }

  static Select(parts)
  {
    let best = undefined;

    for (let i = 0; i < pages.length; i++)
    {
      const page = pages[i];

      if (page && page.url)
      {
        const url = page.url.Evaluate(parts);

        if (best && url.GetScore() > best.GetScore())
        {
          best = url;
        }
        else if (url.GetScore() > 0.0)
        {
          best = url;
        }
      }
    }

    if (best)
    {
      const ctor = best.GetPage();
      return new ctor(best.GetParameters());
    }
  }

  static GetPages(){ return pages; }

  constructor(parameters = {})
  {
    super("div", "page");
    this.ID(this.GetKebabName());

    this.parameters = parameters;
  }

  Close()
  {
    console.log("Closing page");
  }

  ScrollToTop()
  {
    window.scrollTo(0, 0);
  }

  // AnimatePageIn()
  // {
  //   this.Animate(a => a.Opacity(0).Opacity(100).MS(100).Run());
  // }

  ScrollToTop()
  {
    // TODO: scroll options may not always be supported, so I may need a fallback of scrollTo?
    // window.scrollTo(0, 0);

    window.scroll({
      top: 0,
      left: 0,
      behavior: "smooth",
    });
  }

  GetRedirect(){ return "/"; }
  GetParameter(name){ return this.parameters[name]; }
  GetTitle(){ return this.constructor.name; }

  SetWindowTitle(title){ return window.app.SetTitle(title); }

  // GetTitle(){ return window.app.GetTitle(); }
  // GetURL(){ return window.app.GetURL(); }
  GetURL(){ return this.constructor.url.GetURL(); }
  GetType(){ return "website"; }
  GetDescription(){ return ""; }
  GetBaseURL(){ return "/"; }
  GetImage(){ return ""; }

  Preview(...tags)
  {
    return [
      Tag.Meta().Property("og:site_name").Content(window.app.GetName()),
      Tag.Meta().Property("og:title").Content(this.GetTitle()),
      // Tag.Meta().Property("og:type").Content("website"),
      // Tag.Meta().Property("og:description").Content(page.GetDescription()),
      Tag.Meta().Property("og:url").Content(`${window.app.GetBaseURL()}${page.GetURL()}`),
      // Tag.Meta().Property("og:image").Content(`${window.app.GetBaseURL()}${page.GetImage()}`),
      ...tags,
    ];
  }
}

import {TagData} from "/js/Tag/TagData.js";
import {EscapeCodes} from "/js/Utility/EscapeCodes.js";

// Shorter version for code style
const Esc = EscapeCodes.Escape;

const BRANCHES = new WeakMap();
const rendered = new WeakSet();

export class Tag
{
  static GetLocalName(){ return "div"; }

  static GetRegisterParent(){ return Object.getPrototypeOf(this); }
  static GetName(){ return this.name; }

  static Register(meta)
  {
    if (this === Tag) throw new Error(`Cannot register Tag base class`);
    // if (this.hasOwnProperty("_constructor")) throw new Error(`Tag class "${this.name}" is already registered`);

    const name = this.GetName();

    // const parent = Object.getPrototypeOf(this);
    const parent = this.GetRegisterParent();

    if (!parent.children) parent.children = {};
    parent.children[name] = this;

    this._meta = meta;
    this._constructor = this.Constructor.bind(this);
    this._constructor.Get = () => this;

    // console.log("Registering", name, "to", parent.GetName());

    const shortcut = `$${name}`;
    parent[name] = this._constructor;
    parent[shortcut] = this;

    if (parent !== Tag)
    {
      parent._constructor[name] = this._constructor;
      parent._constructor[shortcut] = this;
    }

    return this;
  }

  static Constructor(classes)
  {
    // console.log("Calling Constructor on", this.name);
    return new this(classes);
  }

  static New(name, classes)
  {
    if (!this.children[name]) throw new Error(`Invalid Tag type ${name}`);

    return new this.children[name](classes);
  }

  static NS(name)
  {
    if (this.children) return this.children[name];
  }

  static Query(selector, root = document)
  {
    const element = root.querySelector(selector);
    if (element)
    {
      if (!element.tag) console.warn(`Invalid tag field for element`, element);
      return element.tag;
    }
  }

  static QueryAll(selector, root = document)
  {
    const elements = root.querySelectorAll(selector);
    const tags = [];

    for (let i = 0; i < elements.length; i++)
    {
      const element = elements[i];

      if (!element.tag)
      {
        console.warn(`Element does not have a tag`, element);
        continue;
      }

      tags.push(element.tag);
    }

    return tags;
  }

  static QueryLast(selector, root = document)
  {
    const tags = this.QueryAll(selector, root);
    return tags[tags.length - 1];
  }

  static QueryPoint(selector, x = 0, y = 0, root = document)
  {
    // const elements = root.elementsFromPoint(x, y);
    //
    // for (let i = 0; i < elements.length; i++)
    // {
    //   const element = elements[i];
    //
    //   // Return the first element that matches the selector
    //   if (element && element.matches(selector))
    //   {
    //     if (!element.tag) console.warn(`Invalid tag field for element`, element);
    //     return element.tag;
    //   }
    // }

    return this.QueryAllPoint(selector, x, y, root).shift();
  }

  static QueryLastPoint(selector, x = 0, y = 0, root = document)
  {
    // const elements = root.elementsFromPoint(x, y);
    //
    // for (let i = elements.length - 1; i >= 0; i--)
    // {
    //   const element = elements[i];
    //
    //   // Return the LAST element that matches the selector (cause it's reverse iteration)
    //   if (element && element.matches(selector))
    //   {
    //     if (!element.tag) console.warn(`Invalid tag field for element`, element);
    //     return element.tag;
    //   }
    // }

    return this.QueryAllPoint(selector, x, y, root).pop();
  }

  static QueryAllPoint(selector, x = 0, y = 0, root = document)
  {
    const elements = root.elementsFromPoint(x, y);

    const tags = [];

    for (let i = 0; i < elements.length; i++)
    {
      const element = elements[i];
      if (!element.matches(selector)) continue;

      if (!element.tag)
      {
        // console.warn(`Element does not have a tag`, element);
        continue;
      }

      tags.push(element.tag);
    }

    return tags;
  }

  static QueryPointAll(...args)
  {
    console.warn("QueryPointAll is depreciated, use QueryAllPoint");
    return this.QueryAllPoint(...args);
  }

  Query(selector){ return this.constructor.Query(selector, this.node); }
  QueryAll(selector){ return this.constructor.QueryAll(selector, this.node); }
  QueryChildren(selector){ return this.GetChildren().filter(c => c.node.matches(selector)); }
  QueryChild(selector){ return this.QueryChildren(selector)[0]; }
  QueryLast(selector){ return this.constructor.QueryLast(selector, this.node); }
  QueryAncestor(selector){ return this.node.closest(selector)?.tag; }
  Matches(selector){ return this.node.matches(selector); }

  QueryAncestorAll(selector)
  {
    const tags = [];

    let ancestor = this.QueryAncestor(selector);
    while (ancestor)
    {
      tags.push(ancestor);
      ancestor = ancestor.GetParent()?.QueryAncestor(selector);
    }

    return tags;
  }

  QueryPrevSiblings(selector)
  {
    const tags = [];

    let prev = this.GetPrevSibling();
    while (prev)
    {
      if (prev.Matches(selector)) tags.push(prev);
      prev = prev.GetPrevSibling();
    }

    return tags;
  }

  QueryNextSiblings(selector)
  {
    const tags = [];

    let next = this.GetNextSibling();
    while (next)
    {
      if (next.Matches(selector)) tags.push(next);
      next = next.GetNextSibling();
    }

    return tags;
  }

  QueryPoint(selector, x, y)
  {
    const results = this.constructor.QueryAllPoint(selector, x, y);
    for (let i = 0; i < results.length; i++)
    {
      const result = results[i];

      if (result === this || this.IsAncestorOf(result))
      {
        return result;
      }
    }
  }

  QueryChildPoint(selector, x, y)
  {
    // Grab all children matching the selector
    const children = this.QueryChildren(selector);

    for (let i = 0; i < children.length; i++)
    {
      const child = children[i];

      // If it contains the point, return it
      if (child.IsPointInside(x, y))
      {
        return child;
      }
    }
  }

  QueryAllPoint(selector, x, y)
  {
    const results = this.constructor.QueryAllPoint(selector, x, y);
    return results.filter(result => result === this || this.IsAncestorOf(result));
  }

  static GetCache()
  {
    return this._cache || (this._cache = new WeakMap());
  }

  static ClearCache()
  {
    const cache = this.GetCache();
    cache.clear();
    return this;
  }

  static Uncache()
  {
    const cache = this.GetCache();
    cache.delete(this);
    return this;
  }

  constructor(type, classes)
  {
    if ((typeof(classes) === "object") && (classes instanceof window.Node) && (classes.localName === type))
    {
      type = classes;
      classes = undefined;
    }

    switch (typeof(type))
    {
      case "string":
      {
        this.node = document.createElement(type);
        break;
      }
      case "object":
      {
        this.node = type;
        break;
      }
      case "undefined":
      {
        this.node = document.createElement(this.constructor.GetLocalName() || "div");
        break;
      }
      default:
      {
        throw new Error(`Invalid type for Tag.${this.GetClassName()}`);
      }
    }

    this.node.tag = this;
    if (classes) this.node.className = classes;
  }

  Set(key, value)
  {
    this[key] = value;
    return this;
  }

  Push(key, value)
  {
    const array = this[key] || (this[key] = []);
    array.push(value);
    return this;
  }

  Cache()
  {
    const cache = this.constructor.GetCache();

    if (cache.has(this.constructor))
    {
      // console.log("Using existing cached value for", this.constructor.name);
      return cache.get(this.constructor);
    }
    else
    {
      // console.log("Adding new cached value for", this.constructor.name);
      cache.set(this.constructor, this);
      return this;
    }
  }

  HasParent(){ return !!this.node.parentElement; }
  HasChildren(){ return this.node.children.length > 0; }
  HasChildNodes(){ return this.node.hasChildNodes(); }
  GetChildNodes(){ return this.node.childNodes; }

  InsertUndefined(v, i){ return; }
  InsertBoolean(v, i){ return; }
  InsertSymbol(v, i){ return; }
  InsertNull(v, i){ return; }
  InsertString(v){ return this.InsertNode(document.createTextNode(Esc(v))); }
  InsertNumber(v){ return this.InsertNode(document.createTextNode(v.toString())); }
  InsertNumber(v){ return this.InsertString(v.toString()); }
  InsertFunction(v){ return this.Insert(v.call(this, this)); }

  InsertString(v)
  {
    const tag = Tag.String(v);

    return this.InsertTag(tag);
  }

  InsertNode(node)
  {
    if (!node) throw new Error(`Invalid node`);
    if (node === this.node) throw new Error(`Invalid node`);

    // this.node.appendChild(node);

    return node;
  }

  InsertTag(v)
  {
    if (v === this) return;

    // if (v.HasParent())
    // {
    //   // return console.error(`Tag cannot be added because it already has a parent`, v);
    //   throw new Error(`Tag cannot be added because it already has a parent`);
    // }

    const result = this.InsertNode(v.GetNode());

    if (v.rendered !== true)
    {
      Promise.resolve(v.Render()).catch(error =>
      {
        this.Add(error);
      });
    }

    return result;
  }

  InsertArray(v)
  {
    const fragment = document.createDocumentFragment();
    for (let i = 0; i < v.length; i++)
    {
      const node = this.Insert(v[i]);
      if (node) fragment.appendChild(node);
    }

    return this.InsertNode(fragment);
  }

  InsertCollection(v){ return this.InsertArray(v); }

  InsertIterable(v)
  {
    const fragment = document.createDocumentFragment();

    for (const object of v)
    {
      const node = this.Insert(object);
      if (node) fragment.appendChild(node);
    }

    return this.InsertNode(fragment);
  }

  InsertPromise(v)
  {
    // Create a placeholder element that will be replaced
    // when the promise resolves
    // const temp = document.createElement("div");
    const temp = Tag.Promise(v);

    v.then(result =>
    {
      const node = this.Insert(result);
      if (!temp.node.parentNode) return console.warn(`No parent node for temp`, temp.node);
      else if (node) temp.node.parentNode.replaceChild(node, temp.node);
      else temp.node.parentNode.removeChild(temp.node);
    })
    .catch(error =>
    {
      const node = this.InsertError(error);
      if (!temp.node.parentNode) return console.warn(`No parent node for temp`, temp.node);
      else if (node) temp.node.parentNode.replaceChild(node, temp.node);
      else temp.node.parentNode.removeChild(temp.node);
    });

    return this.InsertTag(temp);
  }

  InsertError(error)
  {
    const tag = Tag.Error(error);

    const event = new Event("tagerror", { bubbles: true, cancelable: true });
    event.error = error;
    this.Fire(event);

    return this.InsertTag(tag);
  }

  InsertObject(v)
  {
    if      (v === null) return this.InsertNull(v);
    else if (v instanceof Tag) return this.InsertTag(v);
    else if (v instanceof Array) return this.InsertArray(v);
    else if (v instanceof Promise) return this.InsertPromise(v);
    else if (v instanceof Node) return this.InsertNode(v);
    else if (v instanceof Error) return this.InsertError(v);
    else if (v instanceof HTMLCollection) return this.InsertCollection(v);
    else if (typeof(v[Symbol.iterator]) === "function") return this.InsertIterable(v);
    else return this.InsertString(v.toString())
    // else throw new Error(`Unknown object: ${v.constructor.name}`);
  }

  InsertUnknown(v){ throw new Error(`Unknown type "${typeof(v)}" in Tag.Insert`); }
  // InsertUnknown(v){ return this.InsertString(v.toString()); }

  Insert(v)
  {
    switch (typeof(v))
    {
      case "function": return this.InsertFunction(v);
      case "string": return this.InsertString(v);
      case "number": return this.InsertNumber(v);
      case "object": return this.InsertObject(v);
      case "undefined": return this.InsertUndefined(v);
      case "boolean": return this.InsertBoolean(v);
      case "symbol": return this.InsertSymbol(v);
      default: return this.InsertUnknown(v);
    }
  }

  Add(...values)
  {
    if (values.length > 0)
    {
      try
      {
        const node = this.InsertArray(values);
        if (node) this.node.appendChild(node);
      }
      catch (error)
      {
        const node = this.InsertError(error);
        if (node) this.node.appendChild(node);
      }
    }

    return this;
  }

  ConvertArrayToNodes(array)
  {
    // QUESTION: Is it faster to create a new array or use splice?

    // Convert each value to a node
    for (let i = array.length - 1; i >= 0; i--)
    {
      const value = this.Insert(array[i]);

      if (value) array[i] = value;
      else array.splice(i, 1);
    }

    return array;
  }

  Append(...values)
  {
    if (values.length > 0)
    {
      try
      {
        this.node.append.apply(this.node, this.ConvertArrayToNodes(values));
      }
      catch (error)
      {
        const node = this.InsertError(error);
        if (node) this.node.append(node);
      }
    }

    return this;
  }

  Prepend(...values)
  {
    if (values.length > 0)
    {
      try
      {
        this.node.prepend.apply(this.node, this.ConvertArrayToNodes(values));
      }
      catch (error)
      {
        const node = this.InsertError(error);
        if (node) this.node.prepend(node);
      }
    }

    return this;
  }

  Before(...values)
  {
    if (values.length > 0)
    {
      try
      {
        this.node.before.apply(this.node, this.ConvertArrayToNodes(values));
      }
      catch (error)
      {
        const node = this.InsertError(error);
        if (node) this.node.before(node);
      }
    }

    return this;
  }

  After(...values)
  {
    if (values.length > 0)
    {
      try
      {
        this.node.after.apply(this.node, this.ConvertArrayToNodes(values));
      }
      catch (error)
      {
        const node = this.InsertError(error);
        if (node) this.node.after(node);
      }
    }

    return this;
  }

  Replace(...values)
  {
    if (values.length > 0)
    {
      try
      {
        this.node.replaceChildren.apply(this.node, this.ConvertArrayToNodes(values));
      }
      catch (error)
      {
        const node = this.InsertError(error);
        if (node) this.node.replaceChildren(node);
      }
    }

    return this;
  }

  // Replace the current tag with the provided values
  Swap(...values)
  {
    if (values.length > 0)
    {
      try
      {
        this.node.replaceWith.apply(this.node, this.ConvertArrayToNodes(values));
      }
      catch (error)
      {
        const node = this.InsertError(error);
        if (node) this.node.replaceWith(node);
      }
    }

    return this;
  }

  TL(strings, ...values)
  {
    for (let i = 0; i < strings.length; i++)
    {
      this.Add(strings[i], values[i]);
    }

    return this;
  }

  Clear()
  {
    while (this.node.firstChild)
    {
      this.node.removeChild(this.node.firstChild);
    }

    // this.rendered = false;
    return this;
  }

  // Replace the current element with a clone, which gets rid of event handlers and children
  Reset()
  {
    const clone = this.node.cloneNode(false);
    this.node.parentNode.replaceChild(clone, this.node);
    this.node = clone;
    return this;
  }

  Copy()
  {
    // Create a copy of this tag, copy any children, and return it
    const clone = new this.constructor(this.node.cloneNode(false));

    if (this.HasChildNodes())
    {
      const nodes = this.GetChildNodes();

      for (let i = 0; i < nodes.length; i++)
      {
        const node = nodes[i];
        if (node.tag)
        {
          // Recursively copy and append
          clone.Append(node.tag.Copy());
        }
        else
        {
          // If it already wasn't a tag, then simply copy it and any descendents
          // over directly. If it has children that are tags, they won't be anymore
          // but whatever, I don't think I care about that edge case
          clone.node.append(node.cloneNode(true));
        }
      }
    }

    return clone;
  }

  ReplaceWith(tag)
  {
    this.node.parentNode.replaceChild(tag.node, this.node);

    return this;
  }

  InsertAtIndex(tag, index = 0)
  {
    this.node.insertBefore(tag.node, this.node.children[index]);
    return this;
  }

  GetIndex()
  {
    const parent = this.node.parentElement;
    if (!parent) throw new Error(`Cannot use GetIndex without a parent element`);

    const children = parent.children;
    for (let i = 0; i < children.length; i++)
    {
      if (children[i] === this.node)
      {
        return i;
      }
    }

    throw new Error(`Failed to find an index for node`);
  }

  Render(...values)
  {
    // this.Clear();
    this.rendered = true;

    return this.Add.apply(this, values);
  }

  Branch(callback)
  {
    BRANCHES.set(this, callback);
    this.SetAttribute("branch", "");

    return this;
  }

  Regrow(branches = 1)
  {
    if (BRANCHES.has(this))
    {
      const callback = BRANCHES.get(this);
      const result = callback();
      this.Swap(result); // Replace the branch with the result of the callback

      if (branches > 1)
      {
        this.GetParent()?.Regrow(branches - 1);
      }

      return result;
    }
    else
    {
      // Find the nearest branch
      return this.GetParent()?.Regrow(branches);
    }
  }

  // // Fire an event through this Tag
  // Fire(name, event = new Event(name, { cancelable: true }))
  // {
  //   event.tag = this;
  //   return this.node.dispatchEvent(event) === true;
  // }

  // Fire an event through this Tag
  Fire(event)
  {
    if (typeof(event) === "string")
    {
      event = new Event(event, { cancelable: true })
    }

    event.tag = this;

    return this.node.dispatchEvent(event) === true;
  }

  // Add a Control to the Tag
  Ctrl(ctor, ...args)
  {
    const ctrl = new ctor(this, args);
    return this;
  }

  Save(object, name)
  {
    if (name) object[name] = this;
    else object.push(this);

    return this;
  }

  // Set the Tag's Add target
  Trunk(tag)
  {
    tag.trunk = this;
    return this;
  }

  // Set the Tag's Remove target
  Root(tag)
  {
    this.root = tag;
    return this;
  }

  Parent(tag)
  {
    tag.Add(this);
    return this;
  }

  Remove()
  {
    // If a root tag is set, redirect to it
    if (this.root) return this.root.Remove();

    if (!this.node)
    {
      throw new Error(`Cannot Remove Tag.${this.GetClassName()} because it has no node`);
    }

    const parent = this.node.parentElement;
    if (!parent)
    {
      throw new Error(`Cannot Remove Tag.${this.GetClassName()} because it has no parent`);
    }

    // Remove it, unless the event got canceled
    if (this.Fire("removing"))
    {
      parent.removeChild(this.node);
    }

    return this;
  }

  ReplaceClass(a, b){ this.node.classList.replace(a, b); return this; }
  ToggleClass(v, force){ this.node.classList.toggle(v, force); return this; }
  HasClass(v){ return this.node.classList.contains(v); }
  GetClass(i){ return this.node.classList.item(i); }
  AddClass(...values){ this.node.classList.add.apply(this.node.classList, values.filter(c => !!c)); return this; }
  RemoveClass(...values){ this.node.classList.remove.apply(this.node.classList, values); return this; }
  OptionalClass(value, add){ return !!add ? this.AddClass(value) : this; }

  AddOptionalClasses(classes)
  {
    const keys = Object.keys(classes);
    for (let i = 0; i < keys.length; i++)
    {
      const key = keys[i];

      if (classes[key]) this.AddClass(key);
    }

    return this;
  }

  HasAttribute(name){ return this.node.hasAttribute(name); }
  GetAttribute(name){ return this.node.getAttribute(name); }
  AddAttribute(name){ this.node.setAttribute(name, ""); return this; }
  RemoveAttribute(name){ this.node.removeAttribute(name); return this; }
  SetAttribute(name, value = ""){ this.node.setAttribute(name, value); return this; }
  SetStateAttribute(name, value = true)
  {
    if (value === true) this.node.setAttribute(name, "");
    else this.node.removeAttribute(name);

    return this;
  }

  ToggleAttribute(name, value, force)
  {
    if (force === true)
    {
      this.SetAttribute(name, value);
    }
    else if (force === false)
    {
      this.RemoveAttribute(name);
    }
    else if (this.HasAttribute(name))
    {
      this.RemoveAttribute(name);
    }
    else
    {
      this.SetAttribute(name, value);
    }

    return this;
  }

  OptionalAttribute(name, value)
  {
    return this.ToggleAttribute(name, value, !!value);
  }

  // This has the default because Edge will render an undefined as "undefined"
  SetProperty(name, value = "")
  {
    this.node[name] = value;
    return this;
  }

  Style            (value){ this.node.style.cssText = value; return this; }
  Text             (value){ return this.SetProperty("textContent",       value ); }
  TextEsc          (value){ return this.SetProperty("textContent",   Esc(value)); }
  Class            (value){ return this.SetProperty("className",          value); }
  InnerHTML        (value){ return this.SetProperty("innerHTML",          value); }
  OuterHTML        (value){ return this.SetProperty("outerHTML",          value); }
  InnerText        (value){ return this.SetProperty("innerText",          value); }
  OuterText        (value){ return this.SetProperty("outerText",          value); }
  ID               (value){ return this.SetAttribute("id",                value); }
  Name             (value){ return this.SetAttribute("name",              value); }
  Placeholder      (value){ return this.SetAttribute("placeholder",       value); }
  Value            (value){ return this.SetAttribute("value",             value); }
  HRef             (value){ return this.SetAttribute("href",              value); }
  Src              (value){ return this.SetAttribute("src",               value); }
  SrcSet           (value){ return this.SetAttribute("srcset",            value); }
  Media            (value){ return this.SetAttribute("media",             value); }
  Rel              (value){ return this.SetAttribute("rel",               value); }
  Sizes            (value){ return this.SetAttribute("sizes",             value); }
  Color            (value){ return this.SetAttribute("color",             value); }
  Role             (value){ return this.SetAttribute("role",              value); }
  Type             (value){ return this.SetAttribute("type",              value); }
  Alt              (value){ return this.SetAttribute("alt",               value); }
  Min              (value){ return this.SetAttribute("min",               value); }
  Max              (value){ return this.SetAttribute("max",               value); }
  Step             (value){ return this.SetAttribute("step",              value); }
  For              (value){ return this.SetAttribute("for",               value); }
  Download         (value){ return this.SetAttribute("download",          value); }
  Method           (value){ return this.SetAttribute("method",            value); }
  Action           (value){ return this.SetAttribute("action",            value); }
  Width            (value){ return this.SetAttribute("width",             value); }
  Height           (value){ return this.SetAttribute("height",            value); }
  Title            (value){ return this.SetAttribute("title",             value); }
  List             (value){ return this.SetAttribute("list",              value); }
  CrossOrigin      (value){ return this.SetAttribute("crossorigin",       value); }
  Preload          (value){ return this.SetAttribute("preload",           value); }
  PlaysInline      (value){ return this.SetAttribute("playsinline",       value); }
  AutoPlay         (value){ return this.SetAttribute("autoplay",          value); }
  Muted            (value){ return this.SetAttribute("muted",             value); }
  Loop             (value){ return this.SetAttribute("loop",              value); }
  Poster           (value){ return this.SetAttribute("poster",            value); }
  FrameBorder      (value){ return this.SetAttribute("frameborder",       value); }
  Allow            (value){ return this.SetAttribute("allow",             value); }
  AllowFullScreen  (value){ return this.SetAttribute("allowfullscreen",   value); }
  AllowTransparency(value){ return this.SetAttribute("allowtransparency", value); }
  Rows             (value){ return this.SetAttribute("rows",              value); }
  Cols             (value){ return this.SetAttribute("cols",              value); }
  Draggable        (value){ return this.SetAttribute("draggable",         value); }
  AutoComplete     (value){ return this.SetAttribute("autocomplete",      value); }
  TabIndex         (value){ return this.SetAttribute("tabindex",          value); }
  Target           (value){ return this.SetAttribute("target",            value); }
  Source           (value){ return this.SetAttribute("source",            value); }
  Anchor           (value){ return this.SetAttribute("anchor",            value); }
  Overlay          (value){ return this.SetAttribute("overlay",           value); }
  Fixed            (value){ return this.SetAttribute("fixed",             value); }
  Origin           (value){ return this.SetAttribute("origin",            value); }
  Index            (value){ return this.SetAttribute("index",             value); }
  Depth            (value){ return this.SetAttribute("depth",             value); }
  Mode             (value){ return this.SetAttribute("mode",              value); }
  Highlight        (value){ return this.SetAttribute("highlight",         value); }
  Open             (value){ return this.SetStateAttribute("open",            value); }
  Editable         (value){ return this.SetStateAttribute("contenteditable", value); }
  Checked          (value){ return this.SetStateAttribute("checked",         value); }
  Disabled         (value){ return this.SetStateAttribute("disabled",        value); }
  Controls         (value){ return this.SetStateAttribute("controls",        value); }
  ReadOnly         (value){ return this.SetStateAttribute("readonly",        value); }
  Selected         (value){ return this.SetStateAttribute("selected",        value); }
  Required         (value){ return this.SetStateAttribute("required",        value); }
  Data(name, value){ return (name && value) ? this.SetAttribute(`data-${name}`, value) : this; }
  SetData(name, value){ this.node.dataset[name] = value; return this; }
  GetData(name){ return this.node.dataset[name] ?? this.GetAttribute("data-" + name); }
  HasData(name){ return this.node.dataset.hasOwnProperty(name); }

  AriaAtomic         (value){ return this.SetAttribute("aria-atomic",          value); }
  AriaBusy           (value){ return this.SetAttribute("aria-busy",            value); }
  AriaControls       (value){ return this.SetAttribute("aria-controls",        value); }
  AriaCurrent        (value){ return this.SetAttribute("aria-current",         value); }
  AriaDescribedBy    (value){ return this.SetAttribute("aria-describedby",     value); }
  AriaDetails        (value){ return this.SetAttribute("aria-details",         value); }
  AriaDisabled       (value){ return this.SetAttribute("aria-disabled",        value); }
  AriaDropEffect     (value){ return this.SetAttribute("aria-dropeffect",      value); }
  AriaErrorMessage   (value){ return this.SetAttribute("aria-errormessage",    value); }
  AriaExpanded       (value){ return this.SetAttribute("aria-expanded",        value); }
  AriaFlowto         (value){ return this.SetAttribute("aria-flowto",          value); }
  AriaGrabbed        (value){ return this.SetAttribute("aria-grabbed",         value); }
  AriaHasPopUp       (value){ return this.SetAttribute("aria-haspopup",        value); }
  AriaHidden         (value){ return this.SetAttribute("aria-hidden",          value); }
  AriaInvalid        (value){ return this.SetAttribute("aria-invalid",         value); }
  AriaKeyShortcuts   (value){ return this.SetAttribute("aria-keyshortcuts",    value); }
  AriaLabel          (value){ return this.SetAttribute("aria-label",           value); }
  AriaLabelledBy     (value){ return this.SetAttribute("aria-labelledby",      value); }
  AriaLive           (value){ return this.SetAttribute("aria-live",            value); }
  AriaOwns           (value){ return this.SetAttribute("aria-owns",            value); }
  AriaRelevant       (value){ return this.SetAttribute("aria-relevant",        value); }
  AriaRoleDescription(value){ return this.SetAttribute("aria-roledescription", value); }
  AriaOrientation    (value){ return this.SetAttribute("aria-orientation",     value); }
  AriaValueNow       (value){ return this.SetAttribute("aria-valuenow",        value); }
  AriaValueMin       (value){ return this.SetAttribute("aria-valuemin",        value); }
  AriaValueMax       (value){ return this.SetAttribute("aria-valuemax",        value); }
  AriaValueText      (value){ return this.SetAttribute("aria-valuetext",       value); }

  Focus(focus = true){ if (focus) this.GetNode().focus(); return this; }
  HasFocus(){ return window.document.activeElement === this.node; }

  SetPointerCapture(pointer_id){ this.node.setPointerCapture(pointer_id); return this; }
  ReleasePointerCapture(pointer_id){ this.node.releasePointerCapture(pointer_id); return this; }
  HasPointerCapture(pointer_id){ return this.node.hasPointerCapture(pointer_id); }

  GetSelectionPosition()
  {
    const range = window.getSelection().getRangeAt(0);
    const result = {};

    if (!this.node.contains(range.startContainer)) return result;

    let index = 0;
    for (const node of this.GetTextNodes())
    {
      if (node === range.startContainer)
      {
        result.start = index + range.startOffset;
      }

      if (node === range.endContainer)
      {
        result.end = index + range.endOffset;
        break;
      }

      index += node.data.length;
    }

    return result;
  }

  SetSelectionPosition(start, end)
  {
    const range = window.document.createRange();

    let index = 0;
    for (const node of this.GetTextNodes())
    {
      const length = node.data.length;

      if (start >= index && (index + length) > start)
      {
        range.setStart(node, start - index);
      }

      if (end >= index && (index + length) > end)
      {
        range.setEnd(node, end - index);
        break;
      }

      index += length;
    }

    const selection = window.getSelection();
    if (selection.rangeCount > 0)
    {
      selection.removeAllRanges();
    }

    this.Focus();
    selection.addRange(range);
    return this;
  }

  // Style
  SetCSS(name, value)
  {
    if (this.node.style[name] === undefined) throw new Error(`Unknown style ${name}.`);
    this.node.style[name] = value;

    return this;
  }

  ClearCSS(){ this.node.removeAttribute("style"); return this; }

  CSS(object)
  {
    if (!object) return this;

    for (const name in object)
    {
      if (!object.hasOwnProperty(name)) continue;
      const value = object[name];

      this.SetCSS(name, value);
    }

    return this;
  }

  Link(url)
  {
    this.HRef(url);
    this.OnClick(e =>
    {
      if (e.ctrlKey)
      {
        window.open(url, "_blank");
      }
      else
      {
        window.app.Go(url);
      }
    });

    return this;
  }

  GetOffsetWidth(){ return this.node.offsetWidth; }
  GetOffsetHeight(){ return this.node.offsetHeight; }
  GetOffsetLeft(){ return this.node.offsetLeft; }
  GetOffsetTop(){ return this.node.offsetTop; }
  GetRect(){ return this.node.getBoundingClientRect(); }
  GetRects(){ return this.node.getClientRects(); }

  IsChecked(){ return this.node.checked === true; }
  IsDisabled(){ return !!this.HasAttribute("disabled"); }
  IsInPage(){ return document.body.contains(this.node); }
  IsChildOf(tag){ return tag.node.contains(this.node); }
  IsAncestorOf(tag){ return this.node.contains(tag.node); }
  IsOverflowingWidth(){ return this.node.scrollWidth > this.node.clientWidth; }
  IsOverflowingHeight(){ return this.node.scrollHeight > this.node.clientHeight; }
  GetOverflowWidth(){ return this.node.scrollWidth - this.node.clientWidth; }
  GetOverflowHeight(){ return this.node.scrollHeight - this.node.clientHeight; }
  IsOverflowing(){ return this.IsOverflowingWidth() || this.IsOverflowingHeight(); }

  Intersects(b)
  {
    const a = this.GetRect();

    return a.left <= b.right
        && b.left <= a.right
        && a.top  <= b.bottom
        && b.top  <= a.bottom;
  }

  Contains(b)
  {
    const a = this.GetRect();

    return a.left <= b.left
        && a.top <= b.top
        && a.right >= b.right
        && a.bottom >= b.bottom;

    // return a.left <= b.right
    //     && b.left <= a.right
    //     && a.top  <= b.bottom
    //     && b.top  <= a.bottom;
  }

  IsPointInside(x, y, rect = this.node.getBoundingClientRect())
  {
    return x >= rect.left
        && x <= rect.right
        && y >= rect.top
        && y <= rect.bottom;
  }

  GetDistanceToCenter(x, y)
  {
    const rect = this.node.getBoundingClientRect();
    const center_x = rect.x + (rect.width / 2);
    const center_y = rect.y + (rect.height / 2);

    return Math.sqrt((Math.pow(x - center_x, 2)) + (Math.pow(y - center_y, 2)));
  }

  sqr(x){ return x * x; }

  dist2(v, w)
  {
    return this.sqr(v.x - w.x) + this.sqr(v.y - w.y);
  }

  distToSegmentSquared(p, v, w)
  {
    var l2 = this.dist2(v, w);
    if (l2 == 0) return this.dist2(p, v);

    var t = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / l2;
    t = Math.max(0, Math.min(1, t));

    return this.dist2(p, {
      x: v.x + t * (w.x - v.x),
      y: v.y + t * (w.y - v.y),
    });
  }

  distToSegment(p, v, w){ return Math.sqrt(this.distToSegmentSquared(p, v, w)); }

  nearestBottom(x, y)
  {
    const rect = this.node.getBoundingClientRect();
    const w = rect.width;
    const h = rect.height;

    const result = child.distToSegment(
      { x: rect.x + h, y: rect.y + h, },
      { x: rect.x + h + w, y: rect.y + h + w, },
      { x, y, },
    );

    return result;
  }

  FindNearestChild(x, y)
  {
    const children = this.GetChildren();

    let nearest;
    let distance;
    for (let i = 0; i < children.length; i++)
    {
      const child = children[i];

      if (child.IsPointInside(x, y))
      {
        return child;
      }

      const d = child.GetDistanceToCenter(x, y);
      if (distance === undefined || d < distance)
      {
        nearest = child;
        distance = d;
      }
    }

    return nearest;
  }

  SwapPrev()
  {
    const parent = this.GetParent();
    const prev = this.GetPrevSibling();
    if (parent && prev)
    {
      parent.node.insertBefore(this.node, prev.node);
      return true;
    }

    return false;
  }

  SwapNext()
  {
    const parent = this.GetParent();
    const next = this.GetNextSibling();
    if (parent && next)
    {
      parent.node.insertBefore(this.node, next.node.nextSibling);
      return true;
    }

    return false;
  }

  GetNode(){ return this.node; }
  GetRoot(){ return this.root; }
  GetTrunk(){ return this.trunk; }
  GetClasses(){ return this.node.className; }
  GetAttributes(){ return this.node.attributes; }
  GetChildElements(){ return this.node.children; }
  GetNodeName(){ return this.node.nodeName; }
  GetLocalName(){ return this.node.localName; }
  GetTagName(){ return this.node.tagName; }
  GetClassName(){ return this.constructor.name; }
  GetKebabName(){ return this.GetClassName().replace(/([a-z0-9])([A-Z])/g, "$1-$2").toLowerCase(); }
  GetStyles(){ return this.node.style; }

  GetTextNodes(current = this.node, nodes = [])
  {
    const children = current.childNodes;
    for (let i = 0; i < children.length; i++)
    {
      const child = children[i];

      if (child.nodeType === Node.TEXT_NODE)
      {
        nodes.push(child);
      }
      else
      {
        this.GetTextNodes(child, nodes);
      }
    }

    return nodes;
  }

  GetChildren()
  {
    const tags = [];

    const children = this.node.children;
    for (let i = 0; i < children.length; i++)
    {
      const child = children[i];
      if (child.tag) tags.push(child.tag);
    }

    return tags;
  }

  GetSelector()
  {
    const id = this.GetID();
    if (id)
    {
      return `#${id}`;
    }
    else
    {
      const selector = [this.GetLocalName()];
      for (let i = 0; i < this.node.classList.length; i++)
      {
        selector.push(this.node.classList[i]);
      }

      return selector.join(".");
    }
  }

  GetSelector()
  {
    return this.GetLocalName();
  }

  UID(base = window.app.GetPage())
  {
    if (!base) throw new Error(`Tag.GenerateID requires a base tag`);

    if (base._id_index === undefined)
    {
      base._id_index = 1;
    }

    return this.ID(`${base.GetLocalName()}.${base.node.className.replace(" ", ".")}-${base._id_index++}`);
  }

  GetRoot(){ const node = this.node.getRootNode(); if (node) return node.tag; }
  GetParent(){ if (this.node.parentElement) return this.node.parentElement.tag; }
  GetFirstChild(){ if (this.node.firstElementChild) return this.node.firstElementChild.tag; }
  GetLastChild(){ if (this.node.lastElementChild) return this.node.lastElementChild.tag; }
  GetPrevSibling(){ if (this.node.previousElementSibling) return this.node.previousElementSibling.tag; }
  GetNextSibling(){ if (this.node.nextElementSibling) return this.node.nextElementSibling.tag; }

  GetScrollLeft(){ return this.node.scrollLeft; }
  GetScrollTop(){ return this.node.scrollTop; }
  GetScrollWidth(){ return this.node.scrollWidth; }
  GetScrollHeight(){ return this.node.scrollHeight; }
  ScrollTo(x, y, options){ this.node.scrollTo(x, y, options); return this; }
  ScrollIntoView(options){ this.node.scrollIntoView(options); return this; }

  ScrollIntoViewAsync(options = { block: "center", behavior: "smooth" })
  {
    this.ScrollIntoView(options);

    return new Promise((resolve, reject) =>
    {
      window.requestAnimationFrame(() =>
      {
        return resolve(this);
      });
    });
  }

  HasName(){ return this.HasAttribute("name"); }
  HasType(){ return this.HasAttribute("type"); }
  HasID(){ return this.HasAttribute("id"); }
  HasValue(){ return this.node.value ? this.node.value.trim() !== "" : false; }

  HasValue()
  {
    if (this.node.nodeName === "INPUT" && this.node.type === "checkbox")
    {
      return this.node.checked;
    }

    return this.node.value ? this.node.value.trim() !== "" : false;
  }

  GetName(){ return this.GetAttribute("name"); }
  GetText(){ return this.node.textContent; }
  GetType(){ return this.GetAttribute("type"); }
  GetID(){ return this.GetAttribute("id"); }
  GetValue()
  {
    if (this.node.nodeName === "INPUT" && this.node.type === "checkbox")
    {
      return this.node.checked ? "true" : "false";
    }

    if (typeof(this.node.value) === "string") return this.node.value.trim();
    else return this.node.value;
  }

  SetValue(value){ this.node.value = value; return this; }

  Tooltip(value, {
    multiline = false,
    active = false,
    top = false,
    bottom = false,
    right = false,
    left = false,
  } = {})
  {
    this.AddClass("tooltip");
    // this.AddClass("is-absolute");

    if (multiline) this.AddClass("has-tooltip-multiline");
    if (active) this.AddClass("has-tooltip-active");

    if      (top   ) this.AddClass("has-tooltip-top");
    else if (bottom) this.AddClass("has-tooltip-bottom");
    else if (left  ) this.AddClass("has-tooltip-left");
    else if (right ) this.AddClass("has-tooltip-right");

    return this.SetAttribute("data-tooltip", value);
  }

  Title(title){ return this.SetAttribute("title", title); }

  // Simulate a click
  Click(){ this.node.click(); return this; }

  // Wait for an event to happen
  Await(name)
  {
    return new Promise((resolve, reject) =>
    {
      this[name](event =>
      {
        resolve(this);
      }, true, { once: true });
    });
  }

  // Events
  On(name, fn, prevent_default = true, options = undefined)
  {
    if (typeof(fn) === "function")
    {
      this.node.addEventListener(name, event =>
      {
        if (prevent_default) event.preventDefault();

        event.tag = this;
        fn(event);
      }, options);
    }
    else
    {
      return new Promise((resolve, reject) =>
      {
        this.node.addEventListener(name, event =>
        {
          if (prevent_default) event.preventDefault();

          event.tag = this;
          resolve(this);
        }, { once: true });
      });
    }

    return this;
  }

  On(name, fn, prevent_default = true, options = undefined)
  {
    if (typeof(prevent_default) === "object")
    {
      options = prevent_default;
      prevent_default = false;
    }

    let Remove;

    const Handler = event =>
    {
      if (prevent_default) event.preventDefault();

      if (options && options.remove && !this.IsInPage())
      {
        console.log("Automatically removing event", name);
        Remove();
        return;
      }

      event.tag = this;
      event._handler = Handler;
      event.Remove = Remove;
      fn(event);
    };

    Remove = () =>
    {
      this.node.removeEventListener(name, Handler);
    };

    this.node.addEventListener(name, Handler, options);

    return this;
  }

  Once(name, fn, prevent_default = true)
  {
    return this.On(name, fn, prevent_default, { once: true });
    // this.node.addEventListener(name, event =>
    // {
    //   if (prevent_default) event.preventDefault();
    //   fn(this, event);
    // }, { once: true });

    // return this;
  }

  OnResize(fn, prevent_default = true, options = undefined)
  {
    // Get the current position
    let position = this.node.getBoundingClientRect();
    this.On("add", (event) =>
    {
      position = this.node.getBoundingClientRect();
    });

    const self = this;
    function Handler(event)
    {
      // // Because we're listening to the window, the event may not be automatically removed when the element is
      // if (!self.IsInPage()) return window.removeEventListener("resize", Handler, options);
      if (prevent_default) event.preventDefault();

      event.tag = self;
      event.last = position;
      event.position = self.element.getBoundingClientRect();
      position = event.position;
      fn(event);
    }

    this.On("remove", (event) =>
    {
      // Because we're listening to the window, the event may not be automatically removed when the element is
      window.removeEventListener("resize", Handler, options);
    });

    window.addEventListener("resize", Handler, options);

    return this;
  }

  OnWindow(name, fn, prevent_default = true, options = undefined)
  {
    const self = this;
    this.On("add", (event) =>
    {
      function Handler(event)
      {
        if (prevent_default) event.preventDefault();

        event.tag = self;
        fn(event);
      }

      window.addEventListener(name, Handler, options);

      this.Once("remove", (event) =>
      {
        // Because we're listening to the window, the event may not be automatically removed when the element is
        window.removeEventListener(name, Handler, options);
      });
    });

    return this;
  }

  OnWindow(name, fn, prevent_default = true, options = undefined)
  {
    if (typeof(prevent_default) === "object")
    {
      options = prevent_default;
      prevent_default = false;
    }

    // const self = this;
    const Handler = (event) =>
    {
      if (prevent_default) event.preventDefault();

      event.tag = this;
      fn(event);
    }

    const OnAdd = () =>
    {
      window.addEventListener(name, Handler, options);

      this.Once("remove", (event) =>
      {
        // Because we're listening to the window, the event may not be automatically removed when the element is
        window.removeEventListener(name, Handler, options);
      });
    }

    this.On("add", OnAdd);

    // If we missed the "add" event, invoke it manually to start the cycle
    if (this.IsInPage()) OnAdd();

    return this;
  }

  OnResize(fn, prevent_default = true, options = undefined)
  {
    // Get the current position
    let position = this.node.getBoundingClientRect();
    this.On("add", (event) =>
    {
      position = this.node.getBoundingClientRect();
    });

    this.OnWindow("resize", event =>
    {
      event.last = position;
      event.position = this.node.getBoundingClientRect();
      position = event.position;
      fn(event);
    }, prevent_default, options);

    return this;
  }

  OnKey(key, fn, prevent_default = true, options = undefined)
  {
    if (typeof(prevent_default) === "object")
    {
      options = prevent_default;
      prevent_default = false;
    }

    let type;
    if      (typeof(key) === "number") type = "keyCode";
    else if (typeof(key) === "string") type = "key";
    else throw new TypeError("Unknown key type");

    return this.OnWindow("keydown", event =>
    {
      if (event[type] === key)
      {
        // if (document.activeElement !== document.body)
        // {
        //   console.log("Active element", document.activeElement);
        //   return;
        // }

        if (prevent_default)
        {
          event.preventDefault();
        }

        fn(event);
      }
    }, false, options);
  }

  // Keyboard events
  OnKeyBackspace(fn, pd){ return this.OnKey(8, fn, pd); }
  OnKeyTab(fn, pd){ return this.OnKey(9, fn, pd); }
  OnKeyEnter(fn, pd){ return this.OnKey(13, fn, pd); }
  OnKeyShift(fn, pd){ return this.OnKey(16, fn, pd); }
  OnKeyCtrl(fn, pd){ return this.OnKey(17, fn, pd); }
  OnKeyAlt(fn, pd){ return this.OnKey(18, fn, pd); }
  OnKeyPause(fn, pd){ return this.OnKey(19, fn, pd); }
  OnKeyCapsLock(fn, pd){ return this.OnKey(20, fn, pd); }
  OnKeyEscape(fn, pd){ return this.OnKey(27, fn, pd); }
  OnKeySpace(fn, pd){ return this.OnKey(32, fn, pd); }
  OnKeyPageUp(fn, pd){ return this.OnKey(33, fn, pd); }
  OnKeyPageDown(fn, pd){ return this.OnKey(34, fn, pd); }
  OnKeyEnd(fn, pd){ return this.OnKey(35, fn, pd); }
  OnKeyHome(fn, pd){ return this.OnKey(36, fn, pd); }
  OnKeyLeftArrow(fn, pd){ return this.OnKey(37, fn, pd); }
  OnKeyUpArrow(fn, pd){ return this.OnKey(38, fn, pd); }
  OnKeyRightArrow(fn, pd){ return this.OnKey(39, fn, pd); }
  OnKeyDownArrow(fn, pd){ return this.OnKey(40, fn, pd); }
  OnKeyInsert(fn, pd){ return this.OnKey(45, fn, pd); }
  OnKeyDelete(fn, pd){ return this.OnKey(46, fn, pd); }
  OnKey0(fn, pd){ return this.OnKey(48, fn, pd); }
  OnKey1(fn, pd){ return this.OnKey(49, fn, pd); }
  OnKey2(fn, pd){ return this.OnKey(50, fn, pd); }
  OnKey3(fn, pd){ return this.OnKey(51, fn, pd); }
  OnKey4(fn, pd){ return this.OnKey(52, fn, pd); }
  OnKey5(fn, pd){ return this.OnKey(53, fn, pd); }
  OnKey6(fn, pd){ return this.OnKey(54, fn, pd); }
  OnKey7(fn, pd){ return this.OnKey(55, fn, pd); }
  OnKey8(fn, pd){ return this.OnKey(56, fn, pd); }
  OnKey9(fn, pd){ return this.OnKey(57, fn, pd); }
  OnKeyA(fn, pd){ return this.OnKey(65, fn, pd); }
  OnKeyB(fn, pd){ return this.OnKey(66, fn, pd); }
  OnKeyC(fn, pd){ return this.OnKey(67, fn, pd); }
  OnKeyD(fn, pd){ return this.OnKey(68, fn, pd); }
  OnKeyE(fn, pd){ return this.OnKey(69, fn, pd); }
  OnKeyF(fn, pd){ return this.OnKey(70, fn, pd); }
  OnKeyG(fn, pd){ return this.OnKey(71, fn, pd); }
  OnKeyH(fn, pd){ return this.OnKey(72, fn, pd); }
  OnKeyI(fn, pd){ return this.OnKey(73, fn, pd); }
  OnKeyJ(fn, pd){ return this.OnKey(74, fn, pd); }
  OnKeyK(fn, pd){ return this.OnKey(75, fn, pd); }
  OnKeyL(fn, pd){ return this.OnKey(76, fn, pd); }
  OnKeyM(fn, pd){ return this.OnKey(77, fn, pd); }
  OnKeyN(fn, pd){ return this.OnKey(78, fn, pd); }
  OnKeyO(fn, pd){ return this.OnKey(79, fn, pd); }
  OnKeyP(fn, pd){ return this.OnKey(80, fn, pd); }
  OnKeyQ(fn, pd){ return this.OnKey(81, fn, pd); }
  OnKeyR(fn, pd){ return this.OnKey(82, fn, pd); }
  OnKeyS(fn, pd){ return this.OnKey(83, fn, pd); }
  OnKeyT(fn, pd){ return this.OnKey(84, fn, pd); }
  OnKeyU(fn, pd){ return this.OnKey(85, fn, pd); }
  OnKeyV(fn, pd){ return this.OnKey(86, fn, pd); }
  OnKeyW(fn, pd){ return this.OnKey(87, fn, pd); }
  OnKeyX(fn, pd){ return this.OnKey(88, fn, pd); }
  OnKeyY(fn, pd){ return this.OnKey(89, fn, pd); }
  OnKeyZ(fn, pd){ return this.OnKey(90, fn, pd); }
  OnKeyLeftWindowKey(fn, pd){ return this.OnKey(91, fn, pd); }
  OnKeyRightWindowKey(fn, pd){ return this.OnKey(92, fn, pd); }
  OnKeySelectKey(fn, pd){ return this.OnKey(93, fn, pd); }
  OnKeyNumpad0(fn, pd){ return this.OnKey(96, fn, pd); }
  OnKeyNumpad1(fn, pd){ return this.OnKey(97, fn, pd); }
  OnKeyNumpad2(fn, pd){ return this.OnKey(98, fn, pd); }
  OnKeyNumpad3(fn, pd){ return this.OnKey(99, fn, pd); }
  OnKeyNumpad4(fn, pd){ return this.OnKey(100, fn, pd); }
  OnKeyNumpad5(fn, pd){ return this.OnKey(101, fn, pd); }
  OnKeyNumpad6(fn, pd){ return this.OnKey(102, fn, pd); }
  OnKeyNumpad7(fn, pd){ return this.OnKey(103, fn, pd); }
  OnKeyNumpad8(fn, pd){ return this.OnKey(104, fn, pd); }
  OnKeyNumpad9(fn, pd){ return this.OnKey(105, fn, pd); }
  OnKeyMultiply(fn, pd){ return this.OnKey(106, fn, pd); }
  OnKeyAdd(fn, pd){ return this.OnKey(107, fn, pd); }
  OnKeySubtract(fn, pd){ return this.OnKey(109, fn, pd); }
  OnKeyDecimalPoint(fn, pd){ return this.OnKey(110, fn, pd); }
  OnKeyDivide(fn, pd){ return this.OnKey(111, fn, pd); }
  OnKeyF1(fn, pd){ return this.OnKey(112, fn, pd); }
  OnKeyF2(fn, pd){ return this.OnKey(113, fn, pd); }
  OnKeyF3(fn, pd){ return this.OnKey(114, fn, pd); }
  OnKeyF4(fn, pd){ return this.OnKey(115, fn, pd); }
  OnKeyF5(fn, pd){ return this.OnKey(116, fn, pd); }
  OnKeyF6(fn, pd){ return this.OnKey(117, fn, pd); }
  OnKeyF7(fn, pd){ return this.OnKey(118, fn, pd); }
  OnKeyF8(fn, pd){ return this.OnKey(119, fn, pd); }
  OnKeyF9(fn, pd){ return this.OnKey(120, fn, pd); }
  OnKeyF10(fn, pd){ return this.OnKey(121, fn, pd); }
  OnKeyF11(fn, pd){ return this.OnKey(122, fn, pd); }
  OnKeyF12(fn, pd){ return this.OnKey(123, fn, pd); }
  OnKeyNumLock(fn, pd){ return this.OnKey(144, fn, pd); }
  OnKeyScrollLock(fn, pd){ return this.OnKey(145, fn, pd); }
  OnKeySemiColon(fn, pd){ return this.OnKey(186, fn, pd); }
  OnKeyEqualSign(fn, pd){ return this.OnKey(187, fn, pd); }
  OnKeyComma(fn, pd){ return this.OnKey(188, fn, pd); }
  OnKeyDash(fn, pd){ return this.OnKey(189, fn, pd); }
  OnKeyPeriod(fn, pd){ return this.OnKey(190, fn, pd); }
  OnKeyForwardSlash(fn, pd){ return this.OnKey(191, fn, pd); }
  OnKeyGraveAccent(fn, pd){ return this.OnKey(192, fn, pd); }
  OnKeyOpenBracket(fn, pd){ return this.OnKey(219, fn, pd); }
  OnKeyBackSlash(fn, pd){ return this.OnKey(220, fn, pd); }
  OnKeyCloseBraket(fn, pd){ return this.OnKey(221, fn, pd); }
  OnKeySingleQuote(fn, pd){ return this.OnKey(222, fn, pd); }

  OnInput(fn, pd){ return this.On("input", fn, pd); }
  OnBeforeInput(fn, pd){ return this.On("beforeinput", fn, pd); }
  OnFocus(fn, pd){ return this.On("focus", fn, pd); }
  OnBlur(fn, pd){ return this.On("blur", fn, pd); }
  OnChange(fn, pd){ return this.On("change", fn, pd); }
  OnSubmit(fn, pd){ return this.On("submit", fn, pd); }
  OnToggle(fn, pd){ return this.On("toggle", fn, pd); }

  OnSubmitForm(fn, pd)
  {
    return this.OnSubmit(async event =>
    {
      const enabled = [];
      if (!this.IsDisabled())
      {
        enabled.push(this);
      }

      try
      {
        event.preventDefault();

        const values = {};
        const inputs = this.QueryAll("input, textarea, select");
        for (let i = 0; i < inputs.length; i++)
        {
          const input = inputs[i];
          const type = input.GetType();
          const name = input.GetID() ?? input.GetName() ?? type;
    
          switch (type)
          {
            case "radio":
            case "checkbox":
            {
              values[name] = input.IsChecked();
              break;
            }
            default:
            {
              values[name] = input.GetValue();
            }
          }

          if (!input.IsDisabled())
          {
            enabled.push(input);
          }
        }

        for (const input of enabled)
        {
          input.AddClass("is-loading");
          input.Disabled(true);
        }
  
        await fn(event, values);
        // await window.app.Sleep(2000);
      }
      catch (error)
      {
        // Display the error to the client
        window.notifications.Error(error);
      }
      finally
      {
        if (enabled && enabled instanceof Array)
        {
          for (const input of enabled)
          {
            input.RemoveClass("is-loading");
            input.Disabled(false);
          }
        }
      }
    }, pd);
  }

  // Pointer events
  OnGotPointerCapture(fn, pd){ return this.On("gotpointercapture", fn, pd); }
  OnLostPointerCapture(fn, pd){ return this.On("lostpointercapture", fn, pd); }
  OnPointerEnter(fn, pd){ return this.On("pointerenter", fn, pd); }
  OnPointerLeave(fn, pd){ return this.On("pointerleave", fn, pd); }
  OnPointerOut(fn, pd){ return this.On("pointerout", fn, pd); }
  OnPointerOver(fn, pd){ return this.On("pointerover", fn, pd); }
  OnPointerDown(fn, pd){ return this.On("pointerdown", fn, pd); }
  OnPointerUp(fn, pd){ return this.On("pointerup", fn, pd); }
  OnPointerMove(fn, pd){ return this.On("pointermove", fn, pd); }
  OnPointerCancel(fn, pd){ return this.On("pointercancel", fn, pd); }

  // Mouse Events
  OnClick(fn, pd){ return this.On("click", fn, pd); }
  OnDoubleClick(fn, pd){ return this.On("dblclick", fn, pd); }
  OnMouseDown(fn, pd){ return this.On("mousedown", fn, pd); }
  OnMouseUp(fn, pd){ return this.On("mouseup", fn, pd); }
  OnMouseOver(fn, pd){ return this.On("mouseover", fn, pd); }
  OnMouseOut(fn, pd){ return this.On("mouseout", fn, pd); }
  OnMouseEnter(fn, pd){ return this.On("mouseenter", fn, pd); }
  OnMouseLeave(fn, pd){ return this.On("mouseleave", fn, pd); }
  OnMouseMove(fn, pd){ return this.On("mousemove", fn, pd); }
  OnTouchMove(fn, pd){ return this.On("touchmove", fn, pd); }
  OnTouchStart(fn, pd){ return this.On("touchstart", fn, pd); }
  OnAuxClick(fn, pd){ return this.On("auxclick", fn, pd); }
  OnWheel(fn, pd){ return this.On("wheel", fn, pd); }
  OnScroll(fn, pd){ return this.On("scroll", fn, pd); }
  OnClickOff(fn, override)
  {
    const self = this;
    document.addEventListener("click", function Handler(event)
    {
      if (!self.IsInPage()) return document.removeEventListener("click", Handler);

      // If the element is somewhere in the path, don't call fn
      for (let i = 0; i < event.path.length; i++)
      {
        const path = event.path[i];
        if (path === self.element) return;
        else if (override && path.classList && path.classList.contains(override)) return;
      }

      // Only prevent if it actually passed
      event.preventDefault();
      event.tag = self;
      fn(event);
    });

    return this;
  }

  OnDrag(fn, pd){ return this.On("drag", fn, pd); }
  OnDragStart(fn, pd){ return this.On("dragstart", fn, pd); }
  OnDragEnd(fn, pd){ return this.On("dragend", fn, pd); }
  OnDragOver(fn, pd){ return this.On("dragover", fn, pd); }
  OnDragEnter(fn, pd){ return this.On("dragenter", fn, pd); }
  OnDragLeave(fn, pd){ return this.On("dragleave", fn, pd); }
  OnDrop(fn, pd){ return this.On("drop", fn, pd); }

  OnAddClass(fn, pd){ return this.On("addclass", fn, pd); }
  OnToggleClass(fn, pd){ return this.On("toggleclass", fn, pd); }
  OnRemoveClass(fn, pd){ return this.On("removeclass", fn, pd); }
  OnContextMenu(fn, pd){ return this.On("contextmenu", fn, pd); }
  OnLoad(fn, pd){ return this.On("load", fn, pd); }
  OnError(fn, pd){ return this.On("error", fn, pd); }

  // Observer events (custom, non standard)
  OnAdd(fn, pd){ return this.On("add", fn, pd); }
  OnRemove(fn, pd){ return this.On("remove", fn, pd); }
  OnAttribute(fn, pd){ return this.On("attribute", fn, pd); }
  OnReflow(fn, pd){ return this.OnWindow("reflow", fn, pd); }

  OnAnimationStart(fn, pd){ return this.On("animationstart", fn, pd); }
  OnAnimationEnd(fn, pd){ return this.On("animationend", fn, pd); }
  OnAnimationIteration(fn, pd){ return this.On("animationiteration", fn, pd); }
  OnAnimationCancel(fn, pd){ return this.On("animationcancel", fn, pd); }
  OnAnimationFinish(fn, pd){ return this.On("animationfinish", fn, pd); }
  OnAnimationPause(fn, pd){ return this.On("animationpause", fn, pd); }
  OnAnimationPlay(fn, pd){ return this.On("animationplay", fn, pd); }
  OnAnimationReverse(fn, pd){ return this.On("animationreverse", fn, pd); }

  // Video events
  OnTimeUpdate(fn, pd){ return this.On("timeupdate", fn, pd); }
  OnPlay(fn, pd){ return this.On("play", fn, pd); }
  OnPause(fn, pd){ return this.On("pause", fn, pd); }
  OnVolumeChange(fn, pd){ return this.On("volumechange", fn, pd); }

  If(value, if_fn, else_fn)
  {
    if (value)
    {
      if_fn.call(this, this);
    }
    else if (else_fn)
    {
      else_fn.call(this, this);
    }

    return this;
  }

  Sleep(ms)
  {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  Walk(fn)
  {
    fn(this);

    const children = this.node.children;
    for (let i = 0; i < children.length; i++)
    {
      const child = children[i];

      if (child.tag) child.tag.Walk(fn);
    }
  }

  Rise(fn)
  {
    fn(this);

    const parent = this.GetParent();
    if (parent) parent.Rise(fn);

    return this;
  }

  Sink(fn)
  {
    fn(this);

    const children = this.node.children;
    for (let i = 0; i < children.length; i++)
    {
      const child = children[i];
      child.tag.Sink(fn);
    }

    return this;
  }

  toJSON()
  {
    const attributes = {};
    for (let i = 0; i < this.node.attributes.length; i++)
    {
      const {name, value} = this.node.attributes[i];
      attributes[name] = value;
    }

    const children = [];
    for (let i = 0; i < this.node.children.length; i++)
    {
      const child = this.node.children[i];
      children.push(child.tag.toJSON());
    }

    return {
      type: this.GetClassName(),
      attributes,
      children,
    };
  }

  fromJSON({type, attributes, children})
  {
    const tag = Tag[type]();

    const keys = Object.keys(attributes);
    for (let i = 0; i < keys.length; i++)
    {
      const key = keys[i];
      const val = attributes[key];

      tag.SetAttribute(key, val);
    }

    this.Add(tag);

    for (let i = 0; i < children.length; i++)
    {
      tag.fromJSON(children[i]);
    }

    return this;
  }

  ToOuterString(){ return this.node.outerHTML.toString(); }
  ToInnerString(){ return this.node.innerHTML.toString(); }
  toString(){ return this.ToOuterString(); }

  SaveData(data = new TagData())
  {
    data.Save("type", this.constructor.name);
    data.Save("class", this.GetAttribute("class"));

    if (this.node.children.length > 0)
    {
      const children = [];
      for (let i = 0; i < this.node.children.length; i++)
      {
        const child = this.node.children[i].tag;
        children.push(child.SaveData(new data.constructor()));
      }

      data.Save("children", children);
    }
    else
    {
      data.Save("text", this.GetText());
    }

    return data;
  }

  LoadData(data)
  {
    this.rendered = true;

    if (this.constructor.name !== data.type)
    {
      throw new Error(`Invalid type "${data.type}" for ${this.constructor.name}.LoadData`);
    }

    if (data.class) this.SetAttribute("class", data.class);
    // if (data.style  ) this.SetAttribute("style", data.style  );

    if (data.children)
    {
      for (let i = 0; i < data.children.length; i++)
      {
        const child_data = data.children[i];
        const child = this.constructor.New(child_data.type).Render();

        child.LoadData(child_data);
        this.Add(child);
      }
    }
    else if (data.text)
    {
      // this.Add(data.text);
      this.Add(
        // data.text,
        // document.createTextNode(data.text)
        Tag.Span().Text(data.text),
      );
    }
    // else
    // {
    //   console.log("No children or text");
    // }

    return this;
  }
}

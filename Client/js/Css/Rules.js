import {Css} from "/js/Css.js";

export class Sheet extends Css
{
  constructor()
  {
    super();
    this.rules = {};
  }

  AddRule(name, value)
  {
    this.rules[name] = value;
    return this;
  }

  Render()
  {
    super.Render();

    const keys = Object.keys(this.rules);
    const rules = [];

    for (let i = 0; i < keys.length; i++)
    {
      const key = keys[i];
      rules.push(`  ${key} {\n${this.rules[key]}\n  }`);
    }

    this.content = "\n" + rules.join("\n\n") + "\n";

    return this;
  }
}

export class Rule extends Css
{
  constructor(value)
  {
    super();
    this.value = value;
    this.attributes = {};
  }

  AddAttribute(name, value)
  {
    // this.attributes.push("  " + value);
    this.attributes[name] = value;
    return this;
  }

  Render(selector)
  {
    if (this.parent && this.parent.selector)
    {
      selector = `${this.parent.selector}${selector}`;
      // selector = `${this.parent.Render()}${selector}`;
    }

    this.selector = selector;

    super.Render();

    const keys = Object.keys(this.attributes);
    const attr = [];

    for (let i = 0; i < keys.length; i++)
    {
      const key = keys[i];
      attr.push(`    ${key}: ${this.attributes[key]};`);
    }

    // this.AddRule(selector, ` {\n${attr.join("\n")}\n}`);
    this.AddRule(selector, attr.join("\n"));

    return this;
  }

  GetValue(){ return this.value; }
}

export class Class extends Rule
{
  Render(){ return super.Render(`.${this.GetValue()}`); }
}

export class Member extends Rule
{
  Render(){ return super.Render(`.${this.GetValue()}`); }
}

export class Not extends Rule
{
  Render(){ return super.Render(`:not(${this.GetValue()})`); }
}

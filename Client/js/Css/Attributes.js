import {Css} from "/js/Css.js";

export class Attribute extends Css
{
  Set(key, value)
  {
    this[key] = value;
    return this;
  }

  Put(...keys)
  {
    const value = keys.pop();

    for (let i = 0; i < keys.length; i++)
    {
      const key = keys[i];

      // Find the first unused key
      if (!this.hasOwnProperty(key))
      {
        return this.Set(key, value);
      }
    }

    throw new Error(`No unused keys from [${keys.join(", ")}]`);
  }

  Push(key, value)
  {
    if (this[key]) this[key].push(value);
    else this[key] = [value];

    return this;
  }

  Value(value){ return this.Set("value", value); }
  Num(v){ return this.Value(`${v}`); }
  Rem(v){ return this.Value(`${v}rem`); }
  Em(v){ return this.Value(`${v}em`); }
  Px(v){ return this.Value(`${v}px`); }
  Important(value = true){ return this.Set("important", value); }

  Render(name, ...values)
  {
    if (this.important) values.push("!important");

    // this.parent.AddAttribute(`${name}: ${values.join(" ")};`);
    this.parent.AddAttribute(name, values.join(" "));

    return this;
  }
}

export class TextAlign extends Attribute
{
  Center(){ return this.Set("alignment", "center"); }
  Render(){ return super.Render("text-align", this.alignment); }
}

export class Color extends Attribute
{
  Raw(v){ return this.Set("color", v); }
  Hex(v){ return this.Set("color", `#${v}`); }
  Render(){ return super.Render("color", this.color); }
}

export class FontWeight extends Attribute
{
  Raw(v){ return this.Set("weight", v); }
  Rem(v){ return this.Set("weight", `${v}rem`); }
  Render(){ return super.Render("font-weight", this.weight); }
}

export class FontSize extends Attribute
{
  Raw(v){ return this.Set("size", v); }
  Rem(v){ return this.Set("size", `${v}rem`); }
  Render(){ return super.Render("font-size", this.size); }
}

export class LineHeight extends Attribute
{
  Raw(v){ return this.Set("height", v); }
  Rem(v){ return this.Set("height", `${v}rem`); }
  Render(){ return super.Render("line-height", this.height); }
}

export class Display extends Attribute
{
  None(){ return this.Value("none"); }
  Block(){ return this.Value("block"); }
  Flex(){ return this.Value("flex"); }
  Grid(){ return this.Value("grid"); }
  InlineFlex(){ return this.Value("inline-flex"); }
  InlineBlock(){ return this.Value("inline-block"); }
  InlineGrid(){ return this.Value("inline-grid"); }
  Render(){ return super.Render("display", this.value); }
}

export class Position extends Attribute
{
  Static(){ return this.Value("static"); }
  Fixed(){ return this.Value("fixed"); }
  Scroll(){ return this.Value("scroll"); }
  Absolute(){ return this.Value("absolute"); }
  Relative(){ return this.Value("relative"); }
  Render(){ return super.Render("position", this.value); }
}

export class FlexWrap extends Attribute
{
  Wrap(){ return this.Value("wrap"); }
  NoWrap(){ return this.Value("nowrap"); }
  Render(){ return super.Render("flex-wrap", this.value); }
}

export class Margin extends Attribute
{
  Num(v){ return this.Push("values", `${v}`); }
  Rem(v){ return this.Push("values", `${v}rem`); }
  Em(v){ return this.Push("values", `${v}em`); }
  Px(v){ return this.Push("values", `${v}px`); }
  Render(){ return super.Render("margin", (this.values || []).join(" ")); }
}

export class MarginTop extends Attribute { Render(){ return super.Render("margin-top", this.value); } }
export class MarginLeft extends Attribute { Render(){ return super.Render("margin-left", this.value); } }
export class MarginRight extends Attribute { Render(){ return super.Render("margin-right", this.value); } }
export class MarginBottom extends Attribute { Render(){ return super.Render("margin-bottom", this.value); } }

export class Padding extends Attribute
{
  Num(v){ return this.Push("values", `${v}`); }
  Rem(v){ return this.Push("values", `${v}rem`); }
  Em(v){ return this.Push("values", `${v}em`); }
  Px(v){ return this.Push("values", `${v}px`); }
  Render(){ return super.Render("padding", (this.values || []).join(" ")); }
}

export class PaddingTop extends Attribute { Render(){ return super.Render("padding-top", this.value); } }
export class PaddingLeft extends Attribute { Render(){ return super.Render("padding-left", this.value); } }
export class PaddingRight extends Attribute { Render(){ return super.Render("padding-right", this.value); } }
export class PaddingBottom extends Attribute { Render(){ return super.Render("padding-bottom", this.value); } }

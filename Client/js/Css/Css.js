export class Css
{
  static Register()
  {
    Css[this.name] = (value) =>
    {
      return new this(value);
    }

    return this;
  }

  constructor()
  {
    this.children = [];
  }

  Add(...css)
  {
    this.children.push.apply(this.children, css);
    return this;
  }

  AddRule(name, value)
  {
    if (!this.parent) throw new Error(`Invalid Css rule parent`);
    else return this.parent.AddRule(name, value);
  }

  Render()
  {
    for (let i = 0; i < this.children.length; i++)
    {
      const child = this.children[i];
      child.parent = this;
      child.Render();
    }

    return this;
  }
}

// import * as CssModules from "/js/Css.js";
// import {Css} from "/js/Css.js";
//
// for (const key in CssModules)
// {
//   CssModules[key].Register();
// }
//
// export class TempTag extends Tag
// {
//   constructor(a, b)
//   {
//     super(a, b);
//   }
//
//   Style(...rules)
//   {
//     this.constructor.style = Tag.Style();
//
//     const sheet = Css.Sheet().Add(...rules).Render();
//     console.log(sheet.content);
//
//     this.constructor.style.Text(sheet.content);
//
//     // this.Add(this.constructor.style);
//     console.log(this.constructor.style);
//
//     return this;
//   }
//
//   Render(...args)
//   {
//     this.Style();
//
//     return super.Render(this.constructor.style, ...args);
//   }
// }

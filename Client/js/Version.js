export class Version
{
  constructor(url)
  {
    this.url = url;
    this.version = url.replace(/.*?\?([\d+\.]+)/, "$1");
    this.parts = this.version.split(".").map(part => Number(part));

    if (window.localStorage)
    {
      this.version = window.localStorage.getItem("version");

      if (this.version !== url)
      {
        window.localStorage.setItem("version", url);

        // Reload the current page without the browser cache
        window.location.reload(true);
      }
    }
  }

  GetUrl(){ return this.url; }
  GetVersion(){ return this.version; }
  GetMajor(){ return this.parts[0]; }
  GetMinor(){ return this.parts[1]; }
  GetPatch(){ return this.parts[2]; }
}

window.version = new Version(import.meta.url);

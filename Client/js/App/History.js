export class History
{
  constructor(app, callback)
  {
    this.app = app;
    this.callback = callback;
    this.state = {
      url: window.location.pathname,
    };

    // When back and forward browser buttons or history.go() is called
    window.addEventListener("popstate", event =>
    {
      event.preventDefault();

      if (event.state)
      {
        // console.log("Swapping state", this.state.url, "for", event.state.url);
        this.state = event.state;
        // this.app.HistoryPop(this.GetUrl());
        this.app.Go(this.GetUrl());
      }

      // if (this.callback)
      // {
      //   this.callback.call(this.app, this.GetUrl());
      // }
    }, false);

    // Tell the browser to remember the initial state
    window.history.replaceState(this.state, "", window.location.href);
    // window.history.replaceState(this.state, "", window.location.pathname);

    // this.Replace(window.location.pathname);
  }

  Push(url)
  {
    // if (typeof(url) === "string")
    // {
    //   url = url.toLowerCase();
    // }

    const temp = new URL(url, window.location.origin);

    // if (url[0] !== "/")
    // {
    //   url = "/" + url;
    // }

    // We only push a new state if it's a different page
    // if (window.location.pathname !== url)
    if (window.location.pathname !== temp.pathname)
    {
      // console.log("Pushing new history state", url);
      this.state.url = temp.href;
      window.history.pushState(this.state, "", temp.href);
      // window.dispatchEvent(new Event("popstate"));
    }

    return this.GetUrl();
  }

  Replace(url)
  {
    const temp = new URL(url, window.location.origin);

    // if (typeof(url) === "string")
    // {
    //   url = url.toLowerCase();
    // }
    //
    // if (url[0] !== "/")
    // {
    //   url = "/" + url;
    // }

    this.state.url = temp.href;
    window.history.replaceState(this.state, "", temp.href);
    // window.dispatchEvent(new Event("popstate"));

    return this.GetUrl();
  }

  Redirect(url)
  {
    // if (typeof(url) === "string")
    // {
    //   url = url.toLowerCase();
    // }
    //
    // if (url[0] !== "/")
    // {
    //   url = "/" + url;
    // }

    const temp = new URL(url, window.location.origin);

    this.state.url = temp.href;
    window.history.replaceState(this.state, "", temp.href);

    return this.GetUrl();
  }

  Back(){ window.history.back(); return this; }
  Forward(){ window.history.forward(); return this; }
  Go(i){ window.history.go(i); return this; }

  GetLength(){ return window.history.length; }
  GetUrl(){ return decodeURI(this.state.url).toLowerCase(); }
}

import {Model} from "/js/Model.js";

export class SessionCache
{
  constructor(app, sessions = [])
  {
    this.app = app;
    this.sessions = sessions;

    if (app.IsLocalHost())
    {
      console.log(this.sessions);
    }
  }

  Store(session)
  {
    // if (!session.name) session.name = session.model.constructor.name;
    // if (!session.name) throw new Error(`Cannot cache a nameless session`);

    this.sessions.push(session);
    return this;
  }

  Delete(session)
  {
    // if (!session.name) throw new Error(`Cannot clear a nameless session`);

    for (let i = 0; i < this.sessions.length; i++)
    {
      if (this.sessions[i]._id.str === session._id.str)
      {
        this.sessions.splice(i, 1);
        return this;
      }
    }

    throw new Error(`Failed to delete session`);
  }

  async Get(ctor, login)
  {
    if (typeof(ctor) === "string")
    {
      ctor = Model.GetModel(ctor);
    }

    for (let i = 0; i < this.sessions.length; i++)
    {
      const session = this.sessions[i];

      if (session.model instanceof ctor)
      {
        const exp = session.expiration;
        if (exp && exp.getTime() < (Date.now() + window.app.GetSessionExpirationOffset()))
        {
          continue;
        }

        return session.model;
      }
    }

    if (typeof(login) === "function")
    {
      const client = await this.app.GetClient();
      await login.call(this, client);

      // Call it again WITHOUT the login function
      return this.Get(ctor);
    }

    return null;
  }

  Find({ctor, name, type})
  {
    if (typeof(ctor) === "string")
    {
      ctor = Model.GetModel(ctor);
    }

    for (let i = 0; i < this.sessions.length; i++)
    {
      const session = this.sessions[i];

      if (ctor && !(session.model instanceof ctor)) continue;
      if (name && session.name !== name) continue;
      if (type && session.type !== type) continue;

      const exp = session.expiration;
      if (exp && exp.getTime() < (Date.now() + window.app.GetSessionExpirationOffset()))
      {
        continue;
      }

      return session.model;
    }
  }

  Search(type, fn, self)
  {
    for (let i = 0; i < this.sessions.length; i++)
    {
      const session = this.sessions[i];

      const exp = session.expiration;
      if (exp && exp.getTime() < (Date.now() + window.app.GetSessionExpirationOffset()))
      {
        continue;
      }

      if (session.type !== type) continue;

      if (fn.call(self, session.model))
      {
        return session.model;
      }
    }
  }
}

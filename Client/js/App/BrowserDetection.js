// NOTE: Credit to https://stackoverflow.com/a/9851769 for this class. Thank you!
export class BrowserDetection
{
  constructor(app)
  {
    this.app = app;

    // Opera 8.0+
    this.opera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(" OPR/") >= 0;

    // Firefox 1.0+
    this.firefox = typeof(InstallTrigger) !== "undefined";

    // Safari 3.0+ "[object HTMLElementConstructor]"
    this.safari = /constructor/i.test(window.HTMLElement) || (function(p){ return p.toString() === "[object SafariRemoteNotification]"; })(!window["safari"] || (typeof(safari) !== "undefined" && safari.pushNotification));

    // Internet Explorer 6-11
    this.explorer = false || !!document.documentMode;

    // Edge 20+
    this.edge = !this.explorer && !!window.StyleMedia;

    // Chrome 1 - 71
    this.chrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);

    // Blink engine detection
    this.blink = (this.chrome || this.opera) && !!window.CSS;

    if      (this.opera   ) this.browser = "Opera";
    else if (this.firefox ) this.browser = "Firefox";
    else if (this.safari  ) this.browser = "Safari";
    else if (this.explorer) this.browser = "InternetExplorer";
    else if (this.edge    ) this.browser = "Edge";
    else if (this.chrome  ) this.browser = "Chrome";
    else if (this.blink   ) this.browser = "Blink";
  }

  IsOpera(){ return this.opera; }
  IsFirefox(){ return this.firefox; }
  IsSafari(){ return this.safari; }
  IsInternetExplorer(){ return this.explorer; }
  IsEdge(){ return this.edge; }
  IsChrome(){ return this.chrome; }
  IsBlink(){ return this.blink; }

  GetBrowser(){ return this.browser; }
}

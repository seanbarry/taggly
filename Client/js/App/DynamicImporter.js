import {Tag} from "/js/Tag.js";

export class DynamicImporter
{
  constructor(app)
  {
    this.app = app;
    this.map = {};
    window.__dynamic_importer = this;

    try
    {
      // Test if the browser supports dynamic imports
      this.importer = new Function("path", "return import(path)");
    }
    catch (error)
    {
    }
  }

  Import(path)
  {
    if (this.importer)
    {
      return this.importer(path);
    }
    else
    {
      return new Promise((resolve, reject) =>
      {
        const url = new URL(path, window.location);

        // Check if we have a cached module for this url
        if (this.map[url])
        {
          resolve(this.map[url]);
          return;
        }

        const blob = new Blob([
          `import * as module from "${url}";`,
          `window.__dynamic_importer.map["${url}"] = module;`, // "export" the module
        ], { type: "text/javascript" });

        const script = Tag
        .Script()
        .Type("module")
        .SetAttribute("defer", "defer")
        .Src(URL.createObjectURL(blob))
        .OnError(e =>
        {
          URL.revokeObjectURL(e.tag.src);
          e.tag.Remove();
          reject(new Error(`Failed to import: ${url}`));
        })
        .OnLoad(e =>
        {
          URL.revokeObjectURL(e.tag.src);
          e.tag.Remove();
          resolve(this.map[url]);
        });

        document.head.appendChild(script.node);
      });
    }
  }

  Require(path, name)
  {
    return new Promise((resolve, reject) =>
    {
      // Check if we have a cached module for this url
      if (this.map[path])
      {
        resolve(this.map[path]);
        return;
      }

      const script = Tag
      .Script()
      .Src(path)
      .OnError(e =>
      {
        e.tag.Remove();
        reject(new Error(`Failed to import: ${url}`));
      })
      .OnLoad(e =>
      {
        e.tag.Remove();

        if (name && window[name])
        {
          this.map[path] = window[name];
          resolve(window[name]);
        }
        else
        {
          resolve(undefined);
        }
      });

      document.head.appendChild(script.node);
    });
  }
}

export class TimeManager
{
  constructor(app)
  {
    this.app = app;
    this.running = true;
    this.timers = [];
    this.tickers = [];
    this.current_time = 0;
    this.then = 0;
    this.dt = 0;
    this._update = this.Update.bind(this);

    // window.requestAnimationFrame(now => this.Update(now));
    window.requestAnimationFrame(this._update);
  }

  Update(now)
  {
    if (!this.running) return;

    const t = Date.now();
    this.current_time = now;

    for (let i = 0; i < this.timers.length; i++)
    {
      const {time, callback} = this.timers[i];
      if (t >= time)
      {
        callback();
        this.timers.splice(i, 1);
        --i;
      }
    }

    for (let i = 0; i < this.tickers.length; i++)
    {
      const {next, callback, interval, state} = this.tickers[i];

      if (t >= next)
      {
        state.ticks += 1;
        callback(state);

        if (state.canceled === true)
        {
          this.tickers.splice(i, 1);
          --i;
        }
        else
        {
          // Reschedule it
          this.tickers[i].next = t + interval;
        }
      }
    }

    // now *= 0.001;  // convert to seconds
    this.dt = now - this.then;
    this.then = now;

    this.app.Update(this.dt, now);

    window.requestAnimationFrame(this._update);
  }

  AddTimer(ms, fn)
  {
    const timer = {
      time: Date.now() + ms,
      callback: fn,
    };

    for (let i = 0; i < this.timers.length; i++)
    {
      if (timer.time >= this.timers[i].time)
      {
        this.timers.splice(i, 0, timer);
        return;
      }
    }

    // Haven't returned yet, so just place it at the end
    this.timers.push(timer);
  }

  AddTicker(ms, fn, {
    immediate = false,
  } = {})
  {
    this.tickers.push({
      next: Date.now() + ms,
      interval: ms,
      callback: fn,
      state: {
        ticks: 0,
        canceled: false,
        Cancel()
        {
          this.canceled = true;
        },
      },
    });

    if (immediate) fn(this.tickers[this.tickers.length - 1].state);
  }
}

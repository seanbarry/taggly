const PART_VISIBLE = new WeakSet();
const FULL_VISIBLE = new WeakSet();

export class Observer
{
  Fire(element, event)
  {
    element.dispatchEvent(event);

    if (element.children)
    {
      for (let i = 0; i < element.children.length; i++)
      {
        this.Fire(element.children[i], event);
      }
    }
  }

  UpdateVisibility(element = document.documentElement)
  {
    if (element === document.documentElement)
    {
      const event = new Event("reflow");
      window.dispatchEvent(event);
    }

    // Disabling this, I think it's too expensive and I don't want to optimize
    // it on this version of the framework
    return;

    const rect = element.getBoundingClientRect();

    // Only completely visible elements return true
    if ((rect.top >= 0) && (rect.bottom <= window.innerHeight))
    {
      if (!FULL_VISIBLE.has(element))
      {
        FULL_VISIBLE.add(element);
        element.dispatchEvent(new Event("fullviewenter"));
      }
    }
    else if (FULL_VISIBLE.has(element))
    {
      FULL_VISIBLE.delete(element);
      element.dispatchEvent(new Event("fullviewleave"));
    }

    // Partially visible elements return true:
    if ((rect.top < window.innerHeight) && (rect.bottom >= 0))
    {
      if (!PART_VISIBLE.has(element))
      {
        PART_VISIBLE.add(element);
        element.dispatchEvent(new Event("viewenter"));
      }
    }
    else if (PART_VISIBLE.has(element))
    {
      PART_VISIBLE.delete(element);
      element.dispatchEvent(new Event("viewleave"));
    }

    if (element.children)
    {
      for (let i = 0; i < element.children.length; i++)
      {
        this.UpdateVisibility(element.children[i]);
      }
    }
  }

  CreateScrollTest()
  {
    window.addEventListener("scroll", (event) =>
    {
      this.UpdateVisibility();
    }, { passive: true, capture: true });
  }

  CreateResizeObserver()
  {
    window.addEventListener("resize", (event) =>
    {
      this.UpdateVisibility();
    });
  }

  CreateIntersectionObserver()
  {
    if (!window.IntersectionObserver) return;

    // console.log("Creating IntersectionObserver");

    // function onIntersection(entries){
    //   entries.forEach(entry => {
    //     console.clear();
    //     console.log(entry.intersectionRatio)
    //     target.classList.toggle('visible', entry.intersectionRatio > 0);
    //
    //     // Are we in viewport?
    //     if (entry.intersectionRatio > 0) {
    //       // Stop watching
    //       // observer.unobserve(entry.target);
    //     }
    //   });
    // }

    this.intersection_observer = new IntersectionObserver((entries) =>
    {
      console.log({entries});
    }, {
      root: null,
      // rootMargin: "150px",
      threshold: 1.0,
    });

    // this.intersection_observer.observe(document.documentElement);

    // this.intersection_observer.observe(document.documentElement, {
    //   // root: null,
    //   // rootMargin: "150px",
    //   // threshold: 1.0,
    // });
  }

  ObserveTagScroll(tag)
  {
    if (this.intersection_observer)
    {
      this.intersection_observer.observe(tag.node);
    }
  }

  UnobserveTagScroll(tag)
  {
    if (this.intersection_observer)
    {
      this.intersection_observer.unobserve(tag.node);
    }
  }

  CreateMutationObserver()
  {
    if (!window.MutationObserver) return;

    let visibility_update_scheduled = false;

    this.mutation_observer = new MutationObserver((mutations, observer) =>
    {
      let update_visibility = false;

      for (let i = 0; i < mutations.length; i++)
      {
        const mutation = mutations[i];

        switch (mutation.type)
        {
          // Child node(s) have been added or removed
          case "childList":
          {
            update_visibility = true;

            for (let i = 0; i < mutation.addedNodes.length; i++)
            {
              const node = mutation.addedNodes[i];

              const event = new Event("add");
              this.Fire(node, event);

              if (node.parentElement)
              {
                const event = new Event("addchild");
                node.parentElement.dispatchEvent(event);
              }
            }

            for (let i = 0; i < mutation.removedNodes.length; i++)
            {
              const node = mutation.removedNodes[i];

              const event = new Event("remove");
              this.Fire(node, event);

              if (mutation.target)
              {
                const event = new Event("removechild");
                mutation.target.dispatchEvent(event);
              }
            }

            break;
          }
          // An attribute was modified
          case "attributes":
          {
            const event = new Event("attribute");
            event.name = mutation.attributeName;
            event.value = mutation.target.getAttribute(mutation.attributeName);
            event.old_value = mutation.oldValue;

            let oldv = event.old_value || "";
            let newv = event.value || "";

            if (oldv.length > newv.length)
            {
              event.change = oldv.replace(newv, "");
            }
            else if (oldv.length < newv.length)
            {
              event.change = newv.replace(oldv, "");
            }

            if (event.value !== event.old_value)
            {
              // console.log(mutation);
              update_visibility = true;
            }

            if (event.old_value && event.value)
            {
              if (event.old_value.length > event.value.length)
              {
                event.removed = event.old_value.replace(event.value, "").trim();
              }
              else
              {
                event.added = event.value.replace(event.old_value, "").trim();
              }
            }
            else
            {
              event.added = event.value;
            }

            this.Fire(mutation.target, event);
            break;
          }
          case "characterData":
          {
            // console.log("characterData", mutation);
            break;
          }
          default:
          {
            throw new Error(`Unknown mutation observer type "${mutation.type}"`);
          }
        }
      }

      if (update_visibility)
      {
        if (visibility_update_scheduled === false)
        {
          visibility_update_scheduled = true;

          window.requestAnimationFrame(() =>
          {
            this.UpdateVisibility();
            visibility_update_scheduled = false;
          });
        }
      }
    });

    this.mutation_observer.observe(document.documentElement, {
      attributes: true,
      characterData: true,
      childList: true,
      subtree: true, // Omit or set to false to observe only changes to the parent node
      attributeOldValue: true,
      characterDataOldValue: true,
    });
  }

  constructor()
  {
    this.CreateScrollTest();
    this.CreateResizeObserver();
    this.CreateIntersectionObserver();
    this.CreateMutationObserver();
  }

  destructor()
  {
    if (this.mutation_observer) this.mutation_observer.disconnect();
    if (this.intersection_observer) this.intersection_observer.disconnect();
  }
}

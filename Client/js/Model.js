import {Tag} from "/js/Tag.js"; // Used in ValidateTag

export class Binary
{
  constructor(value)
  {
    this.value = value;
  }

  length(){ return this.value.length; }
  toJSON(){ return this.value; }
}

const MACHINE_ID = parseInt(Math.random() * 0xffffff, 10);
export class ObjectID
{
  // constructor(id)
  // {
  //   if (id instanceof ObjectID) return id;
  //
  //   // The most common usecase (blank id, new objectId instance)
  //   if (id == null || typeof id === 'number')
  //   {
  //     // Generate a new id
  //     this.id = this.generate(id);
  //
  //     // // If we are caching the hex string
  //     // if (ObjectID.cacheHexString) this.__id = this.toString("hex");
  //
  //     // Return the object
  //     return;
  //   }
  //
  //   // Check if the passed in id is valid
  //   const valid = ObjectID.isValid(id);
  //
  //   // Throw an error if it's not a valid setup
  //   if (!valid && id !== null)
  //   {
  //     throw new Error(
  //       'Argument passed in must be a single String of 12 bytes or a string of 24 hex characters'
  //     );
  //   }
  //   else if (valid && typeof(id) === "string" && id.length === 24 && hasBufferType)
  //   {
  //     return new ObjectID(utils.toBuffer(id, 'hex'));
  //   }
  //   else if (valid && typeof(id) === "string" && id.length === 24)
  //   {
  //     return ObjectID.createFromHexString(id);
  //   }
  //   else if (id !== null && id.length === 12)
  //   {
  //     // assume 12 byte string
  //     this.id = id;
  //   }
  //   else if (id !== null && typeof(id.toHexString) === 'function')
  //   {
  //     // Duck-typing to support ObjectId from different npm packages
  //     return id;
  //   }
  //   else
  //   {
  //     throw new Error(
  //       'Argument passed in must be a single String of 12 bytes or a string of 24 hex characters'
  //     );
  //   }
  //
  //   // this.str = str;
  // }

  constructor(str)
  {
    this.str = str;
  }

  HexToBuffer(string)
  {
    // Credit to csander at https://stackoverflow.com/a/38987857 for this!
    // Thank you!

    const buffer = new Uint8Array(Math.ceil(string.length / 2));
    for (var i = 0; i < buffer.length; i++)
    {
      buffer[i] = parseInt(string.substr(i * 2, 2), 16);
    }

    return buffer;
  }

  BufferToHex(buffer)
  {
    // Credit to csander at https://stackoverflow.com/a/38987857 for this!
    // Thank you!

    let string = "";
    for (let i = 0; i < buffer.length; i++)
    {
      if (buffer[i] < 16) string += "0";
      string += buffer[i].toString(16);
    }

    return string;
  }

  GetTimestamp(id)
  {
    if (typeof(id) === "string")
    {
      id = this.HexToBuffer(id);
    }

    const timestamp = new Date();
    const time = id[3] | (id[2] << 8) | (id[1] << 16) | (id[0] << 24);
    timestamp.setTime(Math.floor(time) * 1000);
    return timestamp;
  };

  static GetInc()
  {
    return (ObjectID.index = (ObjectID.index + 1) % 0xffffff);
  }

  static Generate(time)
  {
    if (typeof(time) !== "number")
    {
      time = ~~(Date.now() / 1000);
    }

    // Use pid
    let pid;
    if (typeof(process) === "undefined" || process.pid === 1)
    {
      pid = Math.floor(Math.random() * 100000) % 0xffff;
    }
    else
    {
      pid = process.pid % 0xffff;
    }

    let inc = this.GetInc();

    // Buffer used
    const buffer = new Uint8Array(12);
    // Encode time
    buffer[3] = time & 0xff;
    buffer[2] = (time >> 8) & 0xff;
    buffer[1] = (time >> 16) & 0xff;
    buffer[0] = (time >> 24) & 0xff;
    // Encode machine
    buffer[6] = MACHINE_ID & 0xff;
    buffer[5] = (MACHINE_ID >> 8) & 0xff;
    buffer[4] = (MACHINE_ID >> 16) & 0xff;
    // Encode pid
    buffer[8] = pid & 0xff;
    buffer[7] = (pid >> 8) & 0xff;
    // Encode index
    buffer[11] = inc & 0xff;
    buffer[10] = (inc >> 8) & 0xff;
    buffer[9] = (inc >> 16) & 0xff;

    // // Return the buffer
    // return buffer;

    let string = "";
    for (let i = 0; i < buffer.length; i++)
    {
      if (buffer[i] < 16) string += "0";
      string += buffer[i].toString(16);
    }

    return new this(string);
  }

  toString(){ return this.str; }
  toJSON(){ return this.str; }
}

ObjectID.index = ~~(Math.random() * 0xffffff);

const models = {};
export class Model
{
  static SetHidden(key, value)
  {
    if (!this.hasOwnProperty("_hidden"))
    {
      Object.defineProperty(this, "_hidden", {
        enumerable: false,
        writable: true,
        value: {},
      });
    }

    this._hidden[key] = value;

    return this;
  }

  static GetHidden(key)
  {
    return this._hidden[key];
  }

  static Register(map)
  {
    if (!map) throw new Error(`${this.name}.Register requires a map parameter`);

    // Create a new map object to have the default parameters at the top for
    // style reasons (purely cosmetic)
    const ordered_map = {
      _id  : "ID"  ,
      // _type: "Type",
      _time: "Time",
      _test: "Test",
    };

    // Copy the map into the ordered_map
    const keys = Object.keys(map);
    for (let i = 0; i < keys.length; i++)
    {
      const key = keys[i];
      ordered_map[key] = map[key];
    }

    this.SetHidden("_map", ordered_map);

    models[this.name] = this;

    return this;
  }

  static Connect(database)
  {
    if (!this.IsServer()) throw new Error(`${this.name}.Connect is only valid on the Server`);
    if (!database) throw new Error(`${this.name}.Connect requires a database parameter`);

    const collection = database.Collection(this.GetCollectionName());

    this.SetHidden("_database", database);
    this.SetHidden("_collection", collection);

    return this;
  }

  static Seed(collection)
  {
    if (!this.IsServer()) throw new Error(`${this.name}.Seed is only valid on the Server`);
    if (!collection) throw new Error(`${this.name}.Seed requires a collection parameter`);
  }

  static Module()
  {
    if (this === Model)
    {
      this.ObjectID = global.app.GetObjectID();
      this.Binary = global.app.GetBinary();

      this.models = {};
      this.SetHidden("_is_server", true);
    }
  }

  static toJSON(data)
  {
    const name = this.name;

    if (!data)
    {
      if (this === Model)
      {
        data = [
          `const {ObjectID, Binary} = require("taggly");`,
          "",
          `const models = {};`,
          this.toString(),
          "",
          `${name}.SetHidden("_is_server", true);`,
        ];
      }
      else
      {
        const parent = this.GetParent();

        data = [
          `const ${parent.name} = require("${parent.name}");`,
          this.toString(),
          `${name}.Register(${JSON.stringify(this.GetMap())});`,
        ];
      }
    }

    return {
      group: "Models",
      name,
      data: [
        `"use strict";`,
        "",
        ...data,
        "",
        `return ${name};`,
      ].join("\n"),
      date: new Date(),
    };
  }

  static ToSlug(string)
  {
    // NOTE: Credit to
    // https://www.w3resource.com/javascript-exercises/fundamental/javascript-fundamental-exercise-123.php
    // Thanks!

    if (typeof(string) !== "string" || string.length === 0) return string;

    return string
    .match(/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g)
    .map(x => x.toLowerCase())
    .join("-");
  }

  // Return an ESTIMATE of an object size in bytes
  static SizeOf(value, visited = new WeakSet())
  {
    switch (typeof(value))
    {
      case "undefined": return 0; // Unsure, seems like it should take SOME space, right?
      case "symbol": return 0; // Unsure
      case "boolean": return 4; // NOTE: Uhh is this right? Seems high, but that's what people said
      case "number": return 8; // I think it's 8 byte numbers by default
      case "string": return value.length * 2; // Each character is 2 bytes
      case "object":
      {
        // Again, confusing that it's 0, but that's what people seem to have said.
        if (value === null) return 0;

        // Check for circular references
        // I'm assuming the reference takes some space
        if (visited.has(value)) return 8;

        visited.add(value);

        // NOTE: Do array indexes take up space or are they implicit?
        // Currently this would treat them as 8 byte numbers each
        let size = 0;
        for (let key in value)
        {
          if (value.hasOwnProperty(key))
          {
            size += this.SizeOf(key, visited);
            size += this.SizeOf(value[key], visited);
          }
        }

        return size;
      }
      default:
      {
        throw new Error(`Unknown type "${typeof(value)}" in Model.SizeOf`);
      }
    }
  }

  // Construct a model or models from raw data
  static Load(data)
  {
    if (!data) return data;
    if (Array.isArray(data)) return data.map(d => this.Load(d));

    // return new this(data);
    return new this().Copy(data);
  }

  static GetCollectionName(){ return this.name; }
  static GetClassName(){ return this.name; }
  static GetModels(){ return models; }
  static GetModel(name){ return models[name]; }
  static GetParent(){ return Object.getPrototypeOf(this); }
  static GetDatabase(){ return this.GetHidden("_database"); }
  static GetCollection(){ return this.GetHidden("_collection"); }
  static GetMap(){ return this.GetHidden("_map"); }
  static GetAncestry(){ return this.GetHidden("_ancestry"); }
  static IsServer(){ return Model.GetHidden("_is_server"); }

  constructor(hidden = {})
  {
    Object.defineProperty(this, "_hidden", {
      enumerable: false,
      writable: true,
      value: hidden,
    });
  }

  Copy(data)
  {
    const map = this.constructor.GetMap();

    const keys = Object.keys(map);
    for (let i = 0; i < keys.length; i++)
    {
      const key = keys[i];

      if (data.hasOwnProperty(key))
      {
        const call = map[key];
        const fn = this[call];

        if (typeof(fn) === "function")
        {
          fn.call(this, undefined, data[key]);
        }
      }
    }

    return this;
  }

  New(data)
  {
    if (!data) return data;
    if (Array.isArray(data)) return data.map(d => this.New(d));

    const model = new this.constructor().Copy(data);

    // if (this.IsServer() && this.GetExplain())
    // {
    //
    // }

    return model;
  }

  IsServer(){ return this.constructor.IsServer(); }

  _TestSizeOf(key, value, max_size)
  {
    if (value === undefined) return;

    const size = this.constructor.SizeOf(value);
    if (size > max_size)
    {
      throw new Error(`${key}'s size cannot be more than ${max_size} bytes`);
    }

    return value;
  }

  DefineHidden(hidden = {})
  {
    Object.defineProperty(this, "_hidden", {
      enumerable: false,
      writable: true,
      value: hidden,
    });

    return this;
  }

  SetHidden(key, value)
  {
    // if (!this.hasOwnProperty("_hidden")) this.DefineHidden();

    this._hidden[key] = value;
    // return value;
    return this;
  }

  GetHidden(key)
  {
    if (this._hidden)
    {
      return this._hidden[key];
    }
  }

  DefaultHidden(key, value)
  {
    this.SetHidden(key, value);
    return value;
  }

  $OwnerID(v){ return this.SetHidden("_owner_id", v); }
  $OwnerType(v){ return this.SetHidden("_owner_type", v); }
  $Access(v){ return this.SetHidden("_access", v); }
  $Action(v){ return this.SetHidden("_action", v); }
  $Operations(v){ return this.SetHidden("_operations", v); }
  $Mode(v){ return this.SetHidden("_mode", v); }
  $Fake(v){ return this.SetHidden("_fake", v); }
  $Filter(v){ return this.SetHidden("_filter", v); }
  $Update(v){ return this.SetHidden("_update", v); }
  $Project(v){ return this.SetHidden("_project", v); }
  $Sort(v){ return this.SetHidden("_sort", v); }
  $Skip(v){ return this.SetHidden("_skip", v); }
  $Limit(v){ return this.SetHidden("_limit", v); }
  $Timeout(v){ return this.SetHidden("_timeout", v); }
  $Explain(v = true){ return this.SetHidden("_explain", v); }
  $Explanation(v){ return this.SetHidden("_explanation", v); }
  $Collation(v){ return this.SetHidden("_collation", v); }
  $Push(v){ return this.SetHidden("_push", v); }

  GetOwnerID(){ return this.GetHidden("_owner_id"); }
  GetOwnerType(){ return this.GetHidden("_owner_type"); }
  GetAccess(){ return this.GetHidden("_access"); }
  GetAction(){ return this.GetHidden("_action"); }
  GetOperations(){ return this.GetHidden("_operations") || this.DefaultHidden("_operations", []); }
  GetMode(){ return this.GetHidden("_mode"); }
  GetFake(){ return this.GetHidden("_fake"); }
  GetFilter(){ return this.GetHidden("_filter") || this.DefaultHidden("_filter", {}); }
  GetUpdate(){ return this.GetHidden("_update") || this.DefaultHidden("_update", {}); }
  GetProject(){ return this.GetHidden("_project"); }
  GetSort(){ return this.GetHidden("_sort"); }
  GetSkip(){ return this.GetHidden("_skip"); }
  GetLimit(){ return this.GetHidden("_limit"); }
  GetTimeout(){ return this.GetHidden("_timeout"); }
  GetExplain(){ return this.GetHidden("_explain"); }
  GetExplanation(){ return this.GetHidden("_explanation"); }
  GetCollation(){ return this.GetHidden("_collation"); }
  GetPush(){ return this.GetHidden("_push") || false; }

  _AddFilter(key, operator, value)
  {
    const filter = this.GetFilter();

    // // The _bsontype check is a really ugly hack to prevent an issue with flattening mongodb types, like ObjectId
    // && (!value._bsontype)

    if ((typeof(value) === "object") && (value !== null) && ((value.constructor === Object) || (value instanceof Model)))
    {
      // Flatten any nested fields
      const keys = Object.keys(value);
      for (let i = 0; i < keys.length; i++)
      {
        this._AddFilter(`${key}.${keys[i]}`, operator, value[keys[i]]);
      }
    }
    else
    {
      if (filter.hasOwnProperty(key))
      {
        if (value === undefined && filter[key].hasOwnProperty(operator))
        {
          // Do nothing, because an undefined shouldn't override a real value
        }
        else
        {
          filter[key][operator] = value;
        }
      }
      else
      {
        filter[key] = { [operator]: value };
      }

      // if (filter[key]) filter[key][operator] = value;
      // else filter[key] = { [operator]: value };
    }
  }

  _AddUpdate(key, operator, value)
  {
    const update = this.GetUpdate();

    if (update.hasOwnProperty(operator))
    {
      if (value === undefined && update[operator].hasOwnProperty(key))
      {
        // Do nothing, because an undefined shouldn't override a real value
      }
      else
      {
        update[operator][key] = value;
      }
    }
    else
    {
      update[operator] = { [key]: value };
    }

    // if (update[operator]) update[operator][key] = value;
    // else update[operator] = { [key]: value };
  }

  _AddOperation(key, operator, value)
  {
    if (!operator) return;

    if (!this.IsServer())
    {
      const ops = this.GetOperations();
      ops.push(key, operator, value);
      return;
    }

    switch (operator)
    {
      // Match if the key is a BSON type or not
      case "type": return this._AddFilter(key, "$type", value);

      // Match if the key exists or not
      case "??":
      case "exists": return this._AddFilter(key, "$exists", value);

      // Equal to value
      case "===":
      case "==":
      case "eq": return this._AddFilter(key, "$eq", value);

      // Not equal to value
      case "!==":
      case "!=":
      case "ne": return this._AddFilter(key, "$ne", value);

      // Matches ANY of the values specified in the value array
      case "[===]":
      case "[==]":
      case "in": return this._AddFilter(key, "$in", value);

      // Matches NONE of the values specified in the value array
      case "[!==]":
      case "[!=]":
      case "nin": return this._AddFilter(key, "$nin", value);

      // Basic logical operations
      case ">":
      case "gt": return this._AddFilter(key, "$gt", value);
      case "<":
      case "lt": return this._AddFilter(key, "$lt", value);
      case ">=":
      case "gte": return this._AddFilter(key, "$gte", value);
      case "<=":
      case "lte": return this._AddFilter(key, "$lte", value);
      // NOTE: Can't think of a good way to implement $and/$or

      case "$":
      case "regex":
      {
        // RegExps should be sent as their default .toString() format,
        // so we need to convert it
        value.replace(/\/(.*)\/(\w*)/, (m, p1, p2) =>
        {
          if (p1) this._AddFilter(key, "$regex", p1);
          if (p2) this._AddFilter(key, "$options", p2);
        });

        return;
      }

      // case "$text": return this.AddTextFilter(key, "$text", value);

      case "=":
      case "set": return this._AddUpdate(key, "$set", value);

      case "?=":
      case "set_on_insert": return this._AddUpdate(key, "$setOnInsert", value);

      // Basic math update operations
      case "++":
      case "inc": return this._AddUpdate(key, "$inc",  1);
      case "--":
      case "dec": return this._AddUpdate(key, "$inc", -1);
      case "+=":
      case "add": return this._AddUpdate(key, "$inc", value);
      case "-=":
      case "sub": return this._AddUpdate(key, "$inc", -value); // MongoDB only has $inc, no $sub, so invert it
      case "*=":
      case "mul": return this._AddUpdate(key, "$mul", value);
      case "/=":
      case "div": return this._AddUpdate(key, "$mul", 1 / value); // MongoDB only has $mul, no $div
      case "%=":
      case "mod": return this._AddUpdate(key, "$mod", value);
      case "min": return this._AddUpdate(key, "$min", value);
      case "max": return this._AddUpdate(key, "$max", value);

      // Delete a field from the model
      case "~":
      case "delete": return this._AddUpdate(key, "$unset", value);

      case "date": return this._AddUpdate(key, "$currentDate", { $type: "date"      });
      case "time": return this._AddUpdate(key, "$currentDate", { $type: "timestamp" });

      case "push"     : return this._AddUpdate(key, "$push", value);
      case "push_each": return this._AddUpdate(key, "$push", { $each: value });

      case "push_unique"     : return this._AddUpdate(key, "$addToSet", value);
      case "push_unique_each": return this._AddUpdate(key, "$addToSet", { $each: value });

      // Finds and removes element(s)
      case "pull"     : return this._AddUpdate(key, "$pull"   , value);
      case "pull_each": return this._AddUpdate(key, "$pullAll", value);

      case "unshift"     : return this._AddUpdate(key, "$push", { $each: [value], $position: 0 });
      case "unshift_each": return this._AddUpdate(key, "$push", { $each:  value , $position: 0 });

      case "pop"  : return this._AddUpdate(key, "$pop",  1); // Remove last element
      case "shift": return this._AddUpdate(key, "$pop", -1); // Remove first element

      case "&": return this._AddUpdate(key, "$bit", { and: value });
      case "|": return this._AddUpdate(key, "$bit", { or : value });
      case "^": return this._AddUpdate(key, "$bit", { xor: value });

      default: throw new Error(`Unknown Model operator: "${operator}"`);
    }
  }

  Force(key, operator)
  {
    if (this.IsServer())
    {
      this._AddOperation(key, operator, undefined);
    }

    return this;
  }

  Set(key, operator, value)
  {
    this._AddOperation(key, operator, value);

    if (value !== undefined)
    {
      if (this.GetPush())
      {
        const array = this[key] || (this[key] = []);
        array.push(value);
      }
      else
      {
        this[key] = value;
      }
    }

    return this;
  }

  Access(type, mode, hidden)
  {
    // Make sure the type is a registered Model class
    const ctor = this.constructor.GetModel(type);
    if (!ctor) throw new Error(`Invalid access type "${type}"`);

    const model = new ctor(hidden);
    model.$OwnerID(this._id);
    model.$OwnerType(this.constructor.name);
    model.$Access(type);
    model.$Mode(mode);

    const map = model.constructor.GetMap();
    const ops = model.GetOperations();

    for (let i = 0; i < ops.length; i += 3)
    {
      const name = ops[i];
      const operator = ops[i + 1];
      const value = ops[i + 2];
      const call = map[name];

      const fn = model[call];

      if (typeof(fn) === "function")
      {
        fn.call(model, operator, value);
      }
    }

    return model;
  }

  HandleSpecialOperators(key, operator, value)
  {
    switch (operator)
    {
      case "type": // Type operator
      {
        this._AddOperation(key, operator, value);
        return true;
      }
      case "~": // Delete operator
      case "delete":
      {
        this.Set(key, operator, null); // Value is ignored
        return true;
      }
      case "??": // Exists operator
      case "exists":
      {
        this._AddOperation(key, operator, !!value); // Convert value to a boolean
        return true;
      }
      default:
      {
        if (value === undefined)
        {
          this.Force(key, operator);
          return true;
        }
        
        return false;
      }
    }
  }

  SetBoolean(key, operator, value, match)
  {
    if (this.HandleSpecialOperators(key, operator, value)) return this;
    if (value === undefined) return this.Force(key, operator);
    if (value === null) value = false;

    if (typeof(value) === "string")
    {
      if (value === "true") value = true;
      else if (value === "false") value = false;
    }

    if (typeof(value) !== "boolean")
    {
      throw new Error(`Expected ${key} to be a boolean`);
    }

    if (typeof(match) === "boolean" && value !== match)
    {
      throw new Error(`Expected ${key} to equal ${match}`);
    }

    return this.Set(key, operator, value);
  }

  SetNumber(key, operator, value, min, max)
  {
    if (this.HandleSpecialOperators(key, operator, value)) return this;
    if (value === undefined) return this.Force(key, operator);

    if (typeof(value) === "string")
    {
      const temp = Number(value);
      if (!Number.isNaN())
      {
        value = temp;
      }
    }

    if (typeof(value) !== "number")
    {
      throw new Error(`Expected ${key} to be a number`);
    }

    if (min !== undefined && value < min) throw new Error(`Expected ${key} to be greater than ${min}`);
    if (max !== undefined && value > max) throw new Error(`Expected ${key} to be less than ${max}`);

    return this.Set(key, operator, value);
  }

  SetString(key, operator, value, min, max)
  {
    if (this.HandleSpecialOperators(key, operator, value)) return this;
    if (value === undefined) return this.Force(key, operator);
    if (value === null) value = "";

    if (value instanceof RegExp)
    {
      value = value.toString();
    }

    if (typeof(value) !== "string")
    {
      throw new Error(`Expected ${key} with value "${value}" to be a string, not type "${typeof(value)}"`);
    }

    if (min !== undefined && value.length < min) throw new Error(`Expected ${key} to be longer than ${min}`);
    if (max !== undefined && value.length > max) throw new Error(`Expected ${key} to be shorter than ${max}`);

    return this.Set(key, operator, value);
  }

  SetSlug(key, operator, value, min, max)
  {
    if (this.HandleSpecialOperators(key, operator, value)) return this;
    if (value === undefined) return this.Force(key, operator);
    if (value === null) value = "";

    // If the string is a RegExp, then don't convert it to a slug
    if (/\/(.*)\/(\w*)/.test(value)) return value;

    value = this.constructor.ToSlug(value);

    if (typeof(value) !== "string")
    {
      throw new Error(`Expected ${key} to be a string`);
    }

    if (min !== undefined && value.length < min) throw new Error(`Expected ${key} to be longer than ${min}`);
    if (max !== undefined && value.length > max) throw new Error(`Expected ${key} to be shorter than ${max}`);

    return this.Set(key, operator, value);
  }

  SetBinary(key, operator, value, bytes)
  {
    if (this.HandleSpecialOperators(key, operator, value)) return this;
    if (value === undefined) return this.Force(key, operator);

    if (!(value instanceof Binary))
    {
      value = new Binary(value);
    }

    if (!(value instanceof Binary))
    {
      throw new Error(`Expected ${key} to be a Binary`);
    }

    if (bytes !== undefined && value.length() > bytes)
    {
      throw new Error(`Expected ${key} to be smaller than ${bytes} bytes`);
    }

    return this.Set(key, operator, value);
  }

  SetDate(key, operator, value, min, max)
  {
    if (this.HandleSpecialOperators(key, operator, value)) return this;
    if (value === undefined) return this.Force(key, operator);

    if (!(value instanceof Date))
    {
      if (typeof(value) === "string" || typeof(value) === "number")
      {
        value = new Date(value);
      }
      else
      {
        throw new Error(`Expected ${key} to be a Date, or convertable to one, but got "${value}"`);
      }
    }

    if (min !== undefined && value < min) throw new Error(`Expected ${key} to be older than ${min}`);
    if (max !== undefined && value > max) throw new Error(`Expected ${key} to be younger than ${max}`);

    return this.Set(key, operator, value);
  }

  SetMinutes(key, operator, value, min, max)
  {
    if (typeof(value) === "number")
    {
      value = new Date(Date.now() + value * 60 * 1000);
    }

    return this.SetDate(key, operator, value, min, max);
  }

  SetSeconds(key, operator, value, min, max)
  {
    if (typeof(value) === "number")
    {
      value = new Date(Date.now() + value * 1000);
    }

    return this.SetDate(key, operator, value, min, max);
  }

  SetMilliseconds(key, operator, value, min, max)
  {
    if (typeof(value) === "number")
    {
      value = new Date(Date.now() + value);
    }

    return this.SetDate(key, operator, value, min, max);
  }

  SetArray(key, operator, value, max_size)
  {
    if (this.HandleSpecialOperators(key, operator, value)) return this;
    if (value === undefined) return this.Force(key, operator);

    if (typeof(value) !== "object")
    {
      throw new Error(`Expected ${key} to be an array, not ${typeof(value)}`);
    }
    else if (value === null)
    {
      throw new Error(`Expected ${key} to be an array, not null`);
    }
    else if (!(value instanceof Array))
    {
      throw new Error(`Expected ${key} to be an array, not ${value.constructor.name}`);
    }

    this._TestSizeOf(key, value, max_size);

    return this.Set(key, operator, value);
  }

  SetObject(key, operator, value, max_size)
  {
    if (this.HandleSpecialOperators(key, operator, value)) return this;
    if (value === undefined) return this.Force(key, operator);

    if (typeof(value) !== "object")
    {
      throw new Error(`Expected ${key} to be an object, not ${typeof(value)}`);
    }
    else if (value === null)
    {
      throw new Error(`Expected ${key} to be an object, not null`);
    }

    this._TestSizeOf(key, value, max_size);

    return this.Set(key, operator, value);
  }

  SetType(key, operator, value)
  {
    if (this.HandleSpecialOperators(key, operator, value)) return this;
    if (value === undefined) return this.Force(key, operator);

    const ctor = this.constructor.GetModel(value);
    if (!ctor) throw new Error(`Invalid Model type of "${value}"`);

    return this.Set(key, operator, value);
  }

  SetModel(key, operator, value, type = Model)
  {
    if (this.HandleSpecialOperators(key, operator, value)) return this;
    if (value === undefined) return this.Force(key, operator);

    if (value instanceof Model)
    {
      // Store the type of any nested model
      value._type = value.constructor.name;
    }
    else
    {
      // Assume that the value is raw data from the client or database
      const ctor = this.constructor.GetModel(value._type);
      if (!ctor) throw new Error(`Invalid _type "${value._type}" for Model validation`);

      // Construct the model as its _type field specifies
      value = new ctor().Copy(value);

      if (this.IsServer())
      {
        value._type = ctor.name;
      }
    }

    // Load the required type if it was given as a string instead of a class
    if (typeof(type) === "string")
    {
      const temp = this.constructor.GetModel(type);
      if (!temp) throw new Error(`Invalid Model type of "${type}"`);

      type = temp;
    }

    // Make sure the model, regardless of where it came from, is an instance of the type
    if (!(value instanceof type))
    {
      throw new Error(`Invalid type "${value.constructor.name}", expected ${type.name}`);
    }

    return this.Set(key, operator, value);
  }

  SetObjectID(key, operator, value)
  {
    if (this.HandleSpecialOperators(key, operator, value)) return this;
    if (value === undefined) return this.Force(key, operator);

    if (typeof(value) === "string")
    {
      const temp = value;

      try
      {
        value = new ObjectID(value);
      }
      catch (error)
      {
        value = temp;
      }
    }
    else if (value === null)
    {
      // Allowing null
    }
    else
    {
      if (!(value instanceof ObjectID))
      {
        throw new Error(`Expected ${key} to be an ObjectID`);
      }
    }

    return this.Set(key, operator, value);
  }

  ForEach(key, operator, values, setter)
  {
    this.$Push(true);

    for (let i = 0; i < values.length; i++)
    {
      setter.call(this, key, operator, values[i]);
    }

    this.$Push(false);
    return this;
  }

  ID  (o, v){ return this.SetObjectID("_id"  , o, v); }
  Time(o, v){ return this.SetDate    ("_time", o, v); }
  Test(o, v){ return this.SetBoolean ("_test", o, v); }
  Meta(o, v){ return this.SetObject  ("_meta", o, v, 5120); }

  AssignID(o = "="){ return this.ID(o, new ObjectID()); }

  _ValidateMode(required, action)
  {
    const mode = this.GetMode();
    if (!mode)
    {
      throw new Error(`Invalid Model.${this.constructor.name} _mode of "${mode}"`);
    }

    for (let i = 0; i < required.length; i++)
    {
      const r = required[i];
      if (!mode.includes(r))
      {
        throw new Error(`Action ${action} requires mode ${r}, but the Model only allows ${mode}`);
      }
    }
  }

  _GetActionTarget(action)
  {
    switch (action)
    {
      case "Insert": return "InsertOne";
      case "Upsert": return "UpsertOne";
      case "Resert": return "ResertOne";
      case "Unsert": return "UnsertOne";
      case "Delete": return "DeleteOne";
      case "Update": return "UpdateOne";
      case "Search": return "FindOne";
      case "Filter": return "FindAll";
      case "Exists": return "CountOne";
      case "Count": return "CountAll";
      default: throw new Error(`No target for action "${action}"`);
    }
  }

  async _ServerAction(action, mode, session)
  {
    this._ValidateMode(mode, action);

    let target = this._GetActionTarget(action);

    const collection = this.constructor.GetCollection();
    return collection[target](this);
  }

  async _ClientAction(action, mode, hidden, type = this.constructor)
  {
    this._ValidateMode(mode, action);

    if (hidden)
    {
      const keys = Object.keys(hidden);
      for (let i = 0; i < keys.length; i++)
      {
        const key = keys[i];
        this.SetHidden(key, hidden[key]);
      }
    }

    this.$Action(action);

    if (window.app._session_id)
    {
      this._hidden._special_session_id = window.app._session_id;
    }

    return window.app.Request({
      type: "POST",
      path: "/model",
      body: this._hidden,
    })
    .then(data =>
    {
      if (typeof data === "object" && data !== null)
      {
        if (data.hasOwnProperty("value")) return type.Load(data.value);
        else if (data.hasOwnProperty("error")) throw new Error(data.error);
      }

      throw new Error(`Unknown server response data "${typeof data}", ${JSON.stringify(data)} from model ${type?.name} with action ${action} and body ${JSON.stringify(this._hidden)}`);
    });
  }

  async _Action(...args)
  {
    const fn = this.IsServer() ? this._ServerAction : this._ClientAction;
    return fn.apply(this, args);
  }

  Insert(v){ return this._Action("Insert", "C"  , v); }
  Upsert(v){ return this._Action("Upsert", "CRU", v); }
  Resert(v){ return this._Action("Resert", "CRU", v); }
  Unsert(v){ return this._Action("Unsert", "CR" , v); }
  Delete(v){ return this._Action("Delete", "D"  , v); }
  Update(v){ return this._Action("Update", "U"  , v); }
  Search(v){ return this._Action("Search", "R"  , v); }
  Filter(v){ return this._Action("Filter", "R"  , v); }
  Exists(v){ return this._Action("Exists", "R"  , v); }
  Count (v){ return this._Action("Count" , "R"  , v); }

  async ProcessSession(data, session_id, Session)
  {
    // Find the session for the owner model
    const session = await new Session()
    .SessionID("==", session_id)
    .Type("==", this.constructor.name)
    .Model("==", this)
    .$Mode("R")
    .Search();

    if (session && session.expiration && session.duration !== undefined)
    {
      // Refresh the expiration timer on the session, but there's no need to actually
      // wait for it to finish before continuing with the request
      new Session().ID("==", session._id).Expiration("=", session.duration).$Mode("RU").Update();
    }

    return session;
  }

  async ProcessAccess(data, session)
  {
    // Make sure the access is a valid Model type
    const ctor = this.constructor.GetModel(data._access);
    if (!ctor) throw new Error(`Illegal access type "${data._access}"`);

    // Make sure the owner actually allows that access
    const access_fn = this[data._access];
    if (typeof(access_fn) !== "function") throw new Error(`Illegal access function "${data._access}"`);

    return access_fn.call(this, data);
  }

  async ProcessAction(data, session)
  {
    switch (data._action)
    {
      case "Insert": return this.Insert(session);
      case "Upsert": return this.Upsert(session);
      case "Resert": return this.Resert(session);
      case "Unsert": return this.Unsert(session);
      case "Delete": return this.Delete(session);
      case "Update": return this.Update(session);
      case "Search": return this.Search(session);
      case "Filter": return this.Filter(session);
      case "Login" : return this.Login (session);
      case "Logout": return this.Logout(session);
      case "Exists": return this.Exists(session);
      case "Count" : return this.Count (session);
      default: throw new Error(`Illegal action "${data._action}"`);
    }
  }

  async _ServerLogin(session, self)
  {
    if (!self) self = await this.Search(session);
    if (!self) return; // Fail quietly, since a login failing doesn't always mean anything was done wrong

    const model = session
    .Session()
    .OwnerID("==", session.model._id)
    .Model("==", self)
    .Type("==", self.constructor.name);

    const name = this.GetHidden("name");
    if (name)
    {
      model.Name("==", name);
    }

    const expiration = this.GetHidden("expiration");
    if (expiration)
    {
      model.Expiration("==", expiration);
    }

    // Find OR create a session for the self model
    return model
    .$Mode("CR")
    .Unsert(session);
  }

  async _ClientLogin(hidden)
  {
    const Session = this.constructor.GetModel("Session");

    const login = await this._Action("Login", "L", hidden, Session);
    if (!login) return null;

    const cache = await window.app.GetSessionCache();
    if (cache) cache.Store(login);

    return login.model;
  }

  async Login(...args)
  {
    this._ValidateMode("L", "Login"); // Make sure we have Login/Logout mode

    const fn = this.IsServer() ? this._ServerLogin : this._ClientLogin;
    return fn.apply(this, args);
  }

  async _ServerLogout(session, self)
  {
    if (!self) self = await this.Search(session);
    if (!self) throw new Error(`Invalid logout model`);

    // Delete a session for this model
    return session
    .Session()
    .Model("==", { _id: self._id, _type: self.constructor.name })
    .Delete(session);
  }

  async _ClientLogout(hidden)
  {
    const Session = this.constructor.GetModel("Session");

    const logout = await this._Action("Logout", "L", hidden, Session);
    if (!logout) return null;

    const cache = await window.app.GetSessionCache();
    if (cache) cache.Delete(logout);

    return logout.model;
  }

  async Logout(...args)
  {
    this._ValidateMode("L", "Logout"); // Make sure we have Login/Logout mode

    const fn = this.IsServer() ? this._ServerLogout : this._ClientLogout;
    return fn.apply(this, args);
  }

  toJSON(){ return this; }
}

Model.SetHidden("_is_server", false);

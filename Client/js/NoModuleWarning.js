/*
  This file can be included in index.html with a nomodule attribute to load as a fallback for old and outdated browsers that cannot load ES6 modules
*/

const html = `
<div class="columns is-centered is-vcentered" style="position: absolute; top: 0; left: 0 right: 0; bottom: 0; width: 100%; margin: auto;">
  <div class="column is-narrow">
    <div class="notification is-danger has-text-centered is-size-5" style="padding-right: 1.25rem">
      <p class="is-size-4">
        <span class="icon is-large">
          <i class="icon-warning"></i>
        </span>
        <span>Sorry! Your browser does not support JavaScript modules.</span>
      </p>
      <br>
      <p>This website requires modules to work.</p>
      <p>Please update your browser or switch to a newer browser,<br>such as <a class="has-text-info" href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a>, <a class="has-text-info" href="https://www.google.com/chrome/">Chrome</a>, or my personal favorite, <a class="has-text-info" href="https://brave.com/">Brave</a>.</p>
      <br>
      <a class="button is-danger is-medium is-inverted is-outlined is-fullwidth" href="/">
        <span class="icon is-large">
          <i class="icon-spinner11"></i>
        </span>
        <span class="is-hidden-mobile">Click here to reload this page</span>
        <span class="is-hidden-tablet">Reload</span>
      </a>
    </div>
  </div>
</div>`;

document.body.innerHTML = html;

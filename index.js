"use strict";

// This design is to allow the components to be loaded on demand, but still easily accessable
module.exports = {
  get App(){ return require("./Server/App.js").default; },
  get Module(){ return require("./Server/Module.js").Module; },
  get Server(){ return require("./Server/Server/Server.js").default; },

  get Connection(){ return require("./Server/Connection.js").default; },
  get Tag(){ return require("./Server/Tag.js").default; },
  get Server(){ return require("./Server/Server/Server.js").default; },
  get Mail(){ return require("./Server/Mail/Mail.js").default; },
  get Transporter(){ return require("./Server/Mail/Transporter.js").default; },

  get Request(){ return require("./Server/Request/Request.js").default; },
  get Response(){ return require("./Server/Request/Response.js").default; },
  get ModelRequest(){ return require("./Server/Request/ModelRequest.js").default; },

  get Database(){ return require("./Server/Database/Database.js").default; },
  get Collection(){ return require("./Server/Database/Collection.js").default; },
  get Query(){ return require("./Server/Database/Query.js").default; },
  get ObjectID(){ return require("mongodb").ObjectID; },
  get Binary(){ return require("mongodb").Binary; },

  get default(){ return this; },
};

//
